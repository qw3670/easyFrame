package qw.easyFrame.throwables;

import qw.easyFrame.concurrent.Logger;
import qw.easyFrame.interfaces.Loggable;

/**
 * 自定义运行时异常. 
 * 部分构造器内调用writeAsyn() 写日志。
 * @author Rex
 *
 */
public class DefinedRuntimeException extends RuntimeException implements Loggable {
	private static final long serialVersionUID = 20221018L;
	
	/**
	 * 异常发生时间.
	 */
	private long occurredTime = System.currentTimeMillis();
	
	/**
	 * 标记此对象是否已加入日志记录器队列.
	 */
	private boolean queued;
	
	/**
	 * 日志记录器.
	 */
	private static final class Pool {
		private static final Logger L = Logger.createByXML(null, "uncheck");
	}
	
	@Override
	public long getOccurredTime() {
		return occurredTime;
	}
	
	private DefinedRuntimeException(String message, Loggable e) {
		super(message, e.getNonNullCause());
		super.setStackTrace(e.getStackTrace());
		this.occurredTime = e.getOccurredTime();
		this.queued = e.isQueued();
	}
	
	/**
	 * 将Loggable对象重新包装为一个自定义异常.<br>
	 * 注意：此方法会将Loggable对象的queued变量直接复制到新建的异常对象，不会调用日志记录器。
	 * 若需要写日志请在包装完成后手动调用写日志的方法。
	 * @param message 此参数若为null，则会自动使用Loggable.getMessge()返回的字符串代替
	 * @param e
	 * @return
	 */
	public static final DefinedRuntimeException rewrap(String message, Loggable e) {
		message = message == null ? e.getMessage() : message;
		return new DefinedRuntimeException(message, e);
	}

	public DefinedRuntimeException() {
		super();
		// 将错误信息输出到日志.
		enqueue();
	}
	
	/**
	 * 使用此构造器初始化的异常对象不写日志.
	 * @param flag 无实际作用，任何布尔值均可，仅用于与其它构造器区分
	 */
	public DefinedRuntimeException(boolean flag) {
		super();
	}

	public DefinedRuntimeException(String message) {
		super(message);
		// 将错误信息输出到日志.
		enqueue();
	}
	
	/**
	 * 使用此构造器初始化的异常对象不写日志.
	 * 	@param flag 无实际作用，任何布尔值均可，仅用于与其它构造器区分
	 * @param message 异常信息
	 */
	public DefinedRuntimeException(boolean flag, String message) {
		super(message);
	}

	public DefinedRuntimeException(Throwable cause) {
		super(cause);
		// 将错误信息输出到日志.
		enqueue();
	}
	
	/**
	 * 使用此构造器初始化的异常对象不写日志.
	 * @param flag 无实际作用，任何布尔值均可，仅用于与其它构造器区分
	 * @param cause
	 */
	public DefinedRuntimeException(boolean flag, Throwable cause) {
		super(cause);
	}

	public DefinedRuntimeException(String message, Throwable cause) {
		super(message, cause);
		// 将错误信息输出到日志.
		enqueue();
	}
	
	/**
	 * 使用此构造器初始化的异常对象不写日志.
	 * @param flag 无实际作用，任何布尔值均可，仅用于与其它构造器区分
	 * @param message
	 * @param cause
	 */
	public DefinedRuntimeException(boolean flag, String message, Throwable cause) {
		super(message, cause);
	}

	public DefinedRuntimeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// 将错误信息输出到日志
		enqueue();
	}
	
	/**
	 * 使用此构造器初始化的异常对象不写日志.
	 * @param flag 无实际作用，任何布尔值均可，仅用于与其它构造器区分
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DefinedRuntimeException(boolean flag, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	/**
	 * 手动将对象加入日志队列.
	 */
	@Override
	public boolean enqueue() {
		this.queued = true;
		final var l = Pool.L;
		return l.writeAsync(this);
	}
	
	@Override
	public boolean isQueued() {
		return queued;
	}
	
	@Override
	public Logger getLogger() {
		return Pool.L;
	}
	
}