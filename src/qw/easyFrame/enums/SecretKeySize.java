package qw.easyFrame.enums;

/**
 * 密钥长度枚举.
 * @see qw.easyFrame.tools.algorithm.AESUtil
 * @see qw.easyFrame.tools.algorithm.RSAUtil
 * @author Rex
 *
 */
public enum SecretKeySize {

	/**
	 * 适用于AES算法CBC模式的128位密钥.
	 */
	AES_CBC_128(128, "AES/CBC/PKCS5Padding"),
	
	/**
	 * 适用于AES算法CBC模式的192位密钥.
	 */
	AES_CBC_192(192, "AES/CBC/PKCS5Padding"),
	
	/**
	 * 适用于AES算法CBC模式的256位密钥.
	 */
	AES_CBC_256(256, "AES/CBC/PKCS5Padding"),
	
	/**
	 * 适用于AES算法ECB模式的128位密钥.
	 */
	AES_ECB_128(128, "AES/ECB/PKCS5Padding"),
	
	/**
	 * 适用于AES算法ECB模式的192位密钥.
	 */
	AES_ECB_192(192, "AES/ECB/PKCS5Padding"),
	
	/**
	 * 适用于AES算法ECB模式的256位密钥.
	 */
	AES_ECB_256(256, "AES/ECB/PKCS5Padding"),
	
	/**
	 * 适用于RSA算法的512位密钥.
	 */
	RSA_512(512, "RSA"),
	
	/**
	 * 适用于RSA算法的1024位密钥.
	 */
	RSA_1024(1024, "RSA"),
	
	/**
	 * 适用于RSA算法的2048位密钥.
	 */
	RSA_2048(2048, "RSA"),
	
	/**
	 * 适用于RSA算法的4096位密钥.
	 */
	RSA_4096(4096, "RSA"),
	
	/**
	 * 适用于RSA算法的8192位密钥.
	 */
	RSA_8192(8192, "RSA"),
	
	/**
	 * 适用于RSA算法的16384位密钥.
	 */
	RSA_16384(16384, "RSA");

	private int keySize;
	
	private String transformation;

	private SecretKeySize(int keySize, String transformation) {
		this.keySize = keySize;
		this.transformation = transformation;
	}

	public int getKeySize() {
		return keySize;
	}

	public String getTransformation() {
		return transformation;
	}

}