package qw.easyFrame.enums;

import javax.swing.JFileChooser;

/**
 * 文件选择模式枚举.
 * @see qw.easyFrame.view.EasyFileChooser
 * @author Rex
 *
 */
public enum SelectionMode {

	/**
	 * 仅选择文件夹.
	 */
	DIRS_ONLY(JFileChooser.DIRECTORIES_ONLY, "选择文件夹"),

	/**
	 * 仅选择文件.
	 */
	FILES_ONLY(JFileChooser.FILES_ONLY, "选择文件"),

	/**
	 * 可选文件夹或文件.
	 */
	FILES_AND_DIRS(JFileChooser.FILES_AND_DIRECTORIES, "选择文件或文件夹");
	
	private int mode;
	
	/**
	 * 显示在文件选择框的标题上的文字.
	 */
	private String title;

	private SelectionMode(int mode, String title) {
		this.mode = mode;
		this.title = title;
	}

	public int getMode() {
		return mode;
	}
	
	public String getTitle() {
		return title;
	}
	
}