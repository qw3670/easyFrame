package qw.easyFrame.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseListener;
import javax.swing.JTextField;
import qw.easyFrame.view.listeners.TextBoxListener;

/**
 * 对JTextField的简单封装.
 * @author Rex
 *
 */
public class EasyTextField extends JTextField {
	private static final long serialVersionUID = 20231214L;
	
	/**
	 * 标记当前字体颜色是否为错误消息对应的颜色.
	 */
	private boolean isErrorMsg = false;

	/**
	 * 默认字体颜色为灰色.
	 * @param text 初始文字
	 * @param font 字体
	 */
	public EasyTextField(String text, Font font) {
		this(text, font, Color.GRAY);
	}
	
	/**
	 * @param text 初始文字
	 * @param font 字体
	 * @param color 默认字体颜色
	 */
	public EasyTextField(String text, Font font, Color color) {
		super(null, text, 0);
		super.setFont(font);
		super.setForeground(color);
		super.addMouseListener(new TextBoxListener(this));
	}
	
	/**
	 * 使用自定义的鼠标单击事件构造文字输入框.
	 * @param text 初始文字
	 * @param font 字体
	 * @param color 默认字体颜色
	 * @param listener 鼠标单击事件
	 */
	public EasyTextField(String text, Font font, Color color,MouseListener listener) {
		super(null, text, 0);
		super.setFont(font);
		super.setForeground(color);
		super.addMouseListener(listener);
	}
	
	/**
	 * 设置文字（正常颜色）.
	 */
	@Override
	public void setText(String text) {
		if (isErrorMsg) {
			super.setForeground(Color.GRAY);
			isErrorMsg = false;
		}
		super.setText(text);
	}
	
	/**
	 * 输出错误消息（红色字体）.
	 * @param msg
	 */
	public void setErrMsg(String msg) {
		if (isErrorMsg == false) {
			super.setForeground(Color.GRAY);
			isErrorMsg = true;
		}
		super.setText(msg);
	}
	
	/**
	 * 获得int值.
	 */
	public int getInt() {
		return Integer.parseInt(super.getText().strip(), 10);
	}

	/**
	 * 获得double值.
	 */
	public double getDouble() {
		return Double.parseDouble(super.getText().strip());
	}

	/**
	 * 获得boolean值.
	 */
	public boolean getBoolean() {
		return "true".equalsIgnoreCase(super.getText().strip());
	}
	
	/**
	 * 无法通过默认的setForeground方法改变字体颜色.
	 */
	@Override
	public void setForeground(Color fg) {/* do nothing */}

}