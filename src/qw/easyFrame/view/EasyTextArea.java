package qw.easyFrame.view;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JTextArea;
import qw.easyFrame.interfaces.EasyTextComponent;

/**
 * 可按段落编辑文字的文本域.
 * 部分方法是线程安全的。
 * @author Rex
 *
 */
public class EasyTextArea extends JTextArea implements EasyTextComponent {
	private static final long serialVersionUID = 20240927L;

	/**
	 * 用于存储文本域内已写入的文本，每个元素表示一行.
	 */
	protected final List<String> list = Collections.synchronizedList(new ArrayList<String>());

	public EasyTextArea() {
		super(null, null, 0, 0);
		super.setEditable(false);
	}

	public EasyTextArea(String text) {
		super(null, null, 0, 0);
		super.setEditable(false);
		setText(text);
	}
	
	public EasyTextArea(Font font) {
		super(null, null, 0, 0);
		super.setEditable(false);
		this.setFont(font);
	}
	
	/**
	 * @param font
	 * @param foreground 前景色
	 */
	public EasyTextArea(Font font, Color foreground) {
		super(null, null, 0, 0);
		super.setEditable(false);
		this.setFont(font);
		this.setForeground(foreground);
	}
	
	/**
	 * @param font
	 * @param foreground 前景色
	 * @param background 背景色
	 */
	public EasyTextArea(Font font, Color foreground, Color background) {
		super(null, null, 0, 0);
		super.setEditable(false);
		this.setFont(font);
		this.setForeground(foreground);
		this.setBackground(background);
	}
	
	/**
	 * 此方法不可用，是空方法.
	 */
	@Override
	@Deprecated
	public void setEditable(boolean b) {}

	/**
	 * 获取所有文本.<br>
	 * 注意：该方法返回的是一个被Collections.synchronizedList()包装过的ArrayList，
	 * 其大部分方法是线程安全的，但其迭代器是非线程安全的。
	 * @return 所有文本包含在一个List中，每行是集合中一个元素
	 */
	@Override
	public List<String> getTexts() {
		return list;
	}

	/**
	 * 写入文字并换行.
	 * 线程安全。
	 * @param str
	 */
	@Override
	public void setText(String str) {
		final var list = this.list;
		str = str.concat("\r\n");
		synchronized (this) {
			super.setText(str);
		}
		list.clear();
		list.add(str);
	}
	
	/**
	 * 清空文字.
	 * 线程安全。
	 */
	@Override
	public void clear() {
		final var list = this.list;
		synchronized (this) {
			super.setText("");
		}
		list.clear();
	}

	/**
	 * 增加文字并换行.
	 * 线程安全。
	 * @param str
	 */
	@Override
	public void append(String str) {
		final var list = this.list;
		str = str.concat("\r\n");
		synchronized (this) {
			super.append(str);
		}
		list.add(str);
	}
	
	/**
	 * 增加文字并换行.
	 * 线程安全。
	 * @param msg
	 */
	public void append(int msg) {
		append(Integer.toString(msg, 10));
	}
	
	/**
	 * 增加文字并换行.
	 * 线程安全。
	 * @param msg
	 */
	public void append(double msg) {
		append(Double.toString(msg));
	}

	/**
	 * 按行号替换文字.
	 * 线程安全。
	 * @param str
	 * @param rowIndex
	 */
	public void editRow(String str, int rowIndex) {
		final List<String> list;
		int size;
		if (rowIndex > -1 && rowIndex < (size = (list = this.list).size())) {
			list.set(rowIndex, str.concat("\r\n"));
			synchronized (this) {
				super.setText(list.get(0));
				for (var i = 1; i < size; i++)
					super.append(list.get(i));
			}
		}
	}

	/**
	 * 按开头文字寻找行，并替换该行文字. 
	 * 若匹配到多个行，默认只替换第一个匹配到的行。
	 * 线程安全。
	 * @param str
	 * @param starts
	 */
	public void editRowByStarts(String str, String starts) {
		final var list = this.list;
		var size = list.size();
		for (var i = 0; i < size; i++)
			if (list.get(i).startsWith(starts, 0)) {
				editRow(str, i);
				return;
			}
	}

	/**
	 * 按结尾文字寻找行，并替换该行文字. 
	 * 若匹配到多个行，默认只替换第一个匹配到的行。
	 * 线程安全。
	 * @param str
	 * @param ends
	 */
	public void editRowByEnds(String str, String ends) {
		final var list = this.list;
		ends = ends.concat("\r\n");
		var size = list.size();
		for (var i = 0; i < size; i++)
			if (list.get(i).endsWith(ends)) {
				editRow(str, i);
				return;
			}
	}

	/**
	 * 按包含的文字寻找行，并替换该行文字. 
	 * 若匹配到多个行，默认只替换第一个匹配到的行。
	 * 线程安全。
	 * @param str
	 * @param contains
	 */
	public void editRowByContains(String str, String contains) {
		final var list = this.list;
		var size = list.size();
		for (var i = 0; i < size; i++)
			if (list.get(i).indexOf(contains, 0) > -1) {
				editRow(str, i);
				return;
			}
	}

	/**
	 * 按包含的文字寻找行，并替换该行文字. 
	 * 若匹配到多个行，默认只替换第一个匹配到的行。
	 * 线程安全。
	 * @param str
	 * @param equals
	 */
	public void editRowByEquals(String str, String equals) {
		final var list = this.list;
		equals = equals.concat("\r\n");
		var size = list.size();
		for (var i = 0; i < size; i++)
			if (list.get(i).equals(equals)) {
				editRow(str, i);
				return;
			}
	}
	
	/**
	 * 返回文本域内的所有文字.
	 */
	@Override
	public String getTextsString() {
		var builder = new StringBuilder();

		synchronized (this) {
			final var list = this.list;
			var size = list.size();
			for (var i = 0; i < size; i++)
				builder.append(list.get(i));
		}
		
		return builder.toString();
	}
	
}