package qw.easyFrame.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import qw.easyFrame.interfaces.EasyTextComponent;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 包装JTextPane.
 * @author Rex
 *
 */
public class EasyTextPane extends JTextPane implements EasyTextComponent {
	private static final long serialVersionUID = 20240927L;

	private StyledDocument doc;
	
	/**
	 * 是否自动换行.
	 */
	private boolean lineFeed;
	
	private List<String> texts;

	public EasyTextPane(Font font) {
		super();
		super.setEditable(false);
		this.setFont(font);
		this.doc = this.getStyledDocument();
		this.texts = new LinkedList<>();
	}

	/**
	 * 是否自动换行.
	 * @return
	 */
	public boolean isLineFeed() {
		return lineFeed;
	}

	/**
	 * 设置是否自动换行.
	 * @param lineFeed
	 */
	public void setLineFeed(boolean lineFeed) {
		this.lineFeed = lineFeed;
	}

	/**
	 * 获取所有文本.
	 * @return
	 */
	@Override
	public List<String> getTexts() {
		return texts;
	}
	
	/**
	 * 此方法不可用，是空方法.
	 */
	@Override
	@Deprecated
	public void setEditable(boolean b) {}

	/**
	 * @param text      文本内容
	 * @param color     文本颜色
	 */
	public void append(String text, Color color) {
		var set = new SimpleAttributeSet();
		StyleConstants.setForeground(set, color);// 设置文本颜色
		StyleConstants.setAlignment(set, StyleConstants.ALIGN_LEFT);// 设置文本对齐方式
		
		synchronized (this) {
			var length = this.getText().length();
			var docLen = doc.getLength();
			doc.setParagraphAttributes(length, docLen - length, set, false);
			try {
				text = text.concat("\r\n");
				doc.insertString(docLen, text, set);// 插入文本
				texts.add(text);
			} catch (BadLocationException e) {
				throw new DefinedRuntimeException(false, "插入文本失败");
			}
		}
	}
	
	/**
	 * @param text 文本内容
	 */
	@Override
	public void append(String text) {
		append(text, Color.BLACK);
	}
	
	/**
	 * @param text 文本内容
	 */
	@Override
	public void setText(String text) {
		clear();
		append(text, Color.BLACK);
	}
	
	/**
	 * 添加错误信息.
	 * @param text 文本内容
	 */
	public void appendErrMsg(String errMsg) {
		append(errMsg, Color.RED);
	}
	
	/**
	 * 清空文本.
	 */
	@Override
	public synchronized void clear() {
		try {
			doc.remove(0, doc.getLength());
			texts.clear();
		} catch (BadLocationException e) {
			throw new DefinedRuntimeException(false, "清空文本失败");
		}
	}
	
	/**
	 * 获取用户点击位置所在的行数.
	 * @return
	 */
	public int getLineNumberOfClickOn() {
		var position = this.getCaretPosition();
		var root = this.getDocument().getDefaultRootElement();
		return root.getElementIndex(position);
	}
	
	@Override
	public boolean getScrollableTracksViewportWidth() {
		if (lineFeed)
			// 当viewPort的宽度大于EditorPane的宽度时，才设置跟随viewPort的宽度
			return getUI().getPreferredSize(this).width <= getParent().getSize().width;
		else
			return super.getScrollableTracksViewportWidth();
	}

	@Override
	public Dimension getPreferredSize() {
		if (lineFeed)
			// 禁止特性：当viewPort的宽度小于EditorPane的最小宽度时，getPreferredSize返回的是minWidth
			return getUI().getPreferredSize(this);
		else
			return super.getPreferredSize();
	}

}