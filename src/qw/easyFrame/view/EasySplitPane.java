package qw.easyFrame.view;

import java.awt.Component;
import javax.swing.JSplitPane;
import javax.swing.UIManager;

/**
 * 包装JSplitPane.
 * @author Rex
 *
 */
public class EasySplitPane extends JSplitPane {
	private static final long serialVersionUID = 20220124L;

	/**
	 * 是否水平切分.
	 */
	private boolean horizontal;
	
	/**
	 * 切分位置坐标.
	 */
	private int dividerLocation;
	
	/**
	 * 默认为水平切分布局.
	 */
	public EasySplitPane() {
		this(true);
	}

	/**
	 * 若horizontal为true，则为水平切分；
	 * 反之，则为垂直切分。
	 * @param horizontal
	 */
	public EasySplitPane(boolean horizontal) {
		super(horizontal ? 1 : 0, UIManager.getBoolean("SplitPane.continuousLayout"), null, null);
		super.setDividerSize(0);
		super.setEnabled(false);
		this.horizontal = horizontal;
	}
	
	/**
	 * 若horizontal为true，则为水平切分；
	 * 反之，则为垂直切分。
	 * @param horizontal
	 * @param dividerLocation
	 */
	public EasySplitPane(boolean horizontal, int dividerLocation) {
		this(horizontal);
		this.setDividerLocation(dividerLocation);
	}
	
	/**
	 * 若horizontal为true，则为水平切分；
	 * 反之，则为垂直切分。
	 * @param horizontal
	 * @param dividerLocation
	 * @param c0
	 * @param c1
	 */
	public EasySplitPane(boolean horizontal, int dividerLocation, Component c0, Component c1) {
		this(horizontal);
		this.setDividerLocation(dividerLocation);
		add(c0, c1);
	}
	
	public boolean isHorizontal() {
		return horizontal;
	}

	/**
	 * 设置是否水平切分.
	 * @param horizontal
	 */
	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
		super.setOrientation(horizontal ? 1 : 0);
	}
	
	public boolean isVertical() {
		return !horizontal;
	}

	/**
	 * 设置是否垂直切分.
	 * @param vertical
	 */
	public void setVertical(boolean vertical) {
		this.setHorizontal(!vertical);
	}
	
	/**
	 * 添加组件.
	 * @param c0
	 * @param c1
	 */
	public void add(Component c0, Component c1) {
		if (horizontal) {
			super.setLeftComponent(c0);
			super.setRightComponent(c1);
		} else {
			super.setTopComponent(c0);
			super.setBottomComponent(c1);
		}
	}
	
	/**
	 * 此方法不可用，是空方法.<br>
	 * 请使用setHorizontal(boolean)或setVertical(boolean)方法设置切分方向。
	 * @see EasySplitPane#setHorizontal(boolean)
	 * @see EasySplitPane#setVertical(boolean)
	 */
	@Override
	@Deprecated
	public void setOrientation(int orientation) {}
	
	@Override
	public void setDividerLocation(int location) {
		this.dividerLocation = location;
		super.setDividerLocation(location);
	}
	
	/**
	 * 此方法不可用，是空方法.<br>
	 * 请使用setDividerLocation(int)方法设置切分位置。
	 * @see EasySplitPane#setDividerLocation(int)
	 */
	@Override
	@Deprecated
	public void setDividerLocation(double proportionalLocation) {}
	
	@Override
	public int getDividerLocation() {
		return dividerLocation;
	}

}