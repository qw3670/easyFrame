package qw.easyFrame.view;

import java.io.File;
import java.io.Serializable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileView;
import qw.easyFrame.interfaces.Acceptable;

/**
 * 本类存在的目的主要是为了使FileFilter实现序列化接口.
 * @author Rex
 * @see FileFilter
 *
 */
public class EasyFileFilter extends FileFilter implements Serializable {
	private static final long serialVersionUID = 20210917L;
	
	private String description;
	
	private Acceptable accept;
	
    public EasyFileFilter() {
		super();
	}
	
    /**
     * Accept参数建议使用lambda表达式传入.
     * @param description
     * @param accept
     */
    public EasyFileFilter(String description, Acceptable accept) {
		this.description = description;
		this.accept = accept;
	}

	public Acceptable getAccept() {
		return accept;
	}
	
	/**
	 * Accept参数建议使用lambda表达式传入.
	 * @param accept
	 */
	public void setAccept(Acceptable accept) {
		this.accept = accept;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	/**
     * Whether the given file is accepted by this filter.
     */
	@Override
	public boolean accept(File f) {
		return accept.accept(f);
	}

	/**
	 * The description of this filter. For example: "JPG and GIF Images"
	 * @see FileView#getName
	 */
	@Override
	public String getDescription() {
		return description;
	}

}