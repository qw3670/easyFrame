package qw.easyFrame.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import static java.awt.datatransfer.DataFlavor.javaFileListFlavor;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.TransferHandler;
import javax.swing.filechooser.FileFilter;
import qw.easyFrame.enums.SelectionMode;
import qw.easyFrame.interfaces.EasyTextComponent;
import qw.easyFrame.view.util.GUIUtil;

/**
 * 文件选择器.
 * @author Rex
 *
 */
public class EasyFileChooser implements Serializable {
	private static final long serialVersionUID = 20240921L;
	
	protected Font font;
	
	/**
	 * 文件选择对话框右侧的提示文字.
	 */
	protected String prompt;

	/**
	 * 文字提示区.
	 */
	protected EasyTextComponent area;
	
	/**
	 * 默认打开的位置.
	 */
	protected File startDir;
	
	/**
	 * 选择模式.
	 */
	protected SelectionMode mode;
	
	/**
	 * 是否允许多选.
	 */
	protected boolean multi;
	
	/**
	 * 选择文件前是否清理文字提示区已有的文字.
	 */
	protected boolean clearUp;
	
	/**
	 * 文件过滤器.
	 */
	protected EasyFileFilter filter;
	
	/**
	 * 首次打开文件选择器时默认打开桌面，
	 * 默认不支持多选，
	 * 打开文件选择器时默认清理文字提示区.
	 * @param area 文字提示区
	 * @param mode 选择模式
	 */
	public EasyFileChooser(EasyTextArea area, SelectionMode mode) {
		this(area, mode, GUIUtil.getDesktop(), false);
	}

	/**
	 * 打开文件选择器时默认清理文字提示区.
	 * @param area 文字提示区
	 * @param mode 选择模式
	 * @param startDir 首次打开文件选择器时的默认路径
	 * @param multi 是否支持多选
	 */
	public EasyFileChooser(EasyTextArea area, SelectionMode mode, File startDir, boolean multi) {
		this.area = area;
		this.mode = mode;
		this.startDir = startDir;
		this.multi = multi;
		this.clearUp = true;
	}

	public EasyTextComponent getArea() {
		return area;
	}
	public File getStartDir() {
		return startDir;
	}
	public SelectionMode getMode() {
		return mode;
	}
	public boolean isMulti() {
		return multi;
	}
	public boolean isClearUp() {
		return clearUp;
	}
	public FileFilter getFilter() {
		return filter;
	}
	
	/**
	 * 设置打开文件选择器时，是否清理文字提示区.
	 * @param clearUp
	 */
	public void setClearUp(boolean clearUp) {
		this.clearUp = clearUp;
	}
	
	/**
	 * 设置文件过滤器.
	 * @param filter
	 */
	public void setFilter(EasyFileFilter filter) {
		this.filter = filter;
	}
	
	/**
	 * 设置对话框全局字体.
	 * @param font
	 */
	public void setFont(Font font) {
		this.font = font;
	}
	
	/**
	 * 获取对话框全局字体.
	 * @return
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * 获取对话框右侧提示文字.
	 * @return
	 */
	public String getPrompt() {
		return prompt == null ? "" : prompt;
	}

	/**
	 * 设置对话框右侧提示文字.
	 * @param prompt
	 */
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	/**
	 * 打开文件选择器.
	 * @return
	 */
	public File[] choose() {
		// 清理文字提示区
		if (clearUp)
			area.clear();
		
		// 新建JFileChooser，初始化时同时设置默认路径
		var chooser = new JFileChooser(startDir, null);
		// 增加文件拖拽功能
		dragFiles(chooser);
		// 默认选择当前打开的文件夹
		if (mode != SelectionMode.FILES_ONLY  && (filter == null || filter.accept(startDir)))
			chooser.setSelectedFile(startDir);
		
		// 设置提示文字
		if (prompt != null) {
			var textArea = new JTextArea(prompt);
			textArea.setEditable(false);
			chooser.setAccessory(textArea);
		}
		
		// 设置窗口字体
		if (font != null)
			updateFont(chooser, font);
		// 设置选择模式
		chooser.setFileSelectionMode(mode.getMode());
		// 设置是否支持多选
		chooser.setMultiSelectionEnabled(multi);
		// 设置文件过滤器
		if (filter != null)
			chooser.setFileFilter(filter);
		// 定义弹出的文件选择器框
		chooser.showDialog((Component) area, mode.getTitle());

		File[] files = null;
		if (multi)// 多选
			files = checkFiles(chooser.getSelectedFiles());
		else// 单选
			if (mode == SelectionMode.DIRS_ONLY)
				// 验证路径
				files = checkPath(chooser.getSelectedFile());
			else
				files = checkFiles(chooser.getSelectedFile());

		return files;
	}
	
	/**
	 * 文件拖拽功能.
	 * @param chooser
	 */
	private final void dragFiles(JFileChooser chooser) {
		chooser.setTransferHandler(new TransferHandler() {
			private static final long serialVersionUID = 20240801L;

			@Override
			public boolean canImport(TransferSupport info) {
				// 检查是否支持拖拽的数据类型
				return true;
			}

			@Override
			public boolean importData(TransferSupport info) {
				// 处理拖拽的数据
				if (!info.isDrop())
					return false;

				// 获取拖拽的文件列表
				try {
					var files = (List<?>) info.getTransferable().getTransferData(javaFileListFlavor);
					for (var o : files)
						if (o instanceof File file)
							if (mode == SelectionMode.FILES_ONLY) {
								if (file.isFile() && (filter == null || filter.accept(file)))
									chooser.setSelectedFile(file);
							} else if (mode == SelectionMode.DIRS_ONLY) {
								if (file.isDirectory() && (filter == null || filter.accept(file)))
									chooser.setSelectedFile(file);
							} else {
								if (filter == null || filter.accept(file))
									chooser.setSelectedFile(file);
							}

				} catch (Exception e) {
					return false;
				}

				return true;
			}
		});
	}
	
	/**
	 * 验证文件.
	 * @param files
	 */
	private final File[] checkFiles(File... files) {
		// 标记files参数是否为空
		var isVoid = true;
		
		int len;
		if (files != null && (len = files.length) != 0) {
			if (len == 1 && files[0] != null && files[0].isDirectory())
				return checkPath(files[0]);
			else
				for (var i = 0; i < len; i++)
					if (files[i] != null) {
						area.append(files[i].getAbsolutePath());
						isVoid = false;
					}
		}
		
		if (isVoid) {
			area.append("您未选择任何文件");
			files = null;
		}

		return files;
	}

	/**
	 * 验证路径.
	 * @param dir
	 */
	private final File[] checkPath(File dir) {
		File[] files = null;
		var path = "您未选择任何文件夹";
		String msg = null;

		if (dir != null) {
			path = dir.getAbsolutePath();
			msg = "无法读取该目录";
			var sums = count(dir);
			if (sums != null) {
				msg = "此路径下有" + sums[0] + "个文件," + sums[1] + "个子文件夹";
				files = new File[] { dir };
			}
		}

		// 提示用户
		area.append(path);
		if (msg != null)
			area.append(msg);

		return files;
	}

	/**
	 * 统计文件夹内的文件数目.
	 * 不包括子文件夹内的文件。
	 * @param folder
	 * @return
	 */
	private final static int[] count(File folder) {
		int[] temp = null;

		var files = folder.listFiles();
		if (files != null) {
			temp = new int[2];
			for (var f : files)
				if (f.isFile())
					temp[0]++;// 文件数
				else
					temp[1]++;// 子文件夹数
		}

		return temp;
	}
	
	/**
	 * 设置字体.
	 * @param comp
	 * @param font
	 */
	private static final void updateFont(Component comp, Font font) {
		comp.setFont(font);
		if (comp instanceof Container con) {
			var count = con.getComponentCount();
			for (var i = 0; i < count; i++)
				updateFont(con.getComponent(i), font);
		}
	}
	
}