package qw.easyFrame.view.util;

import java.awt.AWTException;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileSystemView;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.view.listeners.ComponentListener;

/**
 * 封装与图形用户界面相关操作的工具类.
 * @author Rex
 *
 */
public class GUIUtil {
	
	/**
	 * 设置JFrame在屏幕中央打开；
	 * 设置窗口不可修改尺寸；
	 * 设置关闭窗口后程序退出；
	 * 设置可见.<br>
	 * 注意：在所有GUI代码均编写完成（窗口及窗口内元素均初始化完毕、窗口元素属性均设置完毕、
	 * 窗口布局设计完毕、窗口元素均已填入窗口）后才能使用本方法，
	 * 否则在Java8及以上版本中会出现不显示窗体的Bug。
	 * @param frame 窗体
	 * @param width 窗体宽度
	 * @param height 窗体高度
	 */
	public static void setJFrame(JFrame frame, int width, int height) {
		frame.setSize(width, height);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	/**
	 * 获得当前系统的桌面文件夹.
	 * @return
	 */
	public final static File getDesktop() {
		return FileSystemView.getFileSystemView().getHomeDirectory();
	}
	
	/**
	 * 根据文本和字体生成一个默认的JButton.
	 * @param text
	 * @param font
	 * @return
	 */
	public static JButton defaultJButton(String text, Font font) {
		var btn = new JButton(text = text == null ? "" : text, null);
		btn.setFont(font);
		btn.setFocusPainted(false);
		btn.addMouseListener(new ComponentListener(btn));
		return btn;
	}
	
	/**
	 * 生成一个默认的JPanel对象.
	 * @param rows 行数
	 * @param cols 列数
	 * @param hgap 上下边距
	 * @param vgap 左右边距
	 * @return
	 */
	public static JPanel defaultJPanel(int rows, int cols, int hgap, int vgap) {
		return new JPanel(new GridLayout(rows, cols, hgap, vgap), true);
	}
	
	/**
	 * 获取屏幕的物理分辨率.<br>
	 * @param screenIndex 对于多显示器设备，指定获取第几块屏幕的分辨率
	 * @return 返回一个长度为2的数组，数组内元素依次为屏幕宽度、屏幕高度
	 */
	public static final double[] getScreenSize(int screenIndex) {
		var env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		var mode = env.getScreenDevices()[screenIndex].getDisplayMode();
		return new double[] { mode.getWidth(), mode.getHeight() };
	}
	
	/**
	 * 获取当前屏幕截图.<br>
	 * 注意：对于多显示器设备，只能捕获其主屏幕的截图。
	 * @param f 屏幕截图文件的完整路径
	 */
	public static boolean captureScreen(File f) {
		try {
			// 创建一个robot对象
			var robot = new Robot();
			// 获取屏幕分辨率（缩放）
			var dim = Toolkit.getDefaultToolkit().getScreenSize();
			// 获取屏幕分辨率（实际）
			var size = getScreenSize(0);
			// 创建该分辨率的矩形对象
			var rect = new Rectangle(0, 0, (int) dim.getWidth(), (int) dim.getHeight());
			// 根据这个矩形截图
			var capture = robot.createMultiResolutionScreenCapture(rect);
			var image = capture.getResolutionVariant(size[0], size[1]);
			// 保存截图
			return ImageIO.write((BufferedImage) image, "png", f);
		} catch (HeadlessException | AWTException | IOException e) {
			throw new DefinedRuntimeException(false, "获取屏幕截图失败", e);
		}
	}
	
}