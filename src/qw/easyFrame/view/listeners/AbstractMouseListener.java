package qw.easyFrame.view.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

/**
 * 鼠标监听器抽象类.<br>
 * 类中没有具体的代码，本类存在的目的是为了简化子类的代码、
 * 使得子类实现MouseListener时无需重写所有方法。
 * @author Rex
 *
 */
public abstract sealed class AbstractMouseListener
		extends MouseAdapter permits ComponentListener, EggListener, RightClickMenu, TextBoxListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		mouseClicked();
	}

	@Override
	public void mousePressed(MouseEvent e) {}
	
	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
	
	/**
	 * 增加了一个不需要传递任何参数就可以调用的方法.
	 */
	public void mouseClicked() {}
	
}