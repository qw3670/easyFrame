package qw.easyFrame.view.listeners;

import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 * 鼠标双击彩蛋.
 * @author Rex
 *
 */
public non-sealed class EggListener extends AbstractMouseListener {
	
	/**
	 * 彩蛋窗口.
	 */
	protected JDialog popup;
	
	/**
	 * 彩蛋窗口内的文字提示区.
	 */
	protected JTextArea textArea;
	
	private Frame owner;// 包含彩蛋的窗体
	private Font font;// 字体
	private String title;// 彩蛋窗口标题
	private String msg;// 彩蛋窗口文字
	private int width;// 彩蛋窗口宽度
	private int height;// 彩蛋窗口高度
	
	/**
	 * 默认的构造器.
	 * @param owner 包含菜单的窗体
	 * @param font 字体
	 */
	public EggListener(Frame owner, Font font) {
		this(owner, font, "(-_-!)", " ┑(￣Д ￣)┍", 250, 100);
	}

	/**
	 * 全参构造器.
	 * @param owner 包含彩蛋的窗体
	 * @param font 字体
	 * @param title 彩蛋窗口标题
	 * @param msg 彩蛋窗口文字
	 * @param width 彩蛋窗口宽度
	 * @param height 彩蛋窗口高度
	 */
	public EggListener(Frame owner, Font font, String title, String msg, int width, int height) {
		this.textArea = new JTextArea(null, null, 0, 0);
		this.textArea.setEditable(false);
		this.owner = owner;
		this.font = font;
		this.title = title;
		this.msg = msg;
		this.width =width;
		this.height = height;
		initAll();
	}

	public JDialog getPopup() {
		return popup;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public Frame getOwner() {
		return owner;
	}

	public void setOwner(Frame owner) {
		this.owner = owner;
		initPopup();
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
		this.textArea.setFont(font);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		this.popup.setTitle(title);
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
		this.textArea.setText(msg);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
		this.popup.setSize(width, height);
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
		this.popup.setSize(width, height);
	}

	/**
	 * 初始化全部必要变量.
	 */
	private final void initAll() {
		this.textArea.setFont(font);
		this.textArea.setText(msg);
		initPopup();
	}

	/**
	 * 初始化彩蛋窗口.
	 */
	private final void initPopup() {
		this.popup = new JDialog(owner, title, true);
		var cp = this.popup.getRootPane().getContentPane();
		cp.setLayout(new GridLayout(1, 1, 0, 0));
		cp.add(this.textArea);

		this.popup.setSize(width, height);
		this.popup.setResizable(false);
		// 关闭对话框时，会完全释放内存（不仅仅是隐藏对话框）
		this.popup.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// 默认不开启对话框
		this.popup.dispose();
	}
	
	/**
	 * 此方法监听鼠标双击.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			mouseClicked();
		}
	}
	
	/**
	 * 此方法不监听鼠标，调用此方法直接弹出窗口.
	 */
	@Override
	public final void mouseClicked() {
		this.popup.setLocationRelativeTo(owner);
		popup.setVisible(true);
	}
	
}