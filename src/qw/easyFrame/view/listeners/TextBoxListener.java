package qw.easyFrame.view.listeners;

import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.text.JTextComponent;

/**
 * 输入框鼠标监听器.
 * @author Rex
 *
 */
public non-sealed class TextBoxListener extends AbstractMouseListener {
	
	protected JTextComponent comp;
	
	/**
	 * 标记用户是否是首次点击输入框.
	 */
	protected boolean unused;
	
	public TextBoxListener(JTextComponent comp) {
		this.comp = comp;
		this.unused = true;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 仅当用户首次点击输入框，才触发动作
		if (unused) {
			comp.setText("");
			comp.setForeground(Color.BLACK);
			unused = false;
		}
	}

}