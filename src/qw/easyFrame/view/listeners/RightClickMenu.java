package qw.easyFrame.view.listeners;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;

/**
 * 鼠标右键菜单（鼠标右键监听器）.
 * @author Rex
 *
 */
public non-sealed class RightClickMenu extends AbstractMouseListener {
	
	/**
	 * 右键菜单.
	 */
	protected JPopupMenu menu = new JPopupMenu(null);
	
	private Component invoker;
	
	private Font font;

	public RightClickMenu(Component invoker, Font font) {
		this.invoker = invoker;
		this.font = font;
	}
	
	/**
	 * 添加菜单项.
	 * @param text 菜单文字
	 * @param e
	 */
	public JMenuItem addItem(String text, ActionListener e) {
		// 新建菜单项
		var item = new JMenuItem(text, null);
		item.setFont(this.font);
		// 为单个菜单项绑定鼠标单击事件
		item.addActionListener(e);
		// 将单个菜单项添加到右键菜单列表
		this.menu.add(item);
		
		return item;
	}

	/**
	 * 监听鼠标点击.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// 获取鼠标右键点击的坐标，使右键菜单在用户点击的位置显示
		if (e.getButton() == MouseEvent.BUTTON3)
			menu.show(invoker, e.getX(), e.getY());
	}
	
}