package qw.easyFrame.view.listeners;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;

/**
 * 鼠标悬停时，更改组件颜色.
 * @author Rex
 */
public non-sealed class ComponentListener extends AbstractMouseListener {
	
	private static final class Pool {
		
		/**
		 * 蓝色偏灰.
		 */
		private static final Color BLUE_GRAY = new Color(184, 207, 229, 255);
		
	}

	protected Component cpt;// 组件
	protected Color bgColor;// 当鼠标滑动到按钮上方时的按钮背景色
	protected Color fgColor;// 当鼠标滑动到按钮上方时的按钮前景色

	/**
	 * 使用默认设置的构造器.<br>
	 * 默认背景色：蓝色偏灰；<br>
	 * 默认前景色：灰色。
	 * @param btn
	 */
	public ComponentListener(Component btn) {
		this(btn, Pool.BLUE_GRAY, Color.GRAY);
	}
	
	/**
	 * 全参构造器.
	 * @param btn
	 * @param bgColor 当鼠标滑动到按钮上方时的按钮背景色
	 * @param fgColor 当鼠标滑动到按钮上方时的按钮前景色
	 */
	public ComponentListener(Component btn, Color bgColor, Color fgColor) {
		this.cpt = btn;
		this.bgColor = bgColor;
		this.fgColor = fgColor;
	}

	public Component getComponent() {
		return cpt;
	}

	public void setComponent(Component btn) {
		this.cpt = btn;
	}

	public Color getBgColor() {
		return bgColor;
	}

	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	public Color getFgColor() {
		return fgColor;
	}

	public void setFgColor(Color fgColor) {
		this.fgColor = fgColor;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		cpt.setBackground(bgColor);
		cpt.setForeground(fgColor);
	}

	/**
	 * 恢复按钮默认颜色.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		cpt.setBackground(null);
		cpt.setForeground(null);
	}

}