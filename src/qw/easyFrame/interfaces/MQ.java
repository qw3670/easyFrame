package qw.easyFrame.interfaces;

import java.io.Closeable;

/**
 * MessageQueue接口.
 * @author Rex
 * @param <T>
 */
public interface MQ<T> extends Closeable {

	/**
	 * 获取队列容量上限.
	 * 注意：本接口不设setMaxSize(int)方法，
	 * 建议实现类将maxSize类变量修饰为为final，
	 * 并在构造其中传参赋值。
	 * @return
	 */
	int getMaxSize();
	
	/**
	 * 返回当前队列容量.
	 * @return
	 */
	int size();
	
	/**
	 * 添加元素到队列，若队列容量已达上限，则抛出一个异常.
	 * @param t
	 * @throws Exception
	 */
	void add(T t) throws Exception;
	
	/**
	 * 添加元素到队列，若队列容量已达上限，则返回false.
	 * @param t
	 * @return
	 */
	boolean offer(T t);
	
	/**
	 * 添加元素到队列，调用者无法通过此方法判断队列容量是否已达上限.
	 * @param t
	 */
	void put(T t);
	
	/**
	 * 启动消息队列.
	 */
	void start();
	
	/**
	 * 清空消息队列.
	 * 注意：清空队列不是关闭队列，关闭队列的方法请使用close()。
	 * 当然，也可以使这两个方法互相调用。
	 */
	void clear();
	
	@Override
	void close();
	
}