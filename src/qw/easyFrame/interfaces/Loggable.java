package qw.easyFrame.interfaces;

import qw.easyFrame.concurrent.Logger;
import qw.easyFrame.throwables.DefinedException;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 若用户自定义的异常类希望使用Logger写日志，需要实现此接口.
 * @author Rex
 * @see qw.easyFrame.concurrent.Logger
 * @see DefinedRuntimeException
 * @see DefinedException
 *
 */
public interface Loggable {

	/**
	 * 获取封装的异常信息.
	 * @return
	 */
	String getMessage();

	/**
	 * 获取对象生成时间.
	 * @return
	 */
	long getOccurredTime();
	
	/**
	 * 手动将对象加入队列.
	 */
	boolean enqueue();
	
	/**
	 * 判断该对象是否已加入日志记录器队列.
	 * @return
	 */
	boolean isQueued();
	
	/**
	 * 获取异常对象生成时的栈轨迹.
	 * @return
	 */
	StackTraceElement[] getStackTrace();
	
	/**
	 * 获取封装的Throwable对象.
	 * 必须保证返回值非空。
	 * 建议先调用super.getCause()，判断其返回值是否为null，
	 * 若不为null则直接返回，反之则返回对象自身（this）。
	 * @return
	 */
	default Throwable getNonNullCause() {
		var t = (Throwable) this;
		var cause = t.getCause();
		return cause == null ? t : cause;
	}
	
	/**
	 * 获取对象被创建时的栈轨迹.
	 * @return
	 */
	default String lastStackTrace() {
		var e = this.getStackTrace()[0];
		return e.getClassName().concat(".").concat(e.getMethodName());
	}
	
	/**
	 * 获取异常等级.
	 * 默认为-1（不区分等级）。
	 * @return
	 */
	default int getLevel() {
		return -1;
	}
	
	/**
	 * 获取异常对象的来源.
	 * 默认为null。
	 * @return
	 */
	default String fromDomain() {
		return null;
	}
	
	 /**
	  * 获取日志记录器对象.
	 * @return
	 */
	Logger getLogger();
	
}