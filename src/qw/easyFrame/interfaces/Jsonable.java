package qw.easyFrame.interfaces;

/**
 * 此接口的实现类通过toJson()返回json、通过jsonSize()返回该json的键值对数量.
 * @author Rex
 *
 */
public interface Jsonable {
	
	String toJson();
	
	int jsonSize();
	
}