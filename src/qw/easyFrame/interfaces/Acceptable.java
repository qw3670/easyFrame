package qw.easyFrame.interfaces;

import java.io.File;
import java.io.Serializable;
import javax.swing.filechooser.FileFilter;
import qw.easyFrame.view.EasyFileFilter;

/**
 * 此接口存在的目的，是为了配合EasyFileFilter使用.
 * @see EasyFileFilter
 * @author Rex
 *
 */
public interface Acceptable extends Serializable {

	/**
	 * @see FileFilter#accept(File)
	 * @see EasyFileFilter#accept(File)
	 * @param f
	 * @return
	 */
	boolean accept(File f);
	
}