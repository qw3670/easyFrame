package qw.easyFrame.interfaces;

import qw.easyFrame.concurrent.AsyncTasker;

/**
 * 封装待执行的异步任务.
 * @see AsyncTasker
 * @author Rex
 *
 */
public interface AsyncTask extends Runnable {

	/**
	 * 异步任务的具体实现方法.
	 */
	@Override
	void run();
	
	/**
	 * 获取线程优先级.<br>
	 * 此方法由子类自行决定是否重新实现。
	 */
	default int getThreadPriority() {
		return 5;
	}
	
	/**
	 * 判断执行任务的线程是否是守护线程.<br>
	 * 此方法由子类自行决定是否重新实现。
	 * @return
	 */
	default boolean isDaemon() {
		return false;
	}
	
	/**
	 * 获取线程名.
	 * @return
	 */
	default String getThreadName() {
		return "AsyncTask Thread";
	}
	
	/**
	 * 如何处理run()方法抛出的异常.<br>
	 * 默认是null，即不设置异常处理方式。
	 * 若出现异常会按照JVM的默认方式（在控制台打印错误信息并中断线程）处理。
	 * @return
	 */
	default ThrowHandler getExceptionHandler() {
		return null;
	}

}