package qw.easyFrame.interfaces;

import qw.easyFrame.tools.statics.StringUtil;

/**
 * 字符串查找接口.
 * @author Rex
 *
 */
public interface Indexable {

	/**
	 * 在母串中查找女串.<br>
	 * 若找到，则返回女串首次在母串中出现时的下标；
	 * 若未找到则返回-1。
	 * @param mother 母串
	 * @param daughter 女串
	 * @return
	 */
	default int indexOf(String mother, String daughter) {
		return indexOf(mother, daughter, 0);
	}
	
	/**
	 * 从母串的指定下标开始，查找女串.<br>
	 * 若找到，则返回女串在指定下标之后首次在母串中出现时的下标；
	 * 若未找到则返回-1。
	 * @param mother 母串
	 * @param daughter 女串
	 * @param fromindex 指定下标
	 * @return
	 */
	int indexOf(String mother, String daughter, int fromindex);
	
	/**
	 * 从母串的末尾开始查找女串.
	 * @param mother 母串
	 * @param daughter 女串
	 * @return
	 */
	default int lastIndexOf(String mother, String daughter) {
		mother = StringUtil.reverse(mother);
		daughter = StringUtil.reverse(daughter);
		return mother.length() - indexOf(mother, daughter, 0) - daughter.length();
	}
	
	/**
	 * 检测是否以某个特定字符串开头.
	 * @param str
	 * @param starts
	 * @return
	 */
	default boolean startsWith(String str, String starts) {
		return this.indexOf(str, starts, 0) == 0;
	}
	
	/**
	 * 检测是否以某个特定字符串结尾.
	 * @param str
	 * @param ends
	 * @return
	 */
	default boolean endsWith(String str, String ends) {
		str = StringUtil.reverse(str);
		ends = StringUtil.reverse(ends);
		return this.indexOf(str, ends, 0) == 0;
	}
	
}