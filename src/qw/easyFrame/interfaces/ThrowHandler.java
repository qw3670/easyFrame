package qw.easyFrame.interfaces;

import qw.easyFrame.throwables.DefinedException;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.StringUtil;

/**
 * 此接口用于设置异常处理方式.
 * @author Rex
 *
 */
public interface ThrowHandler {
	
	/**
	 * 不限制变长参数（Object... objs）的具体类型，可按需传入.<br>
	 * 可以是产生异常的Runnable对象、母线程对象、当前的实体类等。
	 * @param e 异常对象
	 * @param objs 按需传入对象
	 */
	void handle(Throwable e, Object... objs);
	
	/**
	 * 生成常用ThrowHandler对象（单例模式）的工厂类.
	 * @author Rex
	 *
	 */
	public final class ThrowHandlers {
		
		private static final class IGNORE {
			// 忽略异常
			private static final ThrowHandler H = (e, objs) -> {};
		}
		
		private static final class PRINT {
			// 在控制台打印异常信息
			private static final ThrowHandler H = (e, objs) -> e.printStackTrace();
		}
		
		private static final class PRINT_MSG {
			// 在控制台输出Exception.getMessage()返回的字符串
			private static final ThrowHandler H = (e, objs) -> System.out.println(e.getMessage());
		}
		
		private static final class PRINT_OBJS {
			// 在控制台输出对象信息
			private static final ThrowHandler H = (e, objs) -> {
				if (objs != null)
					for (Object obj : objs)
						System.out.println(StringUtil.toString(obj));
			};
		}
		
		private static final class CHECK_LOG {
			// 输出至DefinedException的日志（若之前未输出过）
			private static final ThrowHandler H = (e, objs) -> {
				// 当异常对象是DefinedException，且之前未输出日志
				if (e instanceof DefinedException de && !de.isQueued())
					de.enqueue();
				else
					new DefinedException(e);
			};
		}
		
		private static final class UNCHECK_LOG {
			// 输出至DefinedRuntimeException的日志（若之前未输出过）
			private static final ThrowHandler H = (e, objs) -> {
				// 当异常对象是DefinedRuntimeException，且之前未输出日志
				if (e instanceof DefinedRuntimeException dre && !dre.isQueued())
					dre.enqueue();
				else
					new DefinedRuntimeException(e);
			};
		}
		
		/**
		 * 忽略异常.
		 * @return
		 */
		public static final ThrowHandler newIgnore() {
			return IGNORE.H;
		}
		
		/**
		 * 在控制台打印异常信息
		 * @return
		 */
		public static final ThrowHandler newPrint() {
			return PRINT.H;
		}
		
		/**
		 * 在控制台输出Exception.getMessage()返回的字符串.
		 * @return
		 */
		public static final ThrowHandler newPrintMsg() {
			return PRINT_MSG.H;
		}
		
		/**
		 * 在控制台输出对象信息.
		 * @return
		 */
		public static final ThrowHandler newPrintObjs() {
			return PRINT_OBJS.H;
		}
		
		/**
		 * 输出至DefinedException的日志（若之前未输出过）.
		 * @return
		 */
		public static final ThrowHandler newCheckLog() {
			return CHECK_LOG.H;
		}
		
		/**
		 * 输出至DefinedRuntimeException的日志（若之前未输出过）..
		 * @return
		 */
		public static final ThrowHandler newUncheckLog() {
			return UNCHECK_LOG.H;
		}
		
	}
	
}