package qw.easyFrame.interfaces;

import java.io.Serializable;
import java.util.Iterator;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import qw.easyFrame.tools.entity.EasyIterator;
import qw.easyFrame.tools.entity.EasyJson;

/**
 * 若实体类需要使用ExcelBuilder将自身储存的信息输出到Excel表格，则需要实现此接口.<br>
 * 注意：此接口的实现类，对应Excel表中的某个单行的内容，而非整张Excel表的内容。
 * @author Rex
 *
 */
public interface ExcelTuple extends Serializable, Iterable<Entry<String, String>>, Jsonable {

	/**
	 * 输出Excel表头.<br>
	 * 注意：若无数据，应当返回一个长度为0的数组，强烈不建议返回null。
	 * @return
	 */
	String[] toExcelHead();
	
	/**
	 * 将对象转换为ExcelBuilder所需的excel元组.
	 * 与toExcelHead()方法输出的Excel表头对应。<br>
	 * 注意：若无数据，应当返回一个长度为0的数组，强烈不建议返回null。
	 * @return
	 */
	String[] toExcelTuple();
	
	@Override
	default Iterator<Entry<String, String>> iterator(){
		return new EasyIterator<>(this.toExcelEntries());
	}
	
	@Override
	default String toJson() {
		return EasyJson.toJson(this.toExcelHead(), this.toExcelTuple());
	}
	
	@Override
	default int jsonSize() {
		return this.toExcelHead().length;
	}
	
	/**
	 * 输出表头与元组数据一一对应的Entry列表.<br>
	 * 表头为Entry的key，对应的单个的元组数据为Entry的value。<br>
	 * 注意：若表头数组长度与元组数组长度不一致，
	 * 则返回的Entry数组长度为前两者中较长的长度。
	 * 缺失的表头或元组数据，在Entry中会被置为空字符串（""）。
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Entry<String, String>[] toExcelEntries() {
		// 返回值数组
		Entry<String, String>[] array;

		var head = this.toExcelHead();
		var tuple = this.toExcelTuple();
		var headLen = head.length;
		var tupleLen = tuple.length;

		if (headLen == tupleLen) {
			array = new Entry[headLen];
			for (var i = 0; i < headLen; i++)
				array[i] = new SimpleEntry<String, String>(head[i], tuple[i]);
		} else {
			// 取前两者中较长的长度为返回值数组的长度
			var arrLen = headLen > tupleLen ? headLen : tupleLen;
			array = new Entry[arrLen];
			for (var i = 0; i < arrLen; i++) {
				// 当返回值数组下标超过源数组下标时，使用空字符串代替
				var key = i < headLen ? head[i] : "";
				var value = i < tupleLen ? tuple[i] : "";
				array[i] = new SimpleEntry<String, String>(key, value);
			}
		}

		return array;
	}
	
}