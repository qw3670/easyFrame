package qw.easyFrame.interfaces;

import java.io.Serializable;

/**
 * 消息队列消费者接口.
 * @author Rex
 * @param <T>
 */
public interface MQRunnable<T> extends Serializable {
	
	/**
	 * 封装消费者的业务逻辑，此方法会被消费者线程自动调用.
	 * @param t
	 */
	void run(T t);
	
}