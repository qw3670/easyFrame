package qw.easyFrame.interfaces;

import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.List;
import qw.easyFrame.view.listeners.RightClickMenu;

/**
 * 文本显示区域接口.
 * @author Rex
 *
 */
public interface EasyTextComponent {
	
	void setText(String text);
	
	void append(String text);
	
	void clear();
	
	/**
	 * 以List形式获取所有文本.
	 * @return
	 */
	List<String> getTexts();
	
	/**
	 * 获取文本行数.
	 * @return
	 */
	default int getRowsSize() {
		return getTexts().size();
	}
	
	/**
	 * 按行号获取对应文字. <br>
	 * 若未找到则返回一个null，
	 * 以便于区分“按行号找到的字符串是空字符串”和“按行号没有找到字符串”这两种情况。
	 * @param index
	 * @return
	 */
	default String getTextByRow(int index) {
		var texts = getTexts();
		return (index < 0 || index >= texts.size()) ? null : texts.get(index);
	}
	
	/**
	 * 添加默认右键菜单.
	 * @param font
	 */
	default void defaultRightClickMenu(Font font) {
		var component = (Component) this;
		var menu = new RightClickMenu((Component) this, font);
		menu.addItem("清空", e -> clear());
		// 系统剪贴板
		var board = Toolkit.getDefaultToolkit().getSystemClipboard();
		menu.addItem("复制全部", e -> board.setContents(new StringSelection(getTextsString()), null));
		component.addMouseListener(menu);
	}
	
	/**
	 * 以字符串形式获取所有文本.
	 * @return
	 */
	default String getTextsString() {
		var builder = new StringBuilder();

		synchronized (this) {
			final var list = getTexts();
			for (var str : list)
				builder.append(str);
		}

		return builder.toString();
	}
	
}