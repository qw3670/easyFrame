package qw.easyFrame.interfaces;

/**
 * 定义如何处理Loggable对象的接口.
 * @author Rex
 *
 */
public interface Processable {

	void process(Loggable loggable);
	
}