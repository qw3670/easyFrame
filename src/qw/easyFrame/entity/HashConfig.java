package qw.easyFrame.entity;

import java.io.Serializable;
import qw.easyFrame.interfaces.Jsonable;
import qw.easyFrame.tools.algorithm.EasyHash;
import qw.easyFrame.tools.entity.EasyJson;

/**
 * EasyHash�������ļ�.
 * @author Rex
 *	@see EasyHash
 */
public record HashConfig(int times, int saltLen, String fixed, String charset) implements Serializable, Jsonable {

	@Override
	public String toJson() {
		var json = new EasyJson();
		json.add("times", times);
		json.add("saltLen", saltLen);
		json.add("fixed", fixed);
		json.add("charset", charset);
		return json.toJson();
	}

	@Override
	public int jsonSize() {
		return 4;
	}

}