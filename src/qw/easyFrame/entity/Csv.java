package qw.easyFrame.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import qw.easyFrame.interfaces.ExcelTuple;
import qw.easyFrame.tools.entity.EasyJson;
import qw.easyFrame.tools.statics.NumberUtil;

/**
 * 用于保存csv文件信息的实体类.
 * @author Rex
 *
 */
public class Csv implements Serializable {
	private static final long serialVersionUID = 20230616L;
	
	/**
	 * csv文件的全部信息（含表头）.
	 */
	private List<String[]> body;
	
	/**
	 * 表头.
	 */
	private String[] heads;
	
	public Csv() {
		super();
	}

	public Csv(List<String[]> body) {
		this.body = body;
		this.heads = body.get(0);
	}

	public List<String[]> getbody() {
		return body;
	}
	public void setList(List<String[]> body) {
		this.body = body;
		this.heads = body.get(0);
	}
	
	public String[] getHeads() {
		return heads;
	}
	
	/**
	 * 获取csv文件总行数（含表头）.
	 * @return
	 */
	public int getSize() {
		return body.size();
	}

	/**
	 * 按行获取csv中保存的信息，以字符串数组形式返回.<br>
	 * 注意：表头为第0行。
	 * @param index
	 * @return
	 */
	public String[] get(int index) {
		return body.get(index);
	}
	
	/**
	 * 输出为Excelable列表.
	 * @return
	 */
	public final List<ExcelTuple> toExcelables() {
		var size = body.size();

		var list = new ArrayList<ExcelTuple>(size - 1);
		for (var i = 1; i < size; i++) {
			final var index = i;
			list.add(new ExcelTuple() {
				private static final long serialVersionUID = 1L;

				@Override
				public String[] toExcelTuple() {
					return body.get(index);
				}

				@Override
				public String[] toExcelHead() {
					return heads;
				}
			});
		}

		return list;
	}
	
	/**
	 * 按行获取csv中保存的信息，以int数组形式返回.<br>
	 * 注意：表头为第0行。<br>
	 * 注意：无法转换为int值的数据将转换为0。<br>
	 * @param index
	 * @return
	 */
	public int[] getInts(int index) {
		var strs = body.get(index);
		
		var len = strs.length;
		var ints = new int[len];
		for (var i = 0; i < len; i++)
			ints[i] = NumberUtil.parseInt(strs[i].strip(), 0);
		
		return ints;
	}
	
	/**
	 * 按行获取csv中保存的信息，以double数组形式返回.<br>
	 * 注意：表头为第0行。<br>
	 * 注意：无法转换为double值的数据将转换为0.0<br>
	 * @param index
	 * @return
	 */
	public double[] getDoubles(int index) {
		var strs = body.get(index);

		var len = strs.length;
		var ds = new double[len];
		for (var i = 0; i < len; i++)
			try {
				ds[i] = Double.parseDouble(strs[i].strip());
			} catch (Exception e) {
				ds[i] = 0.0;
			}

		return ds;
	}
	
	/**
	 * 按行获取csv中保存的信息，以long数组形式返回.<br>
	 * 注意：表头为第0行。<br>
	 * 注意：无法转换为long值的数据将转换为0<br>
	 * @param index
	 * @return
	 */
	public long[] getLongs(int index) {
		var strs = body.get(index);

		var len = strs.length;
		var ds = new long[len];
		for (var i = 0; i < len; i++)
			try {
				ds[i] = Long.parseLong(strs[i].strip(), 10);
			} catch (Exception e) {
				ds[i] = 0;
			}

		return ds;
	}
	
	/**
	 * 按行获取csv中保存的信息，以json字符串形式返回.<br>
	 * 注意：表头为第0行。
	 * @param index
	 * @return
	 */
	public String getJson(int index) {
		return EasyJson.toJson(heads, body.get(index));
	}
	
	public void clear() {
		body.clear();
		heads = null;
	}
	
}