package qw.easyFrame.entity;

import java.io.Serializable;
import qw.easyFrame.interfaces.MQRunnable;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 封装消息队列消费者以及消费者线程有关的信息.
 * @author Rex
 * @param <T>
 *
 */
public class MQCustomer<T> implements Serializable, Cloneable {
	private static final long serialVersionUID = 20230310L;
	
	private MQRunnable<T> runnable;// 消息队列消费者
	
	private int threadPriority;// 线程优先级
	private boolean daemon;// 标记是否是守护线程
	private String threadName;// 线程名

	public MQCustomer() {
		super();
	}
	
	/**
	 * @param runnable 消费者业务逻辑
	 */
	public MQCustomer(MQRunnable<T> runnable) {
		this(runnable, 5, false, "MQCustomer");
	}
	
	/**
	 * @param threadPriority 线程优先级
	 * @param daemon 标记消费者线程是否为守护线程
	 * @param threadName 线程名
	 */
	public MQCustomer(int threadPriority, boolean daemon, String threadName) {
		this(null, threadPriority, daemon, threadName);
	}
	
	/**
	 * @param runnable 消费者业务逻辑
	 * @param threadPriority 线程优先级
	 * @param daemon 标记消费者线程是否为守护线程
	 * @param threadName 线程名
	 */
	public MQCustomer(MQRunnable<T> runnable, int threadPriority, boolean daemon, String threadName) {
		this.runnable = runnable;
		this.threadPriority = threadPriority;
		this.daemon = daemon;
		this.threadName = threadName;
	}
	
	/**
	 * 此克隆方法是浅克隆.
	 */
	@Override
	public Object clone() {
		try {
			return (MQCustomer<?>) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new DefinedRuntimeException(false, "克隆MQCustomer对象失败", e);
		}
	}

	public boolean isDaemon() {
		return daemon;
	}
	public void setDaemon(boolean daemon) {
		this.daemon = daemon;
	}
	public int getThreadPriority() {
		return threadPriority;
	}
	public void setThreadPriority(int threadPriority) {
		this.threadPriority = threadPriority;
	}
	public MQRunnable<T> getRunnable() {
		return runnable;
	}
	public void setRunnable(MQRunnable<T> runnable) {
		this.runnable = runnable;
	}
	public String getThreadName() {
		return threadName;
	}
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

}