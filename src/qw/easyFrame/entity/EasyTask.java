package qw.easyFrame.entity;

import qw.easyFrame.interfaces.AsyncTask;
import qw.easyFrame.interfaces.ThrowHandler;

/**
 * AsyncTask接口的简单实现.
 * @author Rex
 *
 */
public class EasyTask implements AsyncTask {
	
	/**
	 * 待执行的任务.
	 */
	private Runnable task;
	
	/**
	 * 线程优先级.
	 */
	private int threadPriority;
	
	/**
	 * 是否设置执行任务的线程为守护线程.
	 */
	private boolean daemon;
	
	/**
	 * 线程名.
	 */
	private String threadName;
	
	/**
	 * 异常处理器，定义如何处理run()方法抛出的异常.
	 */
	private ThrowHandler handler;
	
	/**
	 * 线程优先级默认为5，默认非守护线程，使用默认线程名，不处理抛出的异常（继续向上层抛出）.
	 * @param task
	 */
	public EasyTask(Runnable runnable) {
		this(runnable, 5, false, "EasyTask Thread", null);
	}
	
	/**
	 * 线程优先级默认为5，默认非守护线程，使用默认线程名.
	 * @param task
	 * @param handler
	 */
	public EasyTask(Runnable runnable, ThrowHandler handler) {
		this(runnable, 5, false, "EasyTask Thread", handler);
	}
	
	/**
	 * 完全参照AsyncTask对象重构一个EasyTask对象.
	 * @param task
	 */
	public EasyTask(AsyncTask task) {
		this(task, task.getThreadPriority(), task.isDaemon(), task.getThreadName(), task.getExceptionHandler());
	}
	
	/**
	 * 参照Thread对象重构一个EasyTask对象，不处理抛出的异常（继续向上层抛出）.
	 * @param thread
	 */
	public EasyTask(Thread thread) {
		this(thread, thread.getPriority(), thread.isDaemon(), thread.getName(), null);
	}
	
	/**
	 * 参照Thread对象重构一个EasyTask对象，并自定义异常处理方式.
	 * @param thread
	 * @param handler
	 */
	public EasyTask(Thread thread, ThrowHandler handler) {
		this(thread, thread.getPriority(), thread.isDaemon(), thread.getName(), handler);
	}
	
	/**
	 * 全参构造器.
	 * @param task
	 * @param threadPriority
	 * @param daemon
	 * @param threadName
	 * @param handler
	 */
	public EasyTask(Runnable task, int threadPriority, boolean daemon, String threadName, ThrowHandler handler) {
		this.task = task;
		this.threadPriority = threadPriority;
		this.daemon = daemon;
		this.threadName = threadName;
		this.handler = handler;
	}

	@Override
	public void run() {
		this.task.run();
	}

	@Override
	public int getThreadPriority() {
		return threadPriority;
	}

	@Override
	public boolean isDaemon() {
		return daemon;
	}
	
	@Override
	public String getThreadName() {
		return threadName;
	}
	
	@Override
	public ThrowHandler getExceptionHandler() {
		return handler;
	}

	public void setThreadPriority(int threadPriority) {
		this.threadPriority = threadPriority;
	}

	public void setDaemon(boolean daemon) {
		this.daemon = daemon;
	}
	
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
	
	public void setExceptionHandler(ThrowHandler handler) {
		this.handler = handler;
	}

}