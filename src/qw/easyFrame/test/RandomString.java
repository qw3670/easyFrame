package qw.easyFrame.test;

import qw.easyFrame.tools.algorithm.MTRandomFast;

/**
 * 生成随机字符串.
 * @author Rex
 *
 */
public class RandomString {
	
	/**
	 * 若此项为true，则根据给定的长度参数生成固定长度的字符串；
	 * 反之，则生成的字符串长度会在给定的长度参数范围内随机变化。<br>
	 * 注意：字符串长度变化的取值范围是1~给定的参数值，例如给定的长度参数为5，则取值范围是1~5，而非0~4。
	 */
	private boolean stable;
	
	/**
	 * 随机数生成器.
	 */
	private MTRandomFast random;

	/**
	 * 随机字符的字典.
	 */
	private final static char[] DICT = { '4', 'N', 'h', '|', '(', '8', '>', 'O', '-', 'q', 'v', 'A', ']', 'r', 'E', '{',
			'S', '3', '6', 'H', 's', '\'', 'a', '5', 'B', '@', 'D', ';', 't', 'F', 'm', '9', 'i', 'y', 'j', '*', '0',
			'V', 'Y', '&', '%', ',', 'p', '~', '\\', '=', 'P', 'I', 'd', 'W', 'L', 'n', 'Q', 'e', 'u', '+', '/', '$',
			'z', ':', '1', 'T', 'x', 'U', '7', 'l', '_', '[', '}', '?', 'b', 'R', '"', '<', 'X', 'c', '.', 'G', '#',
			'C', 'k', '2', 'Z', 'J', 'w', '`', 'f', 'K', '^', '!', 'M', 'o', ')', 'g', ' ' };
	
	/**
	 * 适于阅读的随机字符的字典.
	 */
	private static final char[] DICT_FOR_READ = { '0', '1', '4', '7', '8', '5', '2', '3', '6', '9', 'Q', 'W', 'E', 'R',
			'T', 'Y', 'U', 'O', 'P', 'L', 'K', 'J', 'H', 'G', 'F', 'D', 'S', 'A', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',
			'n', 'b', 'j', 'h', 'g', 'f', 'd', 'a', 'e', 'r', 't', 'y', 'i' };
	
	/**
	 * 适用于Windows文件名的随机字符的字典.
	 */
	private final static char[] DICT_FOR_WIN_FILE = { '4', 'N', 'h', '(', '8', 'O', '-', 'q', 'v', 'A', ']', 'r', 'E',
			'{', 'S', '3', '6', 'H', 's', '\'', 'a', '5', 'B', '@', 'D', ';', 't', 'F', 'm', '9', 'i', 'y', 'j', '0',
			'V', 'Y', '&', '%', ',', 'p', 'I', '~', '=', 'P', 'd', 'W', 'L', 'n', 'Q', 'e', 'u', '+', '$', 'z', '1',
			'T', 'x', 'U', '7', '_', '[', '}', 'b', 'R', 'X', 'c', '.', 'G', '#', 'C', 'k', '2', 'Z', 'J', 'w', '`',
			'f', 'K', '^', '!', 'M', 'o', ')', 'g' };
	
	/**
	 * 无符号字符的随机字符的字典.
	 */
	private final static char[] DICT_NO_SYMBOL = { 'j', '0', 'R', 'z', 'W', '6', '7', 'h', 'u', 'K', 'I', 'i', 'O', 'P', 'm',
			'V', '9', 'e', 'B', 'w', 'a', 'g', 'b', 'C', 'r', 'Y', 'X', 's', 'c', 'd', 'T', 'y', 'q', 'n', 'l', 'E',
			'o', 'H', 't', 'L', 'f', 'p', 'M', 'J', 'F', 'D', '1', '4', 'Z', '2', '3', 'k', 'x', 'A', 'v', 'N', '5',
			'U', 'Q', 'G', 'S', '8' };
	
	/**
	 * 纯数字的字典.
	 */
	private final static char[] NUMS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	
	/**
	 * 若stable为true，则根据给定的长度参数生成固定长度的字符串；
	 * 反之，则生成的字符串长度会在给定的长度参数范围内随机变化.<br>
	 * 注意：字符串长度变化的取值范围是1~给定的参数值，例如给定的长度参数为5，则取值范围是1~5，而非0~4。
	 * @param stable
	 */
	public RandomString(boolean stable) {
		this.stable = stable;
		this.random = MTRandomFast.inThreadLocal();
	}
	
	/**
	 * 自由生成指定长度的随机字符串.
	 * @param length
	 * @return
	 */
	public String random(int length) {
		final var dict = DICT;
		return create(length, dict);
	}
	
	/**
	 * 生成指定长度的适于阅读的随机字符串.<br>
	 * 可用于生成图片验证码等需要便于肉眼阅读和键盘输入的随机字符。
	 * @param length
	 * @return
	 */
	public String forRead(int length) {
		final var dict = DICT_FOR_READ;
		return create(length, dict);
	}
	
	/**
	 * 生成适用于Windows文件名的随机字符串.<br>
	 * 排除了不能再Windows文件系统中出现的字符。
	 * @param length
	 * @return
	 */
	public String forWinFile(int length) {
		final var dict = DICT_FOR_WIN_FILE;
		return create(length, dict);
	}
	
	/**
	 * 生成无符号字符的随机字符串.
	 * @param length
	 * @return
	 */
	public String noSymbol(int length) {
		final var dict = DICT_NO_SYMBOL;
		return create(length, dict);
	}
	
	/**
	 * 生成随机数字字符串.
	 * @param length
	 * @return
	 */
	public String numbers(int length) {
		final var dict = NUMS;
		return create(length, dict);
	}

	/**
	 * 生成指定长度的随机字符串.
	 * @param length
	 * @return
	 */
	private final String create(int length, char[] dict) {
		final int len;
		final char[] salt;
		if (stable)
			salt = new char[len = length];
		else
			salt = new char[len = random.nextInt(length) + 1];

		final var size = dict.length;
		for (var i = 0; i < len; i++)
			salt[i] = dict[random.nextInt(size)];

		return new String(salt);
	}
	
}