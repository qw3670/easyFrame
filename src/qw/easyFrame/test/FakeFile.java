package qw.easyFrame.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.algorithm.MTRandomFast;

/**
 * 制造虚假文件.<br>
 * 为使生成的虚假文件更加真实，本程序按调用者指定的文件大小（整数）生成的文件会额外增加1MiB以内的体积。<br>
 * 例如：传入的文件大小为95，最终生成的文件大小可能为95.6MiB。
 * @author Rex
 *
 */
public class FakeFile {
	
	/**
	 * 是否将生成的伪文件全部设置为只读.<br>
	 * 默认为false。
	 */
	private boolean readOnly;
	
	/**
	 * 是否在伪文件末尾添加小于1MiB的额外数据.
	 */
	private boolean surplus;
	
	private MTRandomFast ran;
	
	private boolean isForZip;

	/**
	 * @param surplus 是否在伪文件末尾添加小于等于1MiB的额外数据
	 */
	public FakeFile(boolean surplus) {
		this.ran = MTRandomFast.inThreadLocal();
		this.isForZip = true;
		this.surplus = surplus;
		this.readOnly = false;
	}
	
	/**
	 * 是否将生成的伪文件全部设置为只读.
	 * @return
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * 是否将生成的伪文件全部设置为只读.
	 * @param readOnly
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * 在指定的文件夹下批量生成文件.
	 * @param dir 母文件夹
	 * @param Mibs 每个文件的大致体积
	 * @param count 文件数
	 */
	public void batchCreate(File dir, int MiBs, int count) {
		if (dir.isDirectory() && count > 0 && MiBs > 0) {
			var s = new RandomString(false);
			for (int i = 0; i < count; i++) {
				var subFile = s.forRead(10).concat(".").concat(s.noSymbol(3).toLowerCase());
				balancedCreate(new File(dir, subFile), MiBs);
			}
		}
	}

	/**
	 * 此方法制造出的文件有50%的概率极易被zip程序压缩.
	 * @param file 生成的文件
	 * @param MiBs 文件大小
	 */
	public void balancedCreate(File file, int MiBs) {
		if (isForZip)
			forZip(file, MiBs);
		else
			notForZip(file, MiBs);

		isForZip = !isForZip;
	}
	
	/**
	 * 此方法制造出的文件极易被zip程序压缩.
	 * @param file 生成的文件
	 * @param MiBs 文件大小
	 */
	public void forZip(File file, int MiBs) {
		if (MiBs > 0)
			createFile(file, new byte[128 * 128], 64 * MiBs);
	}
	
	/**
	 * 此方法制造出的文件极难被zip程序压缩.
	 * @param file 生成的文件
	 * @param MiBs 文件大小
	 */
	public void notForZip(File file, int MiBs) {
		if (MiBs > 0)
			createFile(file, new byte[1024 * 1024 * MiBs], 1);
	}
	
	/**
	 * 生成伪装文件.
	 * @param file
	 * @param bs
	 * @param times
	 */
	private final void createFile(File file, byte[] bs, int times) {
		var parentFile = file.getParentFile();
		if (!parentFile.exists())
			parentFile.mkdirs();

		try (var fos = new FileOutputStream(file, false)) {
			ran.nextBytes(bs);

			for (var i = 0; i < times; i++)
				fos.write(bs);

			if (surplus) {
				bs = new byte[(ran.nextInt(1023) + 1) * 1024];
				ran.nextBytes(bs);
				fos.write(bs);
			}

			fos.flush();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "FakeFile写文件失败", e);
		} finally {
			if (readOnly && file.exists())
				file.setReadOnly();
		}
	}
	
}