package qw.easyFrame.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import qw.easyFrame.DAO.EasyDataSource;
import qw.easyFrame.DAO.proxy.ResultSetProxy;
import qw.easyFrame.DAO.proxy.StatementProxy;
import qw.easyFrame.concurrent.AsyncTasker;
import qw.easyFrame.concurrent.EasyThreadPool;
import qw.easyFrame.concurrent.Logger;
import qw.easyFrame.entity.EasyTask;
import qw.easyFrame.entity.HashConfig;
import qw.easyFrame.enums.SecretKeySize;
import qw.easyFrame.enums.SelectionMode;
import qw.easyFrame.interfaces.ExcelTuple;
import qw.easyFrame.interfaces.ThrowHandler.ThrowHandlers;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.CloseSourceX;
import qw.easyFrame.tools.ExcelBuilder;
import qw.easyFrame.tools.HttpUtil;
import qw.easyFrame.tools.Serializer;
import qw.easyFrame.tools.TextFinder;
import qw.easyFrame.tools.algorithm.AESUtil;
import qw.easyFrame.tools.algorithm.CsvReader;
import qw.easyFrame.tools.algorithm.ELOUtil;
import qw.easyFrame.tools.algorithm.EasyHash;
import qw.easyFrame.tools.algorithm.MTRandom;
import qw.easyFrame.tools.algorithm.MTRandomFast;
import qw.easyFrame.tools.algorithm.RSAUtil;
import qw.easyFrame.tools.algorithm.SundayUtil;
import qw.easyFrame.tools.algorithm.ZipUtil;
import qw.easyFrame.tools.entity.EasyJson;
import qw.easyFrame.tools.entity.EasyStringArray;
import qw.easyFrame.tools.statics.CloseSource;
import qw.easyFrame.tools.statics.EncoderUtil;
import qw.easyFrame.tools.statics.IOUtil;
import qw.easyFrame.tools.statics.NumberUtil;
import qw.easyFrame.tools.statics.SortUtil;
import qw.easyFrame.tools.statics.StringUtil;
import qw.easyFrame.view.EasyFileChooser;
import qw.easyFrame.view.EasyFileFilter;
import qw.easyFrame.view.EasySplitPane;
import qw.easyFrame.view.EasyTextArea;
import qw.easyFrame.view.listeners.EggListener;
import qw.easyFrame.view.util.GUIUtil;
import static qw.easyFrame.tools.statics.NumberUtil.bigDecimal;

/**
 * 提供对整个项目源代码的兼容性测试.<br>
 * 此测试类只对项目的大部分重要功能进行测试，测试通过亦不保证项目与环境完全兼容。<br>
 * 注意：此测试类需依赖第三方jar包（所依赖的jar包即为本项目所依赖的jar包）;<br>
 * 注意：为了测试网络组件，需要联网。
 * 注意：测试时请勿移动鼠标！
 * @author Rex
 *
 */
public class CompatibilityTest implements ExcelTuple {
	private static final long serialVersionUID = 20241227L;
	
	private static final CloseSourceX CLOSER = new CloseSourceX(ThrowHandlers.newIgnore());
	
	private static volatile EasyThreadPool pool;
	
	private static volatile int flag;
	
	// Excel表头
	private static final String[] HEAD;
	
	static {
		var arr = new EasyStringArray();
		for (var i = 0; i < 10; i++)
			arr.add("Test");
		HEAD = arr.toArray();
	}
	
	private CompatibilityTest() {
		super();
	}
	
	public static void main(String[] args) {
		System.out.println("兼容性测试开始，请稍候……");
		
		// 多线程组件测试
		multiThreadTest("多线程组件测试完毕……");

		// 测试Logger及相关组件
		var dir = loggerTest("Logger及相关组件测试完毕……");
		
		// Excel读写测试
		excelTest("Excel读写测试完毕……", dir);
		
		// 网络功能测试
		httpTest("网络功能测试完毕……");
		
		// AES功能测试
		aesTest("AES组件测试完毕……", dir);
		
		// zip功能测试
		zipTest("zip功能测试完毕……", dir);
		
		// RSA功能测试
		rsaTest("RSA组件测试完毕……");
		
		// Swing组件测试
		guiTest("Swing组件测试完毕……", dir);
		
		MTRandomFast.removeFromThreadLocal();
		System.out.println("全部测试已完成");
		Runtime.getRuntime().exit(0);
	}
	
	/**
	 * 多线程组件测试.
	 * @param progressRate
	 */
	private static void multiThreadTest(String progressRate) {
		String info = "多线程组件有异常！";
		try {
			var tasker = new AsyncTasker(1);
			var task = new EasyTask(() -> pool = new EasyThreadPool(2));
			tasker.setTask(task);
			boolean start = tasker.start();

			if (start) {
				int times0 = 0;
				while (true) {
					++times0;
					// 若超过15秒未完成任务
					if (times0 == 101)
						throw new DefinedRuntimeException(false, "异步任务执行器执行任务超时！");

					if (pool != null) {
						pool.setExceptionHandler((e, r) -> ++flag);
						pool.execute(() -> {
							++flag;
							throw new DefinedRuntimeException(false);
						}, 3);

						break;
					}
					Thread.sleep(10);
				}

				int times1 = 0;
				while (true) {
					// 若超过5秒未完成任务
					if (times1 == 101)
						throw new DefinedRuntimeException(false, "线程池执行任务超时！");

					if (flag == 6) {
						info = progressRate;
						break;
					}
					Thread.sleep(50);
					++times1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pool.shutdown();
			System.out.println(info);
		}
	}
	
	/**
	 * Swing组件测试.
	 * @param progressRate
	 * @param dir
	 */
	private static void guiTest(String progressRate, File dir) {
		try {
			// 初始化变量（及必要的类）
			var btnMsg = """
					<html>测试<br>
					Test</html>
					""";
			var bigFont = new Font("Microsoft yahei", Font.PLAIN, 25);
			var smallFont = new Font("Microsoft yahei", Font.PLAIN, 15);
			var msg = new EasyTextArea(bigFont, Color.GRAY);
			var btn0 = GUIUtil.defaultJButton(btnMsg, bigFont);
			var btn1 = GUIUtil.defaultJButton(btnMsg, bigFont);
			var btn3 = GUIUtil.defaultJButton(btnMsg, bigFont);
			var btn4 = GUIUtil.defaultJButton("Test", bigFont);
			var btn5 = GUIUtil.defaultJButton("Test", bigFont);
			var frame = new JFrame("兼容性测试 - UI测试");
			var panel = GUIUtil.defaultJPanel(1, 4, 2, 2);
			var scroll = new JScrollPane(msg);
			var splitPane = new EasySplitPane(false, 50);

			// 按照布局添加窗口元素
			var cp = frame.getContentPane();
			panel.add(btn0);
			panel.add(btn1);
			// panel.add(btn2);
			panel.add(btn3);
			splitPane.add(btn4, btn5);
			panel.add(splitPane);
			cp.add(panel, BorderLayout.NORTH);
			cp.add(scroll, BorderLayout.CENTER);

			// 绑定鼠标监视器
			msg.addMouseListener(new EggListener(frame, smallFont));
			msg.defaultRightClickMenu(smallFont);
			btn4.addActionListener(e -> {
				var chooser = new EasyFileChooser(msg, SelectionMode.FILES_AND_DIRS, dir, false);
				chooser.setFilter(new EasyFileFilter("Test", f -> f.getName().endsWith(".jar")));
				chooser.setFont(smallFont);
				chooser.setPrompt("Test");
				chooser.choose();
			});
			
			// 填充文字
			var elo = new ELOUtil(bigDecimal(10), bigDecimal(5.5), bigDecimal("5"), 3);
			elo.setZeroed(true);
			elo.setIntMode(true);
			elo.aWin();
			elo.bWin();
			elo.draw();
			var raAndRb = elo.getRaAndRb();
			for (var r : raAndRb)
				msg.append(r.toString());
			msg.append(GUIUtil.getScreenSize(0)[0]);
			msg.append(new ResultSetProxy(null, null, null).toString());
			msg.append(new StatementProxy(null, null).toString());
			msg.append(EasyDataSource.createDruid(new File(Thread.currentThread().getContextClassLoader()
					.getResource("").getPath().concat("template_Druid.properties"))).toString());

			for (var temp : new CompatibilityTest())
				msg.append(temp.getKey().concat("：").concat(temp.getValue()));

			// 设置窗口
			GUIUtil.setJFrame(frame, 600, 500);
			
			// 模拟鼠标点击
			var robot = new Robot();
            var point = btn4.getLocationOnScreen();
            robot.delay(500);
            robot.mouseMove(point.x + 6, point.y + 6);
            robot.delay(100);
            robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            robot.delay(100);
            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            robot.delay(500);
            
            File screen = new File(GUIUtil.getDesktop(), "screen.png");
			boolean captureScreen = GUIUtil.captureScreen(screen);
			Thread.sleep(100);
			if (captureScreen == false || screen.exists() == false)
				throw new DefinedRuntimeException(false, "获取屏幕截图异常");
			
			boolean delete = screen.delete();
			if (delete == false)
				throw new DefinedRuntimeException(false, "获取屏幕截图异常");
		} catch (Exception e) {
			progressRate = "Swing组件兼容性有异常！";
			e.printStackTrace();
		} finally {
			System.out.println(progressRate);
		}
	}
	
	/**
	 * RSA功能测试.
	 * @param progressRate
	 */
	private static void rsaTest(String progressRate) {
		var info = "RSAUtil及相关组件兼容性有异常！";
		RSAUtil rsa = null;
		try {
			final var source = "easyFrame.CompatibilityTest";
			// 生成随机字符串
			var em = new EasyHash(new HashConfig(3, 9, null, null));
			var hash = em.digest(source);

			if (em.matches(source, hash)) {
				rsa = new RSAUtil(SecretKeySize.RSA_2048);

				// 加密再解密
				var encrypt = rsa.encrypt(hash.getBytes());
				var decrypt = rsa.decrypt(encrypt);

				// 判断解密结果
				if (hash.equals(new String(decrypt)) && em.matches(source, hash))
					info = progressRate;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseSource.close(rsa);
			System.out.println(info);
		}
	}
	
	/**
	 * zip功能测试.
	 * @param progressRate
	 * @param dir
	 */
	private static void zipTest(String progressRate, File dir) {
		var info = "ZipUtil及相关组件兼容性有异常！";
		File f1 = null;
		File f2 = null;
		File dest1 = null;
		File dest2 = null;
		Serializer ser = null;
		try {
			f1 = new File(dir, "zipS1.txt");
			f2 = new File(dir, "zipS2.txt");
			dest1 = new File(dir, "zipD1.zip");
			ser = new Serializer("Test");

			var ranStr = new RandomString(true);
			
			// 生成文件
			IOUtil.write(StringUtil.desensitize(ranStr.random(20), 1, 3).getBytes(), f1, false);
			var fake = new FakeFile(true);
			fake.balancedCreate(f2, 10);

			// 序列化ZipUtil工具类再反序列化
			var zipUtil = new ZipUtil(dest1, f1, f2);
			var base64 = ser.objToBase64(zipUtil);
			var obj = (ZipUtil) ser.base64ToObj(base64);

			// 压缩文件再解压缩
			obj.compress();
			obj.setSrc(dest1);
			obj.setDestFile(dir);
			obj.decompress();

			// 判断文件是否存在
			if (dest1.exists() && (dest2 = new File(dir, "zipD1")).exists())
				info = progressRate;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseSource.close(ser);
			IOUtil.delete(f1);
			IOUtil.delete(f2);
			IOUtil.delete(dest1);
			IOUtil.delete(dest2);
			System.out.println(info);
		}
	}
	
	/**
	 * AES功能测试.
	 * @param progressRate
	 * @param dir
	 */
	private static void aesTest(String progressRate, File dir) {
		var info = "AESUtil及相关组件兼容性有异常！";
		File srcEcb = null;
		File targetEcb = null;
		File targetEcb2 = null;
		File srcCbc = null;
		File targetCbc = null;
		File targetCbc2 = null;
		AESUtil aesEcb = null;
		AESUtil aesCbc = null;
		try {
			srcEcb = new File(dir, "TestEcb.txt");
			targetEcb = new File(dir, "TestAESEcb.txt");
			targetEcb2 = new File(dir, "TestAESEcb2.txt");
			
			srcCbc = new File(dir, "TestCbc.txt");
			targetCbc = new File(dir, "TestAESCbc.txt");
			targetCbc2 = new File(dir, "TestAESCbc2.txt");
			
			aesEcb = new AESUtil("Test", SecretKeySize.AES_ECB_256);
			aesCbc = new AESUtil("Test", SecretKeySize.AES_CBC_256);
			aesCbc.setRandomIv();
			
			// 生成文件
			var md5 = EncoderUtil.md5(EncoderUtil.encode("Test".getBytes()));
			var bytes = md5.getBytes();
			IOUtil.write(bytes, srcEcb, false);
			IOUtil.write(bytes, srcCbc, false);
			
			// 加密解密
			aesEcb.encrypt(srcEcb, targetEcb);
			aesEcb.decrypt(targetEcb, targetEcb2);
			
			aesCbc.encrypt(srcCbc, targetCbc);
			aesCbc.decrypt(targetCbc, targetCbc2);
			
			// 判断解密结果
			var result = new String(IOUtil.read(targetEcb2)).strip();
			var result2 = new String(IOUtil.read(targetCbc2)).strip();
			if (md5.equals(result) && md5.equals(result2))
				info = progressRate;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseSource.close(aesEcb, aesCbc);
			IOUtil.delete(srcEcb);
			IOUtil.delete(targetEcb);
			IOUtil.delete(targetEcb2);
			IOUtil.delete(srcCbc);
			IOUtil.delete(targetCbc);
			IOUtil.delete(targetCbc2);
			System.out.println(info);
		}
		
	}
	
	/**
	 * 网络功能测试.
	 * @param progressRate
	 */
	private static void httpTest(String progressRate) {
		var info = "HttpUtil及相关组件兼容性有异常！";
		HttpUtil http = null;
		try {
			// 执行get请求并获取服务器响应
			http = new HttpUtil();
			var responseBody = http.get("https://www.baidu.com/");

			// 判断服务器响应是否正确
			if (SundayUtil.indexOf(responseBody, "<html") > -1)
				info = progressRate;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println(info);
		}
	}

	/**
	 * Excel读写测试.
	 * @param progressRate
	 * @param dir
	 * @throws IOException 
	 * @throws  
	 */
	private static void excelTest(String progressRate, File dir) {
		var info = "Excel相关组件兼容性有异常！";
		File xlsx = null;
		File csv = null;
		Workbook excel = null;
		CsvReader reader = null;
		try {
			xlsx = new File(dir, "Test.xlsx");
			csv = new File(dir, "Test.csv");
			
			// 生成xlsx文件
			var eb = new ExcelBuilder<>(xlsx.getAbsolutePath());
			var list = new ArrayList<ExcelTuple>();
			for (var i = 0; i < 10; i++)
				list.add(new CompatibilityTest());
			eb.addListAndSheetName(list, "sht1");
			eb.setSpColumns(0, 1);
			eb.xlsxWrite();
			
			// 生成csv文件
			eb.setFile(csv.getAbsolutePath());
			eb.write();
			
			// 读取xlsx文件
			excel = WorkbookFactory.create(xlsx);
			var cell = excel.getSheetAt(0).getRow(0).getCell(0).getStringCellValue().strip();
			
			// 读取csv文件
			boolean flag = false;
			reader = new CsvReader(csv);
			for (var arr : reader)
				flag = arr.length == 10;
			
			// 判断读取结果
			var csvAll = reader.parseAll();
			var csvCell = csvAll.get(0)[0];
			if ("Test".equals(cell) && "Test".equals(csvCell) && flag)
				info = progressRate;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CLOSER.close(excel, reader);
			IOUtil.delete(xlsx);
			IOUtil.delete(csv);
			System.out.println(info);
		}
	}

	/**
	 * 对Logger及相关组件进行测试.
	 * @param progressRate
	 * @return
	 * @throws InterruptedException
	 */
	private static File loggerTest(String progressRate) {
		var info = "Logger及相关组件兼容性有异常！";
		Logger logger = null;
		File log = null;
		File dir = null;
		try {
			// 生成含随机字符串的json
			var uuid = Integer.toString(new MTRandom().nextInt());
			var json = new EasyJson();
			json.add("uuid", uuid);
			uuid = json.toJson();
			
			// 将json作为报错信息写入错误日志
			var exc = new DefinedRuntimeException(uuid);
			logger = exc.getLogger();
			log = logger.getFile();
			dir = log.getParentFile();
			Thread.sleep(NumberUtil.parseInt("1000", 1000));
			
			// 读取错误日志并搜索json
			var logText = IOUtil.getText(log.toPath(), "UTF-8");
			var indexOf = SundayUtil.indexOf(logText, uuid);
			
			// 在目标文件夹中批量搜索json
			var finder = new TextFinder();
			finder.setDir(dir);
			finder.setExtensions(".log");
			finder.setTarget(uuid, true, (mom, daugther, from) -> mom.indexOf(daugther, 0));
			var find = finder.find();
			
			// 判断搜索结果
			if (indexOf > -1 && Files.isSameFile(find.get(0).getKey().toPath(), log.toPath()))
				info = progressRate;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseSource.close(logger);
			IOUtil.delete(log);
			System.out.println(info);
		}
		
		return dir;
	}

	@Override
	public String[] toExcelHead() {
		return HEAD;
	}

	@Override
	public String[] toExcelTuple() {
		var ints = new int[10];
		for (var i = 0; i < 10; i++)
			ints[i] = MTRandomFast.inThreadLocal().nextInt(20);
		
		SortUtil.shellSort(ints, true);
		
		var arr = new EasyStringArray();
		arr.addAll(ints);
		return arr.toArray();
	}

}