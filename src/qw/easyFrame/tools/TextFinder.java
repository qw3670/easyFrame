package qw.easyFrame.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.AbstractMap.SimpleEntry;
import qw.easyFrame.interfaces.Indexable;
import java.util.LinkedList;
import java.util.List;
import static java.util.Locale.ENGLISH;
import static qw.easyFrame.tools.statics.StringUtil.endsWith;

/**
 * 文本文件批量检索工具.<br>
 * 扫描文件夹内的文本文件，检索出指定的字符串。
 * @author Rex
 *
 */
public class TextFinder implements Serializable {
	private static final long serialVersionUID = 20241227L;
	
	/**
	 * 待检索的文件或文件夹.
	 */
	private File dir;
	
	/**
	 * 需要检索的扩展名（忽略大小写）.<br>
	 * 若不设置此项，则检索所有文件。
	 */
	private String[] extensions;
	
	/**
	 * 指定字符集，可不设置此项.
	 */
	private String charset;
	
	/**
	 * 目标字符串.
	 */
	private String target;
	
	/**
	 * 检索文件内容时，是否忽略大小写.
	 */
	private boolean ignoreCase;
	
	/**
	 * 用于缓存调用者传入的自定义的字符串检索方法.
	 */
	private Indexable indexer;
	
	/**
	 * 用于缓存String.indexof方法..
	 */
	private static Indexable indexof;
	
	/**
	 * 用于缓存忽略大小写的String.indexof方法..
	 */
	private static Indexable ignoreCaseIndexof;
	
	/**
	 * 是否采用单匹配模式搜索字符串.<br>
	 * 单匹配：在文本文件的某行发现目标字符串后，立刻返回，不再继续搜索文本文件的剩余行；<br>
	 * 多匹配（普通模式）：在文本文件的某行发现目标字符串后，仍继续搜索文本文件的剩余行；<br>
	 * 注意：若一行文本中多次出现目标字符串，即使在多匹配模式下，也仅会匹配首次。
	 */
	private boolean singleMatch;
	
	/**
	 * 搜索文件时，是否穿透子文件夹.<br>
	 * 默认为true。
	 */
	private boolean penetrated = true;
	
	/**
	 * 搜索文件时，是否穿透子文件夹.
	 */
	public boolean isPenetrated() {
		return penetrated;
	}

	/**
	 * 搜索文件时，是否穿透子文件夹.
	 */
	public void setPenetrated(boolean penetrated) {
		this.penetrated = penetrated;
	}

	public TextFinder() {
		super();
	}
	
	public File getDir() {
		return dir;
	}
	
	/**
	 * 待检索的文件或文件夹.
	 * @param dir
	 */
	public void setDir(File dir) {
		this.dir = dir;
	}
	
	public String[] getExtensions() {
		return extensions;
	}
	
	/**
	 * 设置需要检索的扩展名（自动忽略大小写）.<br>
	 * 注意：建议传入的扩展名包含小数点。<br>
	 * 若不设置此项（extensions为null），则检索所有文件。
	 *  若设置此项为空数组，则不检索任何文件。
	 * @param extensions
	 */
	public void setExtensions(String... extensions) {
		var len = extensions.length;
		this.extensions = new String[len];
		for (var i = 0; i < len; i++)
			this.extensions[i] = extensions[i].toLowerCase(ENGLISH);
	}
	
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	public String getTarget() {
		return target;
	}
	
	/**
	 * 检索文件.<br>
	 * @param target 目标字符串
	 * @param singleMatch 
	 * @param ignoreCase 是否忽略大小写（仅适用检索文件内容，不适用匹配文件扩展名）
	 */
	public void setTarget(String target, boolean singleMatch, boolean ignoreCase) {
		this.target = target;
		this.singleMatch = singleMatch;
		this.ignoreCase = ignoreCase;
	}
	
	/**
	 * 检索文件.<br>
	 * @param target 目标字符串
	 * @param singleMatch 
	 * @param indexer 自定义的字符串检索方法
	 */
	public void setTarget(String target, boolean singleMatch, Indexable indexer) {
		this.target = target;
		this.singleMatch = singleMatch;
		this.indexer = indexer;
	}
	
	public boolean isSingleMatch() {
		return this.singleMatch;
	}

	/**
	 * 开始检索并返回检索结果.
	 * 匹配字符串使用的是朴素算法。
	 * @return key为匹配到指定字符串的文本文件；<br>
	 *         value为指定字符在文本文件中出现的行数（从1开始）。
	 */
	public List<SimpleEntry<File, Integer>> find() {
		if (this.indexer != null) { // 自定义
			return find(this.indexer, target);
		} else if (this.ignoreCase) { // 忽略大小写
			if (ignoreCaseIndexof == null)
				ignoreCaseIndexof = (mom, daugther, from) -> mom.toLowerCase(ENGLISH).indexOf(daugther, 0);
			return find(ignoreCaseIndexof, target.toLowerCase(ENGLISH));
		} else { // 区分大小写
			if (indexof == null)
				indexof = (mom, daugther, from) -> mom.indexOf(daugther, 0);
			return find(indexof, target);
		}
	}
	
	/**
	 * 使用指定算法检索字符串并返回结果.
	 * @param indexer 指定的字符串搜索算法
	 * @param daugther 待检索的字符串
	 * @return key为匹配到指定字符串的文本文件；<br>
	 *         value为指定字符在文本文件中出现的行数（从1开始）。
	 */
	private final List<SimpleEntry<File, Integer>> find(Indexable indexer, String daugther) {
		if (singleMatch) // 单匹配
			return doSingleMatch(indexer, daugther);
		else // 多匹配
			return doNormalMatch(indexer, daugther);
	}
	
	/**
	 * 单匹配（普通模式匹配）.
	 * @param indexer
	 * @param daugther
	 * @return
	 */
	private final LinkedList<SimpleEntry<File, Integer>> doSingleMatch(Indexable indexer, String daugther) {
		var list = new LinkedList<SimpleEntry<File, Integer>>();

		flag: for (var file : listFiles(dir, false))
			try (var fis = new FileInputStream(file);
					var in = charset == null ? new InputStreamReader(fis) : new InputStreamReader(fis, charset);
					var reader = new BufferedReader(in)) {

				String line = null;
				var index = 1;

				while ((line = reader.readLine()) != null) {
					if (indexer.indexOf(line, daugther, 0) > -1) {
						list.add(new SimpleEntry<>(file, index));
						continue flag;
					}
					index++;
				}

			} catch (Exception e) {
				continue flag;
			}

		return list;
	}

	/**
	 * 多匹配（普通模式匹配）.
	 * @param indexer
	 * @param daugther
	 * @return
	 */
	private final LinkedList<SimpleEntry<File, Integer>> doNormalMatch(Indexable indexer, String daugther) {
		var list = new LinkedList<SimpleEntry<File, Integer>>();

		for (var file : listFiles(dir, false))
			try (var fis = new FileInputStream(file);
					var in = charset == null ? new InputStreamReader(fis) : new InputStreamReader(fis, charset);
					var reader = new BufferedReader(in)) {

				String line = null;
				var index = 1;

				while ((line = reader.readLine()) != null) {
					if (indexer.indexOf(line, daugther, 0) > -1)
						list.add(new SimpleEntry<>(file, index));
					index++;
				}

			} catch (Exception e) {
				continue;
			}

		return list;
	}
	
	/**
	 * 按照指定扩展名搜索文件.
	 * @param file
	 * @param isDirectory 当且仅当完全确定file参数为可访问的文件夹时，才为true
	 * @return
	 */
	private final List<File> listFiles(File file, boolean isDirectory) {
		var list = new LinkedList<File>();

		if (isDirectory || file.isDirectory()) {
			for (var f : file.listFiles())
				if (this.penetrated && f.isDirectory())
					list.addAll(listFiles(f, true));
				else if (f.isFile())
					matchFile(list, f, extensions);
		} else if (file.isFile()) {
			matchFile(list, file, extensions);
		}
		return list;
	}
	
	/**
	 * 按照扩展名匹配文件.
	 * @param list
	 * @param f
	 * @param extensions
	 */
	private static final void matchFile(List<File> list, File f, String[] extensions) {
		/*
		 *  若未指定扩展名（extensions为null），则检索所有文件（判断条件始终为true）。
		 *  若extensions为空数组，则不检索任何文件（判断条件始终为false）。
		 */
		if (extensions == null || endsWith(f.getName().toLowerCase(ENGLISH), extensions))
			list.add(f);
	}
	
	/**
	 * 重置当前对象.<br>
	 * 即：将当前对象内的成员变量全部置为null，使其回到刚初始化的状态。
	 */
	public void clear() {
		this.dir = null;
		this.extensions = null;
		this.charset = null;
		this.target = null;
		this.ignoreCase = false;
		this.singleMatch = false;
		this.indexer = null;
	}
	
}