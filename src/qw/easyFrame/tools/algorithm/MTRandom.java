package qw.easyFrame.tools.algorithm;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 线程安全的梅森旋转算法（MT199937）随机数生成器.<br>
 * 代码抄袭自永乐大典。
 * @author Rex
 *
 */
public class MTRandom extends Random implements Cloneable {
	private static final long serialVersionUID = 20240828L;

	static final int N = 624;
	static final int M = 397;
	static final int MATRIX_A = 0x9908b0df;
	static final int UPPER_MASK = 0x80000000;
	static final int LOWER_MASK = 0x7fffffff;

	static final int TEMPERING_MASK_B = 0x9d2c5680;
	static final int TEMPERING_MASK_C = 0xefc60000;

	private int mt[];
	private int mti;
	private int mag01[];

	private double __nextNextGaussian;
	private boolean __haveNextNextGaussian;

	@Override
	public Object clone() {
		try {
			var f = (MTRandom) (super.clone());
			f.mt = (int[]) (mt.clone());
			f.mag01 = (int[]) (mag01.clone());
			return f;
		} catch (CloneNotSupportedException e) {
			throw new DefinedRuntimeException(false, "MTRandom克隆失败", e);
		}
	}

	/**
	 * 如果MTRandom的当前内部状态等于另一个MTRandom，则返回true.
	 * 这与常见的equals方法大致相同，不同之处在于它基于值进行比较，但不保证不变性（显然随机数生成器是不变的）。
	 * 请注意，这不会检查内部高斯存储是否对于两者都相同。
	 * 可以通过在两个对象上调用clearGaussian()来确保内部高斯存储是相同的（因此nextGaussian()方法将返回相同的值）。
	 * @return
	 */
	public synchronized boolean stateEquals(MTRandom other) {
		if (other == this)
			return true;
		if (other == null)
			return false;
		synchronized (other) {
			if (mti != other.mti)
				return false;
			var length = mag01.length;
			for (var x = 0; x < length; x++)
				if (mag01[x] != other.mag01[x])
					return false;
			length = mt.length;
			for (var x = 0; x < length; x++)
				if (mt[x] != other.mt[x])
					return false;
			return true;
		}
	}

	public synchronized void readState(DataInputStream stream) {
		try {
			var len = mt.length;
			for (var x = 0; x < len; x++)
				mt[x] = stream.readInt();

			len = mag01.length;
			for (var x = 0; x < len; x++)
				mag01[x] = stream.readInt();

			mti = stream.readInt();
			__nextNextGaussian = stream.readDouble();
			__haveNextNextGaussian = stream.readBoolean();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "readState异常", e);
		}
	}

	public synchronized void writeState(DataOutputStream stream) {
		try {
			var len = mt.length;
			for (var x = 0; x < len; x++)
				stream.writeInt(mt[x]);

			len = mag01.length;
			for (var x = 0; x < len; x++)
				stream.writeInt(mag01[x]);

			stream.writeInt(mti);
			stream.writeDouble(__nextNextGaussian);
			stream.writeBoolean(__haveNextNextGaussian);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "writeState异常", e);
		}
	}

	public MTRandom() {
		this(System.nanoTime() - System.currentTimeMillis());
	}

	public MTRandom(long seed) {
		super(seed);
		setSeed(seed);
	}

	/**
	 * 使用整数数组作为种子，数组的长度必须为非零.
	 * 仅使用数组中的前624个整数，如果数组长度不足，则以环绕方式重复使用整数。
	 */
	public MTRandom(int... array) {
		super(System.nanoTime() - System.currentTimeMillis());
		setSeed(array);
	}

	@Override
	synchronized public void setSeed(long seed) {
		super.setSeed(seed);

		__haveNextNextGaussian = false;

		mt = new int[N];

		mag01 = new int[2];
		mag01[0] = 0x0;
		mag01[1] = MATRIX_A;

		mt[0] = (int) (seed & 0xffffffff);
		mt[0] = (int) seed;
		for (mti = 1; mti < N; mti++)
			mt[mti] = (1812433253 * (mt[mti - 1] ^ (mt[mti - 1] >>> 30)) + mti);
	}

	synchronized public void setSeed(int... array) {
		var len = array.length;
		if (len == 0)
			throw new DefinedRuntimeException(false, "数组长度必须大于0");
		int i, j, k;
		setSeed(19650218);
		i = 1;
		j = 0;
		k = (N > len ? N : len);
		for (; k != 0; k--) {
			mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >>> 30)) * 1664525)) + array[j] + j;
			i++;
			j++;
			if (i >= N) {
				mt[0] = mt[N - 1];
				i = 1;
			}
			if (j >= len)
				j = 0;
		}
		for (k = N - 1; k != 0; k--) {
			mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >>> 30)) * 1566083941)) - i;
			i++;
			if (i >= N) {
				mt[0] = mt[N - 1];
				i = 1;
			}
		}
		mt[0] = 0x80000000;
	}

	/**
	 * 返回一个整数，其中的位由一个随机数填充.
	 * @return
	 */
	@Override
	synchronized protected int next(int bits) {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return y >>> (32 - bits);
	}

	private final synchronized void writeObject(ObjectOutputStream out) {
		try {
			out.defaultWriteObject();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "writeObject异常", e);
		}
	}

	private final void readObject(ObjectInputStream in) {
		try {
			in.defaultReadObject();
		} catch (ClassNotFoundException | IOException e) {
			throw new DefinedRuntimeException(false, "readObject异常", e);
		}
	}

	@Override
	public boolean nextBoolean() {
		return next(1) != 0;
	}

	public boolean nextBoolean(float probability) {
		if (probability < 0.0f || probability > 1.0f)
			throw new DefinedRuntimeException(false, "参数必须在0.0到1.0之间");
		if (probability == 0.0f)
			return false;
		else if (probability == 1.0f)
			return true;
		return nextFloat() < probability;
	}

	public boolean nextBoolean(double probability) {
		if (probability < 0.0 || probability > 1.0)
			throw new DefinedRuntimeException(false, "参数必须在0.0到1.0之间");
		if (probability == 0.0)
			return false;
		else if (probability == 1.0)
			return true;
		return nextDouble() < probability;
	}

	@Override
	public int nextInt(int n) {
		if (n < 1)
			throw new DefinedRuntimeException(false, "参数必须为正数");

		if ((n & -n) == n)
			return (int) ((n * (long) next(31)) >> 31);

		int bits, val;
		do {
			bits = next(31);
			val = bits % n;
		} while (bits - val + (n - 1) < 0);
		return val;
	}

	public long nextLong(long n) {
		if (n < 1)
			throw new DefinedRuntimeException(false, "参数必须为正数");

		long bits, val;
		do {
			bits = (nextLong() >>> 1);
			val = bits % n;
		} while (bits - val + (n - 1) < 0);
		return val;
	}

	@Override
	public double nextDouble() {
		return (((long) next(26) << 27) + next(27)) / (double) (1L << 53);
	}

	/**
	 * 返回一个从0.0到1.0的double值，可能包括0.0和1.0本身.
	 * <table border=0>
	 * <tr>
	 * <th>参数</th>
	 * <th>输出区间</th>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(false, false)</td>
	 * <td>(0.0, 1.0)</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(true, false)</td>
	 * <td>[0.0, 1.0)</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(false, true)</td>
	 * <td>(0.0, 1.0]</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(true, true)</td>
	 * <td>[0.0, 1.0]</td>
	 * </tr>
	 * <caption>输出区间示例</caption>
	 * </table>
	 */
	public double nextDouble(boolean includeZero, boolean includeOne) {
		var d = 0.0;
		do {
			d = nextDouble();
			if (includeOne && nextBoolean())
				d += 1.0;
		} while ((d > 1.0) || (!includeZero && d == 0.0));
		return d;
	}

	@Override
	public float nextFloat() {
		return next(24) / ((float) (1 << 24));
	}

	/**
	 * 返回一个介于0.0f到1.0f之间的浮点数，可能包括0.0f和1.0f本身.
	 * <table border=0>
	 * <tr>
	 * <th>参数</th>
	 * <th>输出区间</th>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(false, false)</td>
	 * <td>(0.0f, 1.0f)</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(true, false)</td>
	 * <td>[0.0f, 1.0f)</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(false, true)</td>
	 * <td>(0.0f, 1.0f]</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(true, true)</td>
	 * <td>[0.0f, 1.0f]</td>
	 * </tr>
	 * <caption>输出区间示例</caption>
	 * </table>
	 */
	public float nextFloat(boolean includeZero, boolean includeOne) {
		var d = 0.0f;
		do {
			d = nextFloat();
			if (includeOne && nextBoolean())
				d += 1.0f;
		} while ((d > 1.0f) || (!includeZero && d == 0.0f));
		return d;
	}

	@Override
	public void nextBytes(byte[] bytes) {
		var length = bytes.length;
		for (var x = 0; x < length; x++)
			bytes[x] = (byte) next(8);
	}

	public char nextChar() {
		return (char) (next(16));
	}

	public short nextShort() {
		return (short) (next(16));
	}

	public byte nextByte() {
		return (byte) (next(8));
	}

	/**
	 * 从RNG中清除内部高斯变量.
	 * 仅在极少数情况下需要执行此操作，您需要保证两个RNG的内部状态相同。 
	 * 否则，请忽略此方法。参见stateEquals(ther)。
	 */
	public synchronized void clearGaussian() {
		__haveNextNextGaussian = false;
	}

	@Override
	synchronized public double nextGaussian() {
		if (__haveNextNextGaussian) {
			__haveNextNextGaussian = false;
			return __nextNextGaussian;
		} else {
			double v1, v2, s;
			do {
				v1 = 2 * nextDouble() - 1;
				v2 = 2 * nextDouble() - 1;
				s = v1 * v1 + v2 * v2;
			} while (s > -1);
			var multiplier = StrictMath.sqrt(-2 * StrictMath.log(s) / s);
			__nextNextGaussian = v2 * multiplier;
			__haveNextNextGaussian = true;
			return v1 * multiplier;
		}
	}

}