package qw.easyFrame.tools.algorithm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.IOUtil;
import static qw.easyFrame.tools.statics.CloseSource.close;
import static qw.easyFrame.tools.statics.IOUtil.delete;

/**
 * zip压缩文件工具类.
 * @author Rex
 *
 */
public class ZipUtil implements Serializable {
	private static final long serialVersionUID = 20240905L;

	/**
	 * 目标文件.<br>
	 * 若src是zip文件，此变量应当为存放解压缩后文件的文件夹路径；<br>
	 * 若src是待压缩文件，此变量应当为压缩后文件的文件名含完整路径。
	 */
	private File destFile;

	/**
	 * 待压缩或待解压的多个文件.
	 */
	private List<File> src;
	
	/**
	 * 待写入到压缩包中的注释.<br>
	 * 此项只在压缩文件时有意义。
	 */
	private String comment;
	
	/**
	 * 输出zip文件时的编码集，可为空.
	 * 用于调整压缩包注释和文件名的编码集。
	 */
	private Charset charset;
	
	/**
	 * 压缩级别.<br>
	 * 此项只在压缩文件时有意义。
	 */
	private int level = Deflater.DEFAULT_COMPRESSION;
	
	/**
	 * 用于获取压缩包注释.
	 * @see ZipUtil#getZipComment(byte[], int)
	 */
	private final byte[] magicNum = { 0x50, 0x4b, 0x05, 0x06 };

	public ZipUtil() {
		super();
	}

	public ZipUtil(File destFile, File... src) {
		setDestFileAndSrc(destFile, src);
	}

	public ZipUtil(File destFile, List<File> src) {
		this.destFile = destFile;
		this.src = src;
	}

	public File getDestFile() {
		return destFile;
	}
	public void setDestFile(File destFile) {
		this.destFile = destFile;
	}

	public void setDestFileAndSrc(File destFile, File... src) {
		this.destFile = destFile;
		var len = src.length;
		this.src = new ArrayList<>(len);
		for (var i = 0; i < len; i++)
			this.src.add(src[i]);
	}

	public void setDestFileAndSrc(File destFile, List<File> src) {
		this.destFile = destFile;
		this.src = src;
	}

	public List<File> getSrc() {
		return src;
	}
	public void setSrc(List<File> src) {
		this.src = src;
	}

	public void setSrc(File... src) {
		var len = src.length;
		this.src = new ArrayList<>(len);
		for (var i = 0; i < len; i++)
			this.src.add(src[i]);
	}

	public void addSrc(File... src) {
		var len = src.length;
		if (this.src == null)
			this.src = new ArrayList<>(len);
		for (var i = 0; i < len; i++)
			this.src.add(src[i]);
	}

	public void addSrc(List<File> src) {
		if (this.src == null)
			this.src = src;
		else
			this.src.addAll(src);
	}

	/**
	 * 获取待写入到压缩文件中的注释.<br>
	 * 注意：此方法仅用于获取用户之前使用setComment(String)方法自行设置的、
	 * 将要写入到待压缩文件中的注释。
	 * 如若要提取已经存在的zip文件的注释，请使用extractZipComment()方法。
	 * @see ZipUtil#setComment(String)
	 * @see ZipUtil#extractComment()
	 * @return
	 */
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCharset() {
		return charset.name();
	}
	public void setCharset(String charset) {
		this.charset = Charset.forName(charset);
	}
	
	public int getLevel() {
		return level;
	}
	
	/**
	 * @see ZipOutputStream#setLevel(int)
	 * @param level
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 压缩文件.
	 */
	public void compress() {
		int size;
		if (src != null && (size = src.size()) != 0)
			if (size == 1)
				compressSingleFile(src.get(0), destFile, null);
			else
				compressFiles();
	}

	/**
	 * 解压缩文件.
	 */
	public void decompress() {
		int size;
		if (src != null && (size = src.size()) != 0)
			if (size == 1)
				decompressSingleFile(src.get(0), destFile);
			else
				decompressFiles();
	}

	/**
	 * 压缩多个待压缩文件.
	 */
	private final void compressFiles() {
		// 临时文件夹
		File tempDir = null;
		try {
			// 建立临时文件夹
			var dir = destFile.getName();
			dir = dir.substring(0, dir.lastIndexOf("."));
			tempDir = new File(destFile.getParent().concat(File.separator).concat(dir));
			tempDir.mkdirs();
			
			// 移动文件到临时文件夹并记录
			List<Path[]> temp = new ArrayList<>(src.size());
			for (var srcFile : src)
				if (srcFile != null && srcFile.exists()) {
					var oldPath = srcFile.toPath();// 源路径
					var newPath = new File(tempDir, srcFile.getName()).toPath();// 新路径
					Files.move(oldPath, newPath, StandardCopyOption.REPLACE_EXISTING);
					temp.add(new Path[] { newPath, oldPath });
				}

			/*
			 *  压缩文件。
			 *  temp参数传入compressSingleFile方法后并未被使用，
			 *  方法直接将原参数返回。
			 *  传参并获取返回值的操作无实际意义，仅为阻止CPU乱序。
			 */
			temp = compressSingleFile(tempDir, destFile, temp);

			// 将文件移动回原位置
			for (var e : temp)
				Files.move(e[0], e[1], StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "转移临时文件失败", e);
		} finally {
			// 删除临时文件夹
			if (tempDir != null && tempDir.list().length == 0)
				tempDir.delete();
		}
	}

	/**
	 * 解压缩多个待解压文件.
	 */
	private final void decompressFiles() {
		for (var f : src)
			decompressSingleFile(f, destFile);
	}

	/**
	 * 压缩单个文件.
	 * @param src
	 * @param target
	 * @param temp 无意义参数
	 */
	private final List<Path[]> compressSingleFile(File src, File target, List<Path[]> temp) {
		if (src != null && src.exists() && target != null) {
			var parentFile = target.getParentFile();
			if (!parentFile.exists())
				parentFile.mkdirs();

			try (var fos = new FileOutputStream(target, false);
					var zos = new ZipOutputStream(fos,
							charset == null ? StandardCharsets.UTF_8 : charset)) {
				zos.setComment(comment);
				zos.setLevel(level);
				compressByType(src, zos, "");
			} catch (FileNotFoundException e) {
				throw new DefinedRuntimeException(false, "ZipUtil未发现源文件", e);
			} catch (Exception e) {
				throw new DefinedRuntimeException(false, "压缩程序异常", e);
			}
		}
		return temp;
	}

	/**
	 * 按照原路径的类型就行压缩.
	 * @param src
	 * @param zos
	 * @param baseDir
	 */
	private final void compressByType(File src, ZipOutputStream zos, String baseDir) {
		// 判断文件是否是文件
		if (src.isFile())
			// 如果是文件调用compressFile方法
			compressFile(src, zos, baseDir);
		else if (src.isDirectory())
			// 如果是路径，则调用compressDir方法
			compressDir(src, zos, baseDir);
	}

	/**
	 * 压缩文件.
	 * @param file
	 * @param zos
	 * @param baseDir
	 */
	private final void compressFile(File file, ZipOutputStream zos, String baseDir) {
		try (var fis = new FileInputStream(file);
				var bis = new BufferedInputStream(fis)) {
			zos.putNextEntry(new ZipEntry(baseDir.concat(file.getName())));

			IOUtil.write(bis, zos, 1048576/* 1MiB */, false);
		} catch (FileNotFoundException e) {
			throw new DefinedRuntimeException(false, "ZipUtil未发现源文件", e);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "压缩程序异常", e);
		}
	}

	/**
	 * 压缩文件夹.
	 * @param dir
	 * @param zos
	 * @param baseDir
	 */
	private final void compressDir(File dir, ZipOutputStream zos, String baseDir) {
		var files = dir.listFiles();
		baseDir = baseDir.concat(dir.getName()).concat(File.separator);

		if (files.length == 0)
			try {
				zos.putNextEntry(new ZipEntry(baseDir));
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "压缩程序异常", e);
			}
		else
			for (var file : files)
				compressByType(file, zos, baseDir);
	}

	/**
	 * 解压缩Zip文件.
	 * @param src
	 * @param dir
	 */
	private final void decompressSingleFile(File src, File dir) {
		List<AutoCloseable> list = null;
		if (src != null && src.isFile() && dir != null)
			try (var fis = new FileInputStream(src);
					var zIn = new ZipInputStream(fis, 
							charset == null ? StandardCharsets.UTF_8 : charset)) {
				if (!dir.exists())
					dir.mkdirs();

				ZipEntry entry = null;
				list = new ArrayList<>();
				while ((entry = zIn.getNextEntry()) != null) {
					var file = new File(dir, entry.getName());
					if (!entry.isDirectory()) {
						if (!file.exists())
							// 创建此文件的上级目录
							file.getParentFile().mkdirs();

						var out = new FileOutputStream(file, false);
						var bos = new BufferedOutputStream(out);
						// list添加元素的顺序不能更改
						list.add(bos);
						list.add(out);

						IOUtil.write(zIn, bos, 1048576/* 1MiB */, false);
					} else
						file.mkdirs();
				}
			} catch (FileNotFoundException e) {
				throw new DefinedRuntimeException(false, "ZipUtil未发现源文件", e);
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "解压缩文件时出现异常", e);
			} finally {
				close(list);
			}
	}

	/**
	 * 提取zip文件的注释.
	 * 返回值是一个String数组，按List内部元素顺序储存了所有待解压文件的注释。
	 * 若待解压文件只有一个，则返回一个长度为1的String数组。
	 * @return
	 */
	public String[] extractComment() {
		String[] comments = null;
		FileInputStream[] ins = null;

		int size;
		if (src != null && (size = src.size()) != 0) {
			comments = new String[size];
			ins = new FileInputStream[size];

			try {
				for (var i = 0; i < size; i++) {
					var file = src.get(i);
					if (file != null && file.exists()) {
						ins[i] = new FileInputStream(file);
						var fileLen = (int) file.length();
						var bufferLen = fileLen < 8192 ? fileLen : 8192;
						var buffer = new byte[bufferLen];
						ins[i].skip(fileLen - bufferLen);
						int len;
						if ((len = ins[i].read(buffer, 0, bufferLen)) > 0)
							comments[i] = getZipComment(buffer, len);
					}
				}
			} catch (FileNotFoundException e) {
				throw new DefinedRuntimeException(false, "未找到zip文件", e);
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "读取zip文件注释失败", e);
			} finally {
				close(ins);
			}
		}

		return comments;
	}

	/**
	 * 从缓存中获取zip注释.
	 * @param buffer
	 * @param len
	 * @return
	 */
	private final String getZipComment(byte[] buffer, int len) {
		var buffLen = buffer.length < len ? buffer.length : len;
		for (var i = buffLen - 23; i > -1; i--) {
			var isMagicStart = true;
			flag: for (var k = 0; k < 4/* 4=magicDirEnd.length */; k++)
				if (buffer[i + k] != magicNum[k]) {
					isMagicStart = false;
					break flag;
				}

			if (isMagicStart) {
				var realLen = buffLen - i - 22;
				var tempLen = buffer[i + 20] + buffer[i + 22] << 8;
				tempLen = tempLen < 0 ? realLen : tempLen;
				var comment = new String(buffer, i + 22, tempLen < realLen ? tempLen : realLen);
				return comment;
			}
		}
		return "";
	}
	
	/**
	 * 压缩byte数组.
	 * @param data 待压缩数据
	 * @param level 压缩级别
	 * @return
	 */
	public static byte[] zip(byte[] data, int level) {
		try (var bos = new ByteArrayOutputStream();
				var zip = new ZipOutputStream(bos)) {
			zip.setLevel(level);
			var entry = new ZipEntry("zip");
			var length = data.length;
			entry.setSize(length);
			zip.putNextEntry(entry);
			zip.write(data, 0, length);
			zip.closeEntry();
			return bos.toByteArray();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "压缩byte[]失败", e);
		}
	}
	
	/**
	 * 解压byte数组.
	 * @param data
	 * @return
	 */
	public static byte[] unzip(byte[] data) {
		try (var bis = new ByteArrayInputStream(data);
				var zip = new ZipInputStream(bis);
				var baos = new ByteArrayOutputStream()) {
			if (zip.getNextEntry() != null) {
				IOUtil.write(zip, baos, 1048576/* 1MiB */, false);
				return baos.toByteArray();
			}
			return null;
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "解压byte[]失败", e);
		}
	}

	/**
	 * 从磁盘中删除待压缩或待解压的多个文件. <br>
	 * 注意：此方法不会清空文件列表。
	 */
	public void deleteSrc() {
		if (src != null)
			for (var file : src)
				delete(file);
	}

	/**
	 * 清空待压缩或待解压文件列表. <br>
	 * 注意：此方法不会从磁盘中删除文件。
	 */
	public void clearSrc() {
		this.src.clear();
	}
	
}