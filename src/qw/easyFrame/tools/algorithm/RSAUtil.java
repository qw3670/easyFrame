package qw.easyFrame.tools.algorithm;

import java.io.Closeable;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import qw.easyFrame.enums.SecretKeySize;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 封装RSA非对称加密算法.
 * @author Rex
 *
 */
public class RSAUtil implements Closeable {
	
	/**
	 * 密钥长度.
	 */
	private SecretKeySize keySize;
	
	/**
	 * 密钥生成器.
	 */
	private KeyFactory keyFactory;

	/**
	 * 公钥.
	 * 用于加密。
	 */
	transient private Key publicKey;
	
	/**
	 * 私钥.
	 * 用于解密。
	 */
	transient private Key privateKey;
	
	/**
	 * 加密/解密器.
	 */
	private Cipher cipher;
	
	/**
	 * 此构造器仅初始化密钥生成器和加密/解密器，不生成任何密钥.
	 * 使用此构造器生成的对象不可用于加密解密，且其getter方法获得的密钥值为null。
	 * 需要手动调用对应的setter方法设置密钥的值（或调用setKeySize(int)方法直接生成一对密钥）后，
	 * 方可正常使用对象加密/解密数据。
	 * @see RSAUtil#setPrivateKey(byte[])
	 * @see RSAUtil#setPublicKey(byte[])
	 * @see RSAUtil#setKeySize(int)
	 */
	public RSAUtil() {
		initCipher();
	}

	/**
	 * 使用指定的密钥长度生成一对密钥.
	 * 同时初始化密钥生成器和加密/解密器。
	 * @param keySize
	 */
	public RSAUtil(SecretKeySize keySize) {
		initCipher();
		setKeySize(keySize);
	}
	
	/**
	 * 使用指定的密钥构造对象.
	 * 同时初始化密钥生成器和加密/解密器。<br>
	 * 注意：若仅需使用加密（解密）功能，传参时可仅传入公钥（私钥），另一个参数可传null。
	 * @param publicKey
	 * @param privateKey
	 */
	public RSAUtil(byte[] publicKey, byte[] privateKey) {
		initCipher();
		createPublicKey(publicKey);
		createPrivateKey(privateKey);
	}

	/**
	 * 初始化加密/解密器.
	 */
	private final void initCipher() {
		try {
			this.cipher = Cipher.getInstance("RSA");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new DefinedRuntimeException(false, "初始化RSA加密/解密器失败", e);
		}
	}
	
	/**
	 * 生成密钥生成器（如有需要）和公钥.
	 * @param publicKey
	 */
	private final void createPublicKey(byte[] publicKey) {
		try {
			// 公钥
			var keySpec = new X509EncodedKeySpec(publicKey);
			// 密钥生成器
			this.keyFactory = this.keyFactory != null ? this.keyFactory : KeyFactory.getInstance("RSA");
			this.publicKey = keyFactory.generatePublic(keySpec);
		} catch (InvalidKeySpecException e) {
			throw new DefinedRuntimeException(false, "生成RSA公钥失败", e);
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "初始化密钥生成器失败", e);
		}
	}
	
	/**
	 * 生成密钥生成器（如有需要）和私钥.
	 * @param privateKey
	 */
	private final void createPrivateKey(byte[] privateKey) {
		try {
			// 私钥
			var keySpec = new PKCS8EncodedKeySpec(privateKey);
			// 密钥生成器
			this.keyFactory = this.keyFactory != null ? this.keyFactory : KeyFactory.getInstance("RSA");
			this.privateKey = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			throw new DefinedRuntimeException(false, "生成RSA私钥失败", e);
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "初始化密钥生成器失败", e);
		}
	}
    
	/**
	 * 获取密钥长度.
	 * @return
	 */
	public SecretKeySize getKeySize() {
		return keySize;
	}

	/**
	 * 设置密钥长度.<br>
	 * 注意：此方法同时会生成一对密钥！
	 * @param keySize
	 */
	public void setKeySize(SecretKeySize keySize) {
		try {
			this.keySize = keySize;
			var generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(keySize.getKeySize(), SecureRandom.getInstanceStrong());
			var keyPair = generator.generateKeyPair();
			this.publicKey = keyPair.getPublic();
			this.privateKey = keyPair.getPrivate();
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "生成RSA密钥对失败", e);
		}
	}

	/**
	 * 获取公钥.
	 * @return
	 */
	public byte[] getPublicKey() {
		return publicKey.getEncoded();
	}
	
	/**
	 * 设置公钥.
	 * @param publicKey
	 */
	public void setPublicKey(byte[] publicKey) {
		createPublicKey(publicKey);
	}

	/**
	 * 获取私钥.
	 * @return
	 */
	public byte[] getPrivateKey() {
		return privateKey.getEncoded();
	}
	
	/**
	 * 设置私钥.
	 * @param privateKey
	 */
	public void setPrivateKey(byte[] privateKey) {
		createPrivateKey(privateKey);
	}

	/**
	 * 使用公钥加密.
	 * @param data
	 * @return
	 */
	public byte[] encrypt(byte[] data) {
		try {
			final var cipher = this.cipher;
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			throw new DefinedRuntimeException(false, "RSA公钥加密失败", e);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new DefinedRuntimeException(false, "RSA加密填充失败", e);
		}
	}
	
	/**
	 * 使用私钥解密.
	 * @param code
	 * @return
	 */
	public byte[] decrypt(byte[] code) {
		try {
			final var cipher = this.cipher;
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return cipher.doFinal(code);
		} catch (InvalidKeyException e) {
			throw new DefinedRuntimeException(false, "RSA私钥解密失败", e);
		} catch (BadPaddingException | IllegalBlockSizeException e) {
			throw new DefinedRuntimeException(false, "RSA解密填充失败", e);
		}
	}

	/**
	 * 使用此方法可将类变量全部重置为null.<br>
	 * 使用场景：在多线程环境中，对象可能在多个线程间共享。
	 * 例如在Tomcat编程中，对象可能被保存在Session或ServletContext中，供多个request取用。
	 * 此时为防止密钥泄露，某个使用者将对象使用完毕并确定对象不会再被其它线程取用后，
	 * 可使用此方法重置对象，以免密钥泄露。<br>
	 * 注意：某个使用者将对象使用完毕并确定对象不会再被其它线程取用后，
	 * 应当优先将对象声明为null，以此来避免密钥泄露。
	 * 本方法应当仅在业务逻辑不支持将对象声明为null（null表示某种特殊状态，或业务逻辑中禁止出现null）时被使用。
	 */
	@Override
	public void close() {
		this.keySize = null;
		this.keyFactory = null;
		this.publicKey = null;
		this.privateKey = null;
		this.cipher = null;
	}
    
}