package qw.easyFrame.tools.algorithm;

/**
 * 封装Sunday算法.
 * @author Rex
 *
 */
public class SundayUtil {
 
	/**
	 * @param source 源字符串（母串）
	 * @param target 目标字符串（女串）
	 * @return
	 */
	public static int indexOf(String source, String target) {
		return indexOf(source.getBytes(), target.getBytes(), 0);
	}
	
	/**
	 * @param source 源字符串（母串）
	 * @param target 目标字符串（女串）
	 * @param fromindex 指定从哪个下标开始检索
	 * @return
	 */
	public static int indexOf(String source, String target, int fromindex) {
		return indexOf(source.getBytes(), target.getBytes(), fromindex);
	}
	
	/**
	 * @param sArr 源数组（母串）
	 * @param tArr 目标数组（女串）
	 * @return
	 */
	public static int indexOf(char[] sArr, char[] tArr) {
		return indexOf(sArr, tArr, 0);
	}
	
	/**
	 * @param sArr 源数组（母串）
	 * @param tArr 目标数组（女串）
	 * @param fromindex 指定从哪个下标开始检索
	 * @return
	 */
	public final static int indexOf(char[] sArr, char[] tArr, int fromindex) {
		var tLen = tArr.length;
		// 若女串为空字符串，直接返回0
		if (tLen == 0)
			return 0;

		var difference = sArr.length - tLen;// 两个字符串的长度差
		var tArrLastIndex = tLen - 1;// 女串的最后一个字符的下标
		int i = fromindex, j = 0;
		// + j是因为可能会出现最后一次移动母串剩余长度与女串长度不一致的情况
		int current;
		while (i <= (current = difference + j))
			if (sArr[i] != tArr[j]) {
				/*
				 * 若母串与女串当前字符不相等，说明女串已经和母串中最后可能相等的字符比较过了，
				 * 并且后面也没有可比较的了，所以返回-1
				 */
				if (i == current)
					return -1;

				/*
				 * 若母串的中间部分与女串匹配，且结果不相等， 就从女串最后面开始，
				 * 找出女串最后一位的下一位对应母串的字符在女串中是否存在
				 */
				var pos = -1;
				// 判断女串中是否存在末尾下一个位置对应的母串的字符
				var temp = i - j + tLen;
				var nextLast = sArr[temp];
				flag: for (var k = tArrLastIndex; k >= 0; k--)
					if (tArr[k] == nextLast) {
						pos = k;
						break flag;
					}

				if (pos == -1) {
					// 不存在则直接跳到再下一位，女串从头开始
					i = temp + 1;
					j = 0;
				} else {
					// 存在则将这个字符与女串最右边与它相同的字符对齐，并再次从头开始比较
					i = temp - pos;
					j = 0;
				}

			} else {
				// 若母串与女串当前字符相等
				if (j == tArrLastIndex) {
					// 若比较到了女串的最后一位，说明已经存在
					return i - j;
				} else {
					// 不是女串最后一位，则进行下一个字符的对比
					i++;
					j++;
				}
			}

		return -1;
	}
	
	/**
	 * 此算法拓展自Sunday字符串搜索算法.
	 * @param sArr 源数组
	 * @param tArr 目标数组
	 * @param fromindex 指定从哪个下标开始检索
	 * @see SundayUtil#indexOf(char[], char[], int)
	 * @return
	 */
	public final static int indexOf(int[] sArr, int[] tArr, int fromindex) {
		var tLen = tArr.length;
		// 若目标数组为空数组，直接返回0
		if (tLen == 0)
			return 0;
		
		var difference = sArr.length - tLen;// 两个数组的长度差
		var tArrLastIndex = tLen - 1;// 目标数组的最后一个变量的下标
		int i = fromindex, j = 0;
		// + j是因为可能会出现最后一次移动源数组剩余长度与目标数组长度不一致的情况
		int current;
		while (i <= (current = difference + j))
			if (sArr[i] != tArr[j]) {
				/*
				 * 若源数组与目标数组当前变量不相等，说明目标数组已经和源数组中最后可能相等的变量比较过了，
				 * 并且后面也没有可比较的了，所以返回-1
				 */
				if (i == current)
					return -1;

				/*
				 * 若源数组的中间部分与目标数组匹配，且结果不相等， 就从目标数组最后面开始，
				 * 找出目标数组最后一位的下一位对应源数组的变量在目标数组中是否存在
				 */
				var pos = -1;
				// 判断目标数组中是否存在末尾下一个位置对应的源数组的变量
				var temp = i - j + tLen;
				var nextLast = sArr[temp];
				flag: for (var k = tArrLastIndex; k >= 0; k--)
					if (tArr[k] == nextLast) {
						pos = k;
						break flag;
					}

				if (pos == -1) {
					// 不存在则直接跳到再下一位，目标数组从头开始
					i = temp + 1;
					j = 0;
				} else {
					// 存在则将这个变量与目标数组最右边与它相同的变量对齐，并再次从头开始比较
					i = temp - pos;
					j = 0;
				}

			} else {
				// 若源数组与目标数组当前变量相等
				if (j == tArrLastIndex) {
					// 若比较到了目标数组的最后一位，说明已经存在
					return i - j;
				} else {
					// 不是目标数组最后一位，则进行下一个变量的对比
					i++;
					j++;
				}
			}

		return -1;
	}
	
	/**
	 * 此算法拓展自Sunday字符串搜索算法.
	 * @param sArr 源数组
	 * @param tArr 目标数组
	 * @param fromindex 指定从哪个下标开始检索
	 * @see SundayUtil#indexOf(char[], char[], int)
	 * @return
	 */
	public final static int indexOf(byte[] sArr, byte[] tArr, int fromindex) {
		var tLen = tArr.length;
		// 若目标数组为空数组，直接返回0
		if (tLen == 0)
			return 0;

		var difference = sArr.length - tLen;// 两个数组的长度差
		var tArrLastIndex = tLen - 1;// 目标数组的最后一个变量的下标
		int i = fromindex, j = 0;
		// + j是因为可能会出现最后一次移动源数组剩余长度与目标数组长度不一致的情况
		int current;
		while (i <= (current = difference + j))
			if (sArr[i] != tArr[j]) {
				/*
				 * 若源数组与目标数组当前变量不相等，说明目标数组已经和源数组中最后可能相等的变量比较过了，
				 * 并且后面也没有可比较的了，所以返回-1
				 */
				if (i == current)
					return -1;

				/*
				 * 若源数组的中间部分与目标数组匹配，且结果不相等， 就从目标数组最后面开始，
				 * 找出目标数组最后一位的下一位对应源数组的变量在目标数组中是否存在
				 */
				var pos = -1;
				// 判断目标数组中是否存在末尾下一个位置对应的源数组的变量
				var temp = i - j + tLen;
				var nextLast = sArr[temp];
				flag: for (var k = tArrLastIndex; k >= 0; k--)
					if (tArr[k] == nextLast) {
						pos = k;
						break flag;
					}

				if (pos == -1) {
					// 不存在则直接跳到再下一位，目标数组从头开始
					i = temp + 1;
					j = 0;
				} else {
					// 存在则将这个变量与目标数组最右边与它相同的变量对齐，并再次从头开始比较
					i = temp - pos;
					j = 0;
				}

			} else {
				// 若源数组与目标数组当前变量相等
				if (j == tArrLastIndex) {
					// 若比较到了目标数组的最后一位，说明已经存在
					return i - j;
				} else {
					// 不是目标数组最后一位，则进行下一个变量的对比
					i++;
					j++;
				}
			}

		return -1;
	}
	
	/**
	 * 此算法拓展自Sunday字符串搜索算法.
	 * @param sArr 源数组
	 * @param tArr 目标数组
	 * @param fromindex 指定从哪个下标开始检索
	 * @see SundayUtil#indexOf(char[], char[], int)
	 * @return
	 */
	public final static int indexOf(double[] sArr, double[] tArr, int fromindex) {
		var tLen = tArr.length;
		// 若目标数组为空数组，直接返回0
		if (tLen == 0)
			return 0;
		
		var difference = sArr.length - tLen;// 两个数组的长度差
		var tArrLastIndex = tLen - 1;// 目标数组的最后一个变量的下标
		int i = fromindex, j = 0;
		// + j是因为可能会出现最后一次移动源数组剩余长度与目标数组长度不一致的情况
		int current;
		while (i <= (current = difference + j))
			if (sArr[i] != tArr[j]) {
				/*
				 * 若源数组与目标数组当前变量不相等，说明目标数组已经和源数组中最后可能相等的变量比较过了，
				 * 并且后面也没有可比较的了，所以返回-1
				 */
				if (i == current)
					return -1;

				/*
				 * 若源数组的中间部分与目标数组匹配，且结果不相等， 就从目标数组最后面开始，
				 * 找出目标数组最后一位的下一位对应源数组的变量在目标数组中是否存在
				 */
				var pos = -1;
				// 判断目标数组中是否存在末尾下一个位置对应的源数组的变量
				var temp = i - j + tLen;
				var nextLast = sArr[temp];
				flag: for (var k = tArrLastIndex; k >= 0; k--)
					if (tArr[k] == nextLast) {
						pos = k;
						break flag;
					}

				if (pos == -1) {
					// 不存在则直接跳到再下一位，目标数组从头开始
					i = temp + 1;
					j = 0;
				} else {
					// 存在则将这个变量与目标数组最右边与它相同的变量对齐，并再次从头开始比较
					i = temp - pos;
					j = 0;
				}

			} else {
				// 若源数组与目标数组当前变量相等
				if (j == tArrLastIndex) {
					// 若比较到了目标数组的最后一位，说明已经存在
					return i - j;
				} else {
					// 不是目标数组最后一位，则进行下一个变量的对比
					i++;
					j++;
				}
			}

		return -1;
	}

}