package qw.easyFrame.tools.algorithm;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import qw.easyFrame.enums.SecretKeySize;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.IOUtil.write;
import static qw.easyFrame.tools.statics.EncoderUtil.encode;
import static qw.easyFrame.tools.statics.EncoderUtil.decode;

/**
 * AES对称加密工具类.<br>
 * 类中所有涉及字符串转换为byte[]的代码，均使用系统默认字符集。
 * 若希望使用指定字符集，请自行编码转换，并直接使用byte[]传参。<br>
 * 为保证安全，AESUtil的实例不应当被序列化和反序列化。
 * @author Rex
 *
 */
public class AESUtil implements Closeable {
	
	/**
	 * 标记该AES加密工具对象是否处在CBC加密模式.
	 */
	transient private boolean isCBC;
	
	/**
	 * 密码器.
	 */
	transient private Cipher cipher;
	
	/**
	 * 用于记录Cipher对象的工作模式（加密模式或解密模式）.<br>
	 * -1表示尚未初始化Cipher对象，即：Cipher对象尚未准备好开始工作。
	 */
	transient private int opmode;
	
	/**
	 * 密钥长度（AES加密强度）.
	 */
	transient private SecretKeySize keySize;
	
	/**
	 * 密钥.
	 */
	transient private SecretKeySpec key;
	
	/**
	 * iv参数.
	 */
	transient private byte[] iv;
	
	/**
	 * CBC模式的初始向量.
	 */
	transient private IvParameterSpec ivps;
    
	/**
	 * 默认使用256位密钥、ECB模式.
	 * @param key 原始密钥
	 */
	public AESUtil(final String key) {
		this(key.getBytes(), SecretKeySize.AES_ECB_256, null);
	}

	/**
	 * @param key 原始密钥
	 * @param keySize 密钥长度（AES加密强度和模式）
	 */
	public AESUtil(final String key, SecretKeySize keySize) {
		this(key.getBytes(), keySize, null);
	}
	
	/**
	 * @param key 原始密钥
	 * @param keySize 密钥长度（AES加密强度和模式）
	 * @param iv 指定的IV参数，此参数只在CBC模式下生效
	 */
	public AESUtil(final byte[] key, SecretKeySize keySize, byte[] iv) {
		if (key.length == 0)
			throw new DefinedRuntimeException(false, "密钥长度至少为1");

		try {
			// 创建密码器
			var transformation = keySize.getTransformation();
			this.isCBC = transformation.startsWith("AES/CBC", 0);
			this.cipher = Cipher.getInstance(keySize.getTransformation());
			this.opmode = -1;
			this.keySize = keySize;
			this.key = getSecretKey(key);
			setIv(iv);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new DefinedRuntimeException(false, "创建密码器失败", e);
		}
	}
	
	/**
	 * 使用随机的IV参数进行AES加密/解密操作.<br>
	 * 此方法只在CBC模式下生效。
	 */
	public void setRandomIv() {
		if (isCBC) {
			var iv = new byte[16];
			var ran = MTRandomFast.inThreadLocal();
			ran.nextBytes(iv);
			setIv(iv);
		}
	}
	
	/**
	 * 使用指定的IV参数进行AES加密/解密操作.<br>
	 * 此方法只在CBC模式下生效。
	 * @param iv
	 */
	public void setIv(byte[] iv) {
		if (isCBC && iv != null && iv.length == 16) {
			this.iv = iv;
			this.ivps = null;
			this.opmode = -1;
		}
	}

	/**
     * AES加密操作.
     * @param content 待加密内容
     * @return 返回Base64转码后的加密数据
     */
	public String encrypt(String content) {
		var encode = encode(encrypt(content.getBytes()));
		return new String(encode, 0, encode.length);
	}
	
	/**
     * AES加密操作.
     * @param content 待加密内容
     * @return
     */
	public byte[] encrypt(byte[] content) {
		try {
			final var cipher = this.cipher;
			// 初始化为加密模式的密码器
			initCipher(Cipher.ENCRYPT_MODE);
			// 加密
			return cipher.doFinal(content);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new DefinedRuntimeException(false, "密钥初始化失败", e);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new DefinedRuntimeException(false, "加密失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "加密程序出现未知异常", e);
		}
	}

	/**
	 * AES加密文件.
	 * @param src 待加密文件
	 * @param target 加密文件存放位置
	 */
	public void encrypt(File src, File target) {
		if (src.isFile()) {
			var length = src.length();
			if (length > Integer.MAX_VALUE)
				throw new DefinedRuntimeException(false, "文件体积超出2GiB上限");

			try (var in = new FileInputStream(src)) {
				var len = (int) length;
				var data = new byte[len];
				in.read(data, 0, len);
				write(encrypt(data), target, false);
			} catch (DefinedRuntimeException e) {
				throw e;
			} catch (OutOfMemoryError e) {
				throw new DefinedRuntimeException(false, "内存不足！", e);
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "加密文件失败", e);
			}
		}
	}
    
    /**
     * AES解密操作.
     * @param content 密文byte数组转化后的Base64字符串
     * @return
     */
	public String decrypt(String content) {
		var bs = decrypt(decode(content.getBytes()));
		return new String(bs, 0, bs.length);
	}
	
    /**
     * AES解密操作.
     * @param content
     * @return
     */
	public byte[] decrypt(byte[] content) {
		try {
			final var cipher = this.cipher;
			// 使用密钥初始化，设置为解密模式
			initCipher(Cipher.DECRYPT_MODE);
			// 执行操作
			return cipher.doFinal(content);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new DefinedRuntimeException(false, "密钥初始化失败", e);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new DefinedRuntimeException(false, "解密失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "解密程序出现未知异常", e);
		}
	}
	
	/**
	 * AES解密文件.
	 * @param src 待解密文件
	 * @param target 解密后文件存放位置
	 */
	public void decrypt(File src, File target) {
		if (src.isFile()) {
			var length = src.length();
			if (length > Integer.MAX_VALUE)
				throw new DefinedRuntimeException(false, "文件体积超出2GiB上限");
			
			try (var in = new FileInputStream(src)) {
				var len = (int) length;
				var data = new byte[len];
				in.read(data, 0, len);
				write(decrypt(data), target, false);
			} catch (DefinedRuntimeException e) {
				throw e;
			} catch (OutOfMemoryError e) {
				throw new DefinedRuntimeException(false, "内存不足！", e);
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "解密文件失败", e);
			}
		}
	}

	/**
	 * 初始化Cipher对象.
	 * @param opmode
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 */
	private final void initCipher(int opmode) throws InvalidKeyException, InvalidAlgorithmParameterException {
		// 判断Cipher是否已被正确初始化
		if (this.opmode != opmode) {
			if (isCBC)
				cipher.init(opmode, key, getIvps());
			else
				cipher.init(opmode, key);
			this.opmode = opmode;
		}
	}
    
    /**
     * 生成密钥.
     * @return
     */
	private final SecretKeySpec getSecretKey(final byte[] key) {
		try {
			// 生成指定算法密钥的生成器
			final var kg = KeyGenerator.getInstance("AES");
			// 初始化密钥生成器
			kg.init(keySize.getKeySize(), SecureRandom.getInstanceStrong());
			// 生成一个密钥，转换为AES专用密钥
			return new SecretKeySpec(kg.generateKey()/* 生成一个密钥 */.getEncoded(), "AES");
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "生成密钥失败", e);
		}
	}
	
	/**
	 * 生成CBC模式必须的初始化参数.
	 * @return
	 */
	private final IvParameterSpec getIvps() {
		if (ivps == null) {
			if (iv == null)
				setRandomIv();
			ivps = new IvParameterSpec(iv);
		}
		return ivps;
	}
	
	/**
	 * 使用此方法可将类变量全部重置为null.<br>
	 * 使用场景：在多线程环境中，对象可能在多个线程间共享。
	 * 例如在Tomcat编程中，对象可能被保存在Session或ServletContext中，供多个request取用。
	 * 此时为防止密钥泄露，某个使用者将对象使用完毕并确定对象不会再被其它线程取用后，
	 * 可使用此方法重置对象，以免密钥泄露。
	 */
	@Override
	public void close() {
		this.isCBC = false;
		this.cipher = null;
		this.opmode = -1;
		this.keySize = null;
		this.key = null;
		this.iv = null;
		this.ivps = null;
	}
    
}