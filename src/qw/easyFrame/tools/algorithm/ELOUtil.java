package qw.easyFrame.tools.algorithm;

import java.io.Serializable;
import java.math.BigDecimal;
import qw.easyFrame.tools.statics.NumberUtil;
import static qw.easyFrame.tools.statics.NumberUtil.divide;
import static qw.easyFrame.tools.statics.NumberUtil.bigDecimal;
import static java.lang.StrictMath.pow;
import static java.math.MathContext.UNLIMITED;

/**
 * ELO（埃洛等级分系统）算法工具类.
 * @author Rex
 *
 */
// 从JDK 17开始，JVM已默认启用严格的浮点数语义，故不再需要strictfp关键字
public /* strictfp */ class ELOUtil implements Serializable {
	private static final long serialVersionUID = 20230310L;

	/**
	 * 每个玩家的默认胜率为50%.
	 */
	private static final BigDecimal HALF = new BigDecimal(new char[] { '0', '.', '5' }, 0, 3, UNLIMITED);
	
	/**
	 * 根据公式可以得出，当K值相同的情况下，分母越高，积分变化越小。
	 * 总体来说400是一个平衡的、能让多数玩家的积分保持正态分布的值.
	 */
	private static final BigDecimal FOUR_HUNDREDS = new BigDecimal(400);
	
	/**
	 * 玩家A最新得分.
	 */
	private BigDecimal ra;
	
	/**
	 * 玩家B最新得分.
	 */
	private BigDecimal rb;
	
	/**
	 * 极限值，代表理论上每轮最多可以输/赢的分数.
	 */
	private BigDecimal k;
	
	/**
	 * 四舍五入的精度，默认为3.
	 */
	private int scale;
	
	/**
	 * 是否限制最低分为0.<br>
	 * 若为true，则玩家的最低分不会低于0。<br>
	 * 默认为false。
	 */
	private boolean zeroed;
	
	/**
	 * 是否开启整数计分模式.<br>
	 * 在整数计分模式下，最终计算出的得分只保留整数部分（四舍五入）。<br>
	 * 默认为false。<br>
	 * 注意：此模式下，ra和rb的初始值仍然可以是浮点数。<br>
	 * 注意：即使在此模式下也强烈不建议将scale设为0，以免计算预估胜率不精确。
	 */
	private boolean intMode;
	
	public ELOUtil() {
		super();
	}
	
	/**
	 * 当仅需计算预估胜率时，使用此构造器.<br>
	 * 计算预估胜率时，仅需ra、rb、sacle参数即可。
	 * @param ra 玩家A本轮竞技前得分
	 * @param rb 玩家B本轮竞技前得分
	 * @param scale 四舍五入的精度
	 */
	public ELOUtil(BigDecimal ra, BigDecimal rb, int scale) {
		this(ra, rb, null, scale);
	}
	
	/**
	 * 全参构造器.
	 * @param ra 玩家A本轮竞技前得分
	 * @param rb 玩家B本轮竞技前得分
	 * @param k 极限值，代表理论上每轮最多可以输/赢的分数
	 * @param scale 四舍五入的精度
	 */
	public ELOUtil(BigDecimal ra, BigDecimal rb, BigDecimal k, int scale) {
		this.ra = ra;
		this.rb = rb;
		this.k = k;
		this.scale = scale;
	}

	public BigDecimal getRa() {
		return ra;
	}
	public void setRa(BigDecimal ra) {
		this.ra = ra;
	}
	public BigDecimal getRb() {
		return rb;
	}
	public void setRb(BigDecimal rb) {
		this.rb = rb;
	}
	public BigDecimal getK() {
		return k;
	}
	public void setK(BigDecimal k) {
		this.k = k;
	}
	public int getScale() {
		return scale;
	}
	public void setScale(int scale) {
		this.scale = scale;
	}
	public boolean isZeroed() {
		return zeroed;
	}
	public void setZeroed(boolean zeroed) {
		this.zeroed = zeroed;
	}
	public boolean isIntMode() {
		return intMode;
	}
	public void setIntMode(boolean intMode) {
		this.intMode = intMode;
	}

	public BigDecimal[] getRaAndRb() {
		return new BigDecimal[] { ra, rb };
	}
	
	public void setRaAndRb(BigDecimal ra, BigDecimal rb) {
		this.ra = ra;
		this.rb = rb;
	}
	
	public void setRaAndRb(int ra, int rb) {
		this.ra = bigDecimal(ra);
		this.rb = bigDecimal(rb);
	}
	
	public void setK(int k) {
		this.k = bigDecimal(k);
	}

	/**
	 * 计算双方预估胜率.
	 * @return 双方预估胜率
	 */
	public BigDecimal[] winRate() {
		// 默认各为50%
		var ea = HALF;
		var eb = ea;
		if (ra.compareTo(rb) != 0) {
			final var one = BigDecimal.ONE;
			final var four_hundreds = FOUR_HUNDREDS;
			// Ea = 1/{1+10^[(Rb-Ra)/400]}
			var pow = pow(10, divide(rb.subtract(ra), four_hundreds, scale).doubleValue());
			ea = divide(one, one.add(bigDecimal(pow)), scale);
			// Eb = 1-Ea
			eb = one.subtract(ea);
		}
		return new BigDecimal[] { ea, eb };
	}

	/**
	 * 玩家A获胜.
	 * 通过ELO算法计算竞技得分。
	 */
	public ELOUtil aWin() {
		// Ra=Ra+K(1-Ea)=Ra+K(Eb)
		var rate = winRate();
		var score = this.k.multiply(rate[1]);
		this.ra = this.ra.add(score);
		// 玩家B减少的分数 等于 玩家A增加的分数
		this.rb = this.rb.subtract(score);

		return limit();
	}
	
	/**
	 * 玩家B获胜.
	 * 通过ELO算法计算竞技得分。
	 */
	public ELOUtil bWin() {
		// Rb=Rb+K(1-Eb)=Rb+K(Ea)
		var rate = winRate();
		var score = this.k.multiply(rate[0]);
		this.rb = this.rb.add(score);
		// 玩家A减少的分数 等于 玩家B增加的分数
		this.ra = this.ra.subtract(score);

		return limit();
	}
	
	/**
	 * 平局.<br>
	 * 通过ELO算法计算竞技得分。<br>
	 * 注意：若调用者希望平局时不计分，请不要调用此方法。
	 */
	public ELOUtil draw() {
		var rate = winRate();
		// 若两位玩家胜率相同，不会计分
		if (rate[0] != rate[1]) {
			// Ra=Ra+K(0.5-Ea)
			final var half = HALF;
			var score = this.k.multiply(half.subtract(rate[0]));// 可能为负数
			this.ra = this.ra.add(score);
			// 玩家B减少/增加的分数 等于 玩家A增加/减少的分数
			this.rb = this.rb.subtract(score);
		}

		return limit();
	}
	
	/**
	 * 判断是否开启了整数计分模式和最低分为0分的限制，并进行相应处理.
	 */
	private final ELOUtil limit() {
		if (zeroed) {
			// 若ra或rb参数小于0，则将其重新赋值为0
			final var zero = BigDecimal.ZERO;
			if (this.ra.compareTo(zero) < 0)
				this.ra = zero;
			if (this.rb.compareTo(zero) < 0)
				this.rb = zero;
		}

		if (intMode) {
			// 若开启了整数计分模式，则四舍五入ra和rb为整数
			this.ra = NumberUtil.setScale(this.ra, 0);
			this.rb = NumberUtil.setScale(this.rb, 0);
		}

		return this;
	}

}