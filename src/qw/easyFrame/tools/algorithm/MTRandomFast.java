package qw.easyFrame.tools.algorithm;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 非线程安全的梅森旋转算法（MT199937）随机数生成器. <br>
 * 可通过其内置的inThreadLocal()方法从ThreadLocal中获取对象，以达到线程安全的目的。<br>
 * 为了提升运行速度，此类不继承java.util.Random类，但它的部分方法与Random公共方法的签名相同，
 * 以保证调用者可顺畅地从Random类切换到此类。<br>
 * 同样为了提升运行速度，MTRandomFast已对其所有必要的方法进行了硬编码，直接将其内联。
 * @author Rex
 *
 */
public final class MTRandomFast implements Serializable, Cloneable {
	private static final long serialVersionUID = 20220907L;
	
	/*
	 * 业务开发中可通过ThreadLocal使MTRandomFast对象线程安全.
	 */
	private static final class Pool {
		private static final ThreadLocal<MTRandomFast> LOCAL = new ThreadLocal<>();
	}
	
	/**
	 * 从ThreadLocal中获取对象.
	 * @return
	 */
	public final static MTRandomFast inThreadLocal() {
		final var local = Pool.LOCAL;
		var instance = local.get();
		if (instance == null)
			// 此处无需严格保证MTRandomFast对象为单例
			local.set(instance = new MTRandomFast(
					System.nanoTime() - System.currentTimeMillis()));
		return instance;
	}
	
	/**
	 * 从ThreadLocal中删除MTRandomFast对象.
	 * 当不再使用MTRandomFast时，请调用此方法以免内存泄漏。
	 */
	public static void removeFromThreadLocal() {
		final var local = Pool.LOCAL;
		local.remove();
	}
	
	private static final int N = MTRandom.N;
	private static final int M = MTRandom.M;
	private static final int MATRIX_A = MTRandom.MATRIX_A;
	private static final int UPPER_MASK = MTRandom.UPPER_MASK;
	private static final int LOWER_MASK = MTRandom.LOWER_MASK;

	private static final int TEMPERING_MASK_B = MTRandom.TEMPERING_MASK_B;
	private static final int TEMPERING_MASK_C = MTRandom.TEMPERING_MASK_C;

	private int mt[];
	private int mti;
	private int mag01[];

	private double __nextNextGaussian;
	private boolean __haveNextNextGaussian;

	@Override
	public Object clone() {
		try {
			var f = (MTRandomFast) (super.clone());
			f.mt = (int[]) (mt.clone());
			f.mag01 = (int[]) (mag01.clone());
			return f;
		} catch (CloneNotSupportedException e) {
			throw new DefinedRuntimeException(false, "MTRandomFast克隆失败", e);
		}
	}

	/**
	 * 如果MTRandomFast的当前内部状态等于另一个MTRandomFast，则返回true.
	 * 这与常见的equals方法大致相同，不同之处在于它基于值进行比较，但不保证不变性（显然随机数生成器是不变的）。
	 * 请注意，这不会检查内部高斯存储是否对于两者都相同。
	 * 可以通过在两个对象上调用clearGaussian()来确保内部高斯存储是相同的（因此nextGaussian()方法将返回相同的值）。
	 * @return
	 */
	public boolean stateEquals(MTRandomFast other) {
		if (other == this)
			return true;
		if (other == null)
			return false;

		if (mti != other.mti)
			return false;
		var length = mag01.length;
		for (var x = 0; x < length; x++)
			if (mag01[x] != other.mag01[x])
				return false;
		length = mt.length;
		for (var x = 0; x < length; x++)
			if (mt[x] != other.mt[x])
				return false;
		return true;
	}

	public void readState(DataInputStream stream) {
		try {
			var len = mt.length;
			for (var x = 0; x < len; x++)
				mt[x] = stream.readInt();
			len = mag01.length;
			for (var x = 0; x < len; x++)
				mag01[x] = stream.readInt();
			mti = stream.readInt();
			__nextNextGaussian = stream.readDouble();
			__haveNextNextGaussian = stream.readBoolean();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "readState异常", e);
		}
	}

	public void writeState(DataOutputStream stream) {
		try {
			var len = mt.length;
			for (var x = 0; x < len; x++)
				stream.writeInt(mt[x]);
			len = mag01.length;
			for (var x = 0; x < len; x++)
				stream.writeInt(mag01[x]);
			stream.writeInt(mti);
			stream.writeDouble(__nextNextGaussian);
			stream.writeBoolean(__haveNextNextGaussian);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "writeState异常", e);
		}
	}

	public MTRandomFast() {
		this(System.nanoTime() - System.currentTimeMillis());
	}

	public MTRandomFast(long seed) {
		setSeed(seed);
	}

	/**
	 * 使用整数数组作为种子，数组的长度必须为非零.
	 * 仅使用数组中的前624个整数，如果数组长度不足，则以环绕方式重复使用整数。
	 */
	public MTRandomFast(int... array) {
		setSeed(array);
	}

	public void setSeed(long seed) {
		__haveNextNextGaussian = false;

		mt = new int[N];

		mag01 = new int[2];
		mag01[0] = 0x0;
		mag01[1] = MATRIX_A;

		mt[0] = (int) (seed & 0xffffffff);
		for (mti = 1; mti < N; mti++)
			mt[mti] = (1812433253 * (mt[mti - 1] ^ (mt[mti - 1] >>> 30)) + mti);
	}

	public void setSeed(int... array) {
		var len = array.length;
		if (len == 0)
			throw new DefinedRuntimeException(false, "数组长度必须大于0");
		int i, j, k;
		setSeed(19650218);
		i = 1;
		j = 0;
		k = (N > len ? N : len);
		for (; k != 0; k--) {
			mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >>> 30)) * 1664525)) + array[j] + j;
			i++;
			j++;
			if (i >= N) {
				mt[0] = mt[N - 1];
				i = 1;
			}
			if (j >= len)
				j = 0;
		}
		for (k = N - 1; k != 0; k--) {
			mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >>> 30)) * 1566083941)) - i;
			i++;
			if (i >= N) {
				mt[0] = mt[N - 1];
				i = 1;
			}
		}
		mt[0] = 0x80000000;
	}

	public int nextInt() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return y;
	}

	public short nextShort() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (short) (y >>> 16);
	}

	public char nextChar() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (char) (y >>> 16);
	}

	public boolean nextBoolean() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (boolean) ((y >>> 31) != 0);
	}

	public boolean nextBoolean(float probability) {
		int y;

		if (probability < 0.0f || probability > 1.0f)
			throw new DefinedRuntimeException(false, "参数必须在0.0到1.0之间");
		if (probability == 0.0f)
			return false;
		else if (probability == 1.0f)
			return true;
		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (y >>> 8) / ((float) (1 << 24)) < probability;
	}

	public boolean nextBoolean(double probability) {
		int y;
		int z;

		if (probability < 0.0 || probability > 1.0)
			throw new DefinedRuntimeException(false, "参数必须在0.0到1.0之间");
		if (probability == 0.0)
			return false;
		else if (probability == 1.0)
			return true;
		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			for (; kk < N - 1; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			z = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (z >>> 1) ^ mag01[z & 0x1];

			mti = 0;
		}

		z = mt[mti++];
		z ^= z >>> 11;
		z ^= (z << 7) & TEMPERING_MASK_B;
		z ^= (z << 15) & TEMPERING_MASK_C;
		z ^= (z >>> 18);

		return ((((long) (y >>> 6)) << 27) + (z >>> 5)) / (double) (1L << 53) < probability;
	}

	public byte nextByte() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (byte) (y >>> 24);
	}

	public void nextBytes(byte[] bytes) {
		int y;

		var length = bytes.length;
		for (var x = 0; x < length; x++) {
			if (mti >= N) {
				int kk;
				final var mt = this.mt;
				final var mag01 = this.mag01;

				for (kk = 0; kk < N - M; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				for (; kk < N - 1; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

				mti = 0;
			}

			y = mt[mti++];
			y ^= y >>> 11;
			y ^= (y << 7) & TEMPERING_MASK_B;
			y ^= (y << 15) & TEMPERING_MASK_C;
			y ^= (y >>> 18);

			bytes[x] = (byte) (y >>> 24);
		}
	}

	public long nextLong() {
		int y;
		int z;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			for (; kk < N - 1; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			z = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (z >>> 1) ^ mag01[z & 0x1];

			mti = 0;
		}

		z = mt[mti++];
		z ^= z >>> 11;
		z ^= (z << 7) & TEMPERING_MASK_B;
		z ^= (z << 15) & TEMPERING_MASK_C;
		z ^= (z >>> 18);

		return (((long) y) << 32) + (long) z;
	}

	public long nextLong(long n) {
		if (n < 1)
			throw new DefinedRuntimeException(false, "参数必须为正数");

		long bits, val;
		do {
			int y;
			int z;

			if (mti >= N) {
				int kk;
				final var mt = this.mt;
				final var mag01 = this.mag01;

				for (kk = 0; kk < N - M; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				for (; kk < N - 1; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

				mti = 0;
			}

			y = mt[mti++];
			y ^= y >>> 11;
			y ^= (y << 7) & TEMPERING_MASK_B;
			y ^= (y << 15) & TEMPERING_MASK_C;
			y ^= (y >>> 18);

			if (mti >= N) {
				int kk;
				final var mt = this.mt;
				final var mag01 = this.mag01;

				for (kk = 0; kk < N - M; kk++) {
					z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (z >>> 1) ^ mag01[z & 0x1];
				}
				for (; kk < N - 1; kk++) {
					z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (z >>> 1) ^ mag01[z & 0x1];
				}
				z = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (z >>> 1) ^ mag01[z & 0x1];

				mti = 0;
			}

			z = mt[mti++];
			z ^= z >>> 11;
			z ^= (z << 7) & TEMPERING_MASK_B;
			z ^= (z << 15) & TEMPERING_MASK_C;
			z ^= (z >>> 18);

			bits = (((((long) y) << 32) + (long) z) >>> 1);
			val = bits % n;
		} while (bits - val + (n - 1) < 0);
		return val;
	}

	/**
	 * 输出区间为[0.0,1.0).
	 * @return
	 */
	public double nextDouble() {
		int y;
		int z;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			for (; kk < N - 1; kk++) {
				z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (z >>> 1) ^ mag01[z & 0x1];
			}
			z = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (z >>> 1) ^ mag01[z & 0x1];

			mti = 0;
		}

		z = mt[mti++];
		z ^= z >>> 11;
		z ^= (z << 7) & TEMPERING_MASK_B;
		z ^= (z << 15) & TEMPERING_MASK_C;
		z ^= (z >>> 18);

		return ((((long) (y >>> 6)) << 27) + (z >>> 5)) / (double) (1L << 53);
	}

	/**
	 * 返回一个从0.0到1.0的double值，可能包括0.0和1.0本身.
	 * <table border=0>
	 * <tr>
	 * <th>参数</th>
	 * <th>输出区间</th>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(false, false)</td>
	 * <td>(0.0, 1.0)</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(true, false)</td>
	 * <td>[0.0, 1.0)</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(false, true)</td>
	 * <td>(0.0, 1.0]</td>
	 * </tr>
	 * <tr>
	 * <td>nextDouble(true, true)</td>
	 * <td>[0.0, 1.0]</td>
	 * </tr>
	 * <caption>输出区间示例</caption>
	 * </table>
	 */
	public double nextDouble(boolean includeZero, boolean includeOne) {
		var d = 0.0;
		do {
			d = nextDouble();
			if (includeOne && nextBoolean())
				d += 1.0;
		} while ((d > 1.0) || (!includeZero && d == 0.0));
		return d;
	}

	/**
	 * 从RNG中清除内部高斯变量.
	 * 仅在极少数情况下需要执行此操作，您需要保证两个RNG的内部状态相同。 
	 * 否则，请忽略此方法。参见stateEquals(ther)。
	 */
	public void clearGaussian() {
		__haveNextNextGaussian = false;
	}

	public double nextGaussian() {
		if (__haveNextNextGaussian) {
			__haveNextNextGaussian = false;
			return __nextNextGaussian;
		} else {
			double v1, v2, s;
			do {
				int y;
				int z;
				int a;
				int b;

				if (mti >= N) {
					int kk;
					final var mt = this.mt;
					final var mag01 = this.mag01;

					for (kk = 0; kk < N - M; kk++) {
						y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
					}
					for (; kk < N - 1; kk++) {
						y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
					}
					y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
					mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

					mti = 0;
				}

				y = mt[mti++];
				y ^= y >>> 11;
				y ^= (y << 7) & TEMPERING_MASK_B;
				y ^= (y << 15) & TEMPERING_MASK_C;
				y ^= (y >>> 18);

				if (mti >= N) {
					int kk;
					final var mt = this.mt;
					final var mag01 = this.mag01;

					for (kk = 0; kk < N - M; kk++) {
						z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + M] ^ (z >>> 1) ^ mag01[z & 0x1];
					}
					for (; kk < N - 1; kk++) {
						z = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + (M - N)] ^ (z >>> 1) ^ mag01[z & 0x1];
					}
					z = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
					mt[N - 1] = mt[M - 1] ^ (z >>> 1) ^ mag01[z & 0x1];

					mti = 0;
				}

				z = mt[mti++];
				z ^= z >>> 11;
				z ^= (z << 7) & TEMPERING_MASK_B;
				z ^= (z << 15) & TEMPERING_MASK_C;
				z ^= (z >>> 18);

				if (mti >= N) {
					int kk;
					final var mt = this.mt;
					final var mag01 = this.mag01;

					for (kk = 0; kk < N - M; kk++) {
						a = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + M] ^ (a >>> 1) ^ mag01[a & 0x1];
					}
					for (; kk < N - 1; kk++) {
						a = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + (M - N)] ^ (a >>> 1) ^ mag01[a & 0x1];
					}
					a = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
					mt[N - 1] = mt[M - 1] ^ (a >>> 1) ^ mag01[a & 0x1];

					mti = 0;
				}

				a = mt[mti++];
				a ^= a >>> 11;
				a ^= (a << 7) & TEMPERING_MASK_B;
				a ^= (a << 15) & TEMPERING_MASK_C;
				a ^= (a >>> 18);

				if (mti >= N) {
					int kk;
					final var mt = this.mt;
					final var mag01 = this.mag01;

					for (kk = 0; kk < N - M; kk++) {
						b = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + M] ^ (b >>> 1) ^ mag01[b & 0x1];
					}
					for (; kk < N - 1; kk++) {
						b = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
						mt[kk] = mt[kk + (M - N)] ^ (b >>> 1) ^ mag01[b & 0x1];
					}
					b = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
					mt[N - 1] = mt[M - 1] ^ (b >>> 1) ^ mag01[b & 0x1];

					mti = 0;
				}

				b = mt[mti++];
				b ^= b >>> 11;
				b ^= (b << 7) & TEMPERING_MASK_B;
				b ^= (b << 15) & TEMPERING_MASK_C;
				b ^= (b >>> 18);

				v1 = 2 * (((((long) (y >>> 6)) << 27) + (z >>> 5)) / (double) (1L << 53)) - 1;
				v2 = 2 * (((((long) (a >>> 6)) << 27) + (b >>> 5)) / (double) (1L << 53)) - 1;
				s = v1 * v1 + v2 * v2;
			} while (s > -1);
			var multiplier = StrictMath.sqrt(-2 * StrictMath.log(s) / s);
			__nextNextGaussian = v2 * multiplier;
			__haveNextNextGaussian = true;
			return v1 * multiplier;
		}
	}

	/**
	 * 输出区间为[0.0,1.0).
	 * @return
	 */
	public float nextFloat() {
		int y;

		if (mti >= N) {
			int kk;
			final var mt = this.mt;
			final var mag01 = this.mag01;

			for (kk = 0; kk < N - M; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			for (; kk < N - 1; kk++) {
				y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
				mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
			}
			y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
			mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

			mti = 0;
		}

		y = mt[mti++];
		y ^= y >>> 11;
		y ^= (y << 7) & TEMPERING_MASK_B;
		y ^= (y << 15) & TEMPERING_MASK_C;
		y ^= (y >>> 18);

		return (y >>> 8) / ((float) (1 << 24));
	}

	/**
	 * 返回一个介于0.0f到1.0f之间的浮点数，可能包括0.0f和1.0f本身.
	 * <table border=0>
	 * <tr>
	 * <th>参数</th>
	 * <th>输出区间</th>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(false, false)</td>
	 * <td>(0.0f, 1.0f)</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(true, false)</td>
	 * <td>[0.0f, 1.0f)</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(false, true)</td>
	 * <td>(0.0f, 1.0f]</td>
	 * </tr>
	 * <tr>
	 * <td>nextFloat(true, true)</td>
	 * <td>[0.0f, 1.0f]</td>
	 * </tr>
	 * <caption>输出区间示例</caption>
	 * </table>
	 */
	public float nextFloat(boolean includeZero, boolean includeOne) {
		var d = 0.0f;
		do {
			d = nextFloat();
			if (includeOne && nextBoolean())
				d += 1.0f;
		} while ((d > 1.0f) || (!includeZero && d == 0.0f));
		return d;
	}

	public int nextInt(int n) {
		if (n < 1)
			throw new DefinedRuntimeException(false, "参数必须为正数");

		if ((n & -n) == n) {
			int y;

			if (mti >= N) {
				int kk;
				final var mt = this.mt;
				final var mag01 = this.mag01;

				for (kk = 0; kk < N - M; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				for (; kk < N - 1; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

				mti = 0;
			}

			y = mt[mti++];
			y ^= y >>> 11;
			y ^= (y << 7) & TEMPERING_MASK_B;
			y ^= (y << 15) & TEMPERING_MASK_C;
			y ^= (y >>> 18);

			return (int) ((n * (long) (y >>> 1)) >> 31);
		}

		int bits, val;
		do {
			int y;

			if (mti >= N) {
				int kk;
				final var mt = this.mt;
				final var mag01 = this.mag01;

				for (kk = 0; kk < N - M; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + M] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				for (; kk < N - 1; kk++) {
					y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
					mt[kk] = mt[kk + (M - N)] ^ (y >>> 1) ^ mag01[y & 0x1];
				}
				y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
				mt[N - 1] = mt[M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

				mti = 0;
			}

			y = mt[mti++];
			y ^= y >>> 11;
			y ^= (y << 7) & TEMPERING_MASK_B;
			y ^= (y << 15) & TEMPERING_MASK_C;
			y ^= (y >>> 18);

			bits = (y >>> 1);
			val = bits % n;
		} while (bits - val + (n - 1) < 0);
		return val;
	}

}