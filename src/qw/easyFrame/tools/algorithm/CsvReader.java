package qw.easyFrame.tools.algorithm;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import qw.easyFrame.entity.Csv;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.CloseSource;

/**
 * 用于读取csv文件的工具类.<br>
 * 注意：非线程安全
 */
public class CsvReader implements Serializable, Closeable, Iterator<String[]>, Iterable<String[]> {
	private static final long serialVersionUID = 20220828L;

	/**
	 * csv文件.
	 */
	private File csv;
	
	/**
	 * 字符集.
	 */
	private String charset;
	
	private transient FileInputStream fis;
	
	private transient InputStreamReader isr;
	
	private transient BufferedReader br;
	
	/**
	 * 分隔符，默认为英文输入状态下的半角逗号.
	 */
	private String delimiter;
	
	/**
	 * 遍历csv文件时，用于临时保存当前的行.
	 */
	private transient String currLine;
	
	/**
	 * 使用此构造器构造的对象须使用setCsv(File)方法设置csv参数后才能使用.<br>
	 * 使用系统默认字符集、使用英文半角逗号作为分隔符。
	 * @param csv
	 */
	public CsvReader() {
		this(null, null, ",");
	}
	
	/**
	 * 使用系统默认字符集、使用英文半角逗号作为分隔符.
	 * @param csv
	 */
	public CsvReader(File csv) {
		this(csv, null, ",");
	}
	
	/**
	 * 使用英文半角逗号作为分隔符.
	 * @param csv
	 * @param charset
	 */
	public CsvReader(File csv, String charset) {
		this(csv, charset, ",");
	}
	
	public CsvReader(File csv, String charset, String delimiter) {
		this.csv = csv;
		this.charset = charset;
		this.delimiter = delimiter;
	}

	public File getCsv() {
		return csv;
	}
	public void setCsv(File csv) {
		this.csv = csv;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getDelimiter() {
		return delimiter;
	}
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	/**
	 * 将整个csv文件一次全部解析.<br>
	 * 注意：此方法不会缓存解析结果，每此调用均会重新读取csv文件。<br>
	 * 优点：代码简洁，且方便后续操作；<br>
	 * 缺点：内存占用和CPU占用较高，建议在文件体积不大、内存充足时使用。
	 * @return
	 */
	public Csv parseAll() {
		try (var in = new FileInputStream(csv);
				var reader = charset == null ? new InputStreamReader(in)
						: new InputStreamReader(in, charset);
				var buff = new BufferedReader(reader)) {
			
			var list = new ArrayList<String[]>();
			String line = null;
			while ((line = buff.readLine()) != null)
				list.add(line.split(delimiter, 0));
			
			return new Csv(list);
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "不支持的字符集", e);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "文件读取异常或未找到文件", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "解析csv文件时出现异常", e);
		}
	}

	@Override
	public boolean hasNext() {
		try {
			if (this.fis == null) {
				this.fis = new FileInputStream(csv);
				this.isr = charset == null ? new InputStreamReader(fis) : new InputStreamReader(fis, charset);
				this.br = new BufferedReader(isr);
			}
			return (currLine = br.readLine()) != null;
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "不支持的字符集", e);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "文件读取异常或未找到文件", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "解析csv文件时出现异常", e);
		}
	}

	@Override
	public String[] next() {
		return currLine.split(delimiter, 0);
	}
	
	/**
	 * 注意：非线程安全.<br>
	 * 强烈不建议多个线程调用同一个CsvReader对象的迭代器。
	 */
	@Override
	public Iterator<String[]> iterator() {
		close();
		return this;
	}
	
	@Override
	public void close() {
		if (fis != null) {
			CloseSource.close(br, isr, fis);
			fis = null;
			isr = null;
			br = null;
		}
	}

}