package qw.easyFrame.tools.algorithm;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import qw.easyFrame.entity.HashConfig;
import qw.easyFrame.test.RandomString;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.StringUtil.reverse;
import static qw.easyFrame.tools.statics.StringUtil.delete;

/**
 * 基于一般散列算法的摘要/摘要验证算法.<br>
 * 基于一般散列算法的类BCrypt算法，大小写敏感。
 * @author Rex
 *
 */
public class EasyHash {
	
	/**
	 * 用于生成随机字符串.
	 */
	private RandomString ranStr;
	
	/**
	 * Hash算法对象.
	 */
	private MessageDigest app;
	
	/**
	 * 字符集.
	 */
	private String charset;
	
	/**
	 * 循环计算Hash的次数，默认为3.
	 */
	private int times;
	
	/**
	 * 盐字符串的长度，默认为9.
	 */
	private int saltLen;
	
	/**
	 * encode方法生成的摘要的长度，等于saltLen + 64.
	 */
	private int digestLen;
	
	/**
	 * 固定盐.
	 * 算法中使用随机盐+固定盐组成最终盐。
	 */
	private final String fixed;
	
	/**
	 * 系数，用于生成算法所需的某些中间值.
	 * 此变量的值等于((times << 3) - fixed.length) % (saltLen + fixed.length)。
	 */
	private int coefficient;
	
	/**
	 * 采用UTF-8编码，循环计算3次Hash，盐字符串长度为9，使用默认的固定盐.
	 */
	public EasyHash() {
		this(3, 9, null, null);
	}
	
	/**
	 * 采用UTF-8编码，使用默认的固定盐.
	 * @param times 指定循环计算Hash的次数，不能小于3
	 * @param saltLen 指定随机生成的盐字符串的长度，不能小于9
	 */
	public EasyHash(int times, int saltLen) {
		this(times, saltLen, null, null);
	}
	
	/**
	 * @param config EasyHash的配置文件
	 */
	public EasyHash(HashConfig config) {
		this(config.times(), config.saltLen(), config.fixed(), config.charset());
	}
	
	/**
	 * @param times 指定循环计算Hash的次数，不能小于3
	 * @param saltLen 指定随机生成的盐字符串的长度，不能小于9
	 * @param fixed 固定盐（算法中使用随机盐+固定盐组成最终盐）
	 * @param charset 指定编码集
	 */
	public EasyHash(int times, int saltLen, String fixed, String charset) {
		try {
			this.app = MessageDigest.getInstance("SHA-256");
			this.charset = charset == null ? "UTF-8" : charset;
			this.times = times < 3 ? 3 : times;
			this.saltLen = saltLen < 9 ? 9 : saltLen;
			this.digestLen = saltLen + 64;
			this.ranStr = new RandomString(true);
			var bytes = fixed == null ? "ahau.Hash".getBytes() : fixed.getBytes();
			this.fixed = new String(bytes, 0, bytes.length, this.charset);
			var fixedLen = this.fixed.length();
			this.coefficient = ((times << 3) - fixedLen) % (saltLen + fixedLen);
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "初始化EasyHash失败", e);
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "不支持的字符集", e);
		}
	}

	/*
	 * 不提供charset变量、times变量和saltLen变量的setter方法，
	 * 三者的值必须在调用工厂方法时确定。
	 */
	public String getCharset() {
		return charset;
	}
	public int getTimes() {
		return times;
	}
	public int getSaltLen() {
		return saltLen;
	}
	
	/**
	 * digestLen变量的值在构造器内由saltLen + 32计算得出.
	 * @return
	 */
	public int getDigestLen() {
		return digestLen;
	}

	/**
	 * 获取 EasyHash的配置文件.
	 * @return
	 */
	public HashConfig getConfig() {
		return new HashConfig(this.times, this.saltLen, this.fixed, this.charset);
	}
	
	/**
	 * 计算密码的摘要并返回.
	 * 摘要长度为digestLen，即saltLen + 32。
	 * @param pwd
	 * @return
	 */
	public String digest(String pwd) {
		final var ranStr = this.ranStr;
		return encode(pwd, ranStr.random(saltLen));
	}
	
	/**
	 * 验证摘要与原密码是否匹配.
	 * @param pwd 原密码
	 * @param digest 摘要
	 * @return
	 */
	public boolean matches(String pwd, String digest) {
		if (digest.length() == digestLen)
			return encode(pwd, digest.substring(64, digestLen)).equals(digest);
		return false;
	}
	
	/**
	 * 根据指定的明文密码和盐，计算摘要.
	 * @param pwd
	 * @param salt
	 * @return
	 */
	private final String encode(String pwd, String salt) {
		try {
			var salt_ = salt.getBytes();
			salt = new String(salt_, 0, salt_.length, charset);
			var pwd_ = pwd.getBytes();
			pwd = new String(pwd_, 0, pwd_.length, charset);
			var realSalt = delete(salt.concat(fixed), coefficient, 1);

			var builder = new StringBuilder(64);
			for (var i = 0; i < times; i++) {
				app.reset();
				app.update(realSalt.concat(pwd).getBytes());
				
				builder.delete(0, 64);
				for (var b : app.digest()) {
					var s = Integer.toHexString(b & 0xFF);
					if (s.length() == 1)
						builder.append("0");
					builder.append(s);
				}

				pwd = builder.toString();
				pwd = i % 5 == 0 ? reverse(pwd) : pwd;
			}

			var bytes = pwd.getBytes();
			return new String(bytes, 0, bytes.length, charset).concat(salt);
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "不支持的字符集", e);
		}
	}

}