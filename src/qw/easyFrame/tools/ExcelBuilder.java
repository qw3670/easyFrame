package qw.easyFrame.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import qw.easyFrame.entity.Csv;
import qw.easyFrame.interfaces.ExcelTuple;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.StringUtil.changeExtension;

/**
 * 输出实体类信息到Excel文件.<br>
 * 依赖：poi-5.2.3.jar、poi-ooxml-5.2.3.jar、poi-ooxml-full-5.2.3.jar、xmlbeans-5.1.1.jar、
 * commons-collections4-4.4.jar、commons-compress-1.21.jar、commons-io-2.11.0.jar、
 * commons-math3-3.6.1.jar、log4j-api-2.18.0.jar
 * @author AL
 * 
 */
public class ExcelBuilder<T extends ExcelTuple> implements Serializable {
	private static final long serialVersionUID = 20241226L;

	/**
	 * 特殊列.<br>
	 * 特殊列：即使设置了“在需要时自动转换单元格格式为数字格式”也不会被转换格式的列。
	 * 默认为null。
	 * 注意：列的下标从0开始。
	 */
	private int[] spColumns;
	
	/**
	 * 标记是否自动转换单元格格式为数字格式（若待填充的文本为数字）.
	 * 默认为true。
	 * @see ExcelBuilder#setTransform(boolean)
	 */
	private boolean transform = true;
	
	/**
	 * 转换单元格格式为数字格式时，数字保留到小数点后的精度.
	 * 默认为2。
	 * @see ExcelBuilder#setScale(int)
	 */
	private int scale = 2;
	
	/**
	 * 匹配数字(非科学计数法).
	 */
	private final Pattern normalNum = Pattern.compile("^-{0,1}[0-9]+\\.{0,1}[0-9]*$");

	/**
	 * 匹配数字(科学计数法).
	 */
	private final Pattern notationNum = Pattern.compile("^-{0,1}[0-9]+\\.{0,1}[0-9]*[Ee]{1}-{0,1}[0-9]+$");
	
	/**
	 * 封装了Excel信息的Map.
	 */
	private Map<String, List<T>> map;
	
	/**
	 * 最终输出的文件名（完整路径）.
	 * 构造器和set方法不会检查路径是否合法，请调用者自行保证该路径可用.
	 */
	private String file;
	
	/**
	 * 单元格格式，用于转换单元格格式为数字格式.
	 */
	private transient CellStyle style;
	
	/**
	 * CSV格式分隔符，默认为英文输入状态下的半角逗号.<br>
	 * 此参数仅对以CSV格式写出的文件有效。
	 * @see ExcelBuilder#csvWrite()
	 */
	private String delimiter;
	
	/**
	 * 是否冻结表格首行（每个sheet的首行均使用此设置）.<br>
	 * 此项对于以csv格式输出的表格文件无效。
	 */
	private boolean freeze1stRow;
	
	/**
	 * 检查传入的路径的母路径是否存在，若不存在则新建.
	 * @param file
	 */
	private final void checkParentFile(String file) {
		var parentFile = new File(file).getParentFile();
		if (!parentFile.exists())
			parentFile.mkdirs();
		
		this.file = file;
	}
	
	public ExcelBuilder() {
		super();
	}
	
	/**
	 * 构造方法不会检查路径是否合法，请确保该路径可用.
	 * @param file 输出的文件名（完整路径）
	 */
	public ExcelBuilder(String file) {
		checkParentFile(file);
	}
	
	/**
	 * 构造方法不会检查路径是否合法，请确保该路径可用.
	 * @param map key为Excel中每个分页的名称、value为分页的内容
	 * @param file 输出的文件名（完整路径）
	 */
	public ExcelBuilder(Map<String, List<T>> map, String file) {
		this.map = map;
		checkParentFile(file);
	}
	
	/**
	 * 当待输出的Excel表格内容仅有一个sheet分页时，可使用此构造器.<br>
	 * 注意：构造方法不会检查路径是否合法，请确保该路径可用
	 * @param list List&lt;T&gt;
	 * @param sheetName sheet名称
	 * @param file 输出的文件名（完整路径）
	 */
	public ExcelBuilder(List<T> list, String sheetName, String file) {
		this.map = new LinkedHashMap<>();
		this.map.put(sheetName, list);
		checkParentFile(file);
	}
	
	/**
	 * 使用Csv实体类构造ExcelBuilder对象.<br>
	 * 注意：构造方法不会检查路径是否合法，请确保该路径可用
	 * @param csv
	 * @param sheetName sheet名称
	 * @param file 输出的文件名（完整路径）
	 */
	public static final ExcelBuilder<ExcelTuple> createByCsv(Csv csv, String sheetName, String file) {
		return new ExcelBuilder<ExcelTuple>(csv.toExcelables(), sheetName, file);
	}

	/**
	 * 获取特殊列.<br>
	 * 特殊列：即使设置了在需要时自动转换单元格格式为数字格式，也不会被转换格式的列。
	 * 注意：列的下标从0开始。
	 * @return
	 */
	public int[] getSpColumns() {
		return spColumns;
	}

	/**
	 * 设置特殊列.<br>
	 * 特殊列：即使设置了在需要时自动转换单元格格式为数字格式，也不会被转换格式的列。
	 * 若不设置，默认为null。
	 * 注意：列的下标从0开始。
	 * @param spColumns int类型的变长参数
	 */
	public void setSpColumns(int... spColumns) {
		this.spColumns = spColumns;
	}
	
	/**
	 * 设置是否自动转换单元格格式为数字格式（若待填充的文本为数字）.
	 * 若不设置此项，则默认自动转换。
	 * @param transform
	 */
	public void setTransform(boolean transform) {
		this.transform = transform;
	}
	
	/**
	 * 获取是否自动转换单元格格式为数字格式的标记.
	 * @return transform
	 */
	public boolean isTransform() {
		return this.transform;
	}

	/**
	 * 设置转换单元格格式为数字格式时，数字保留到小数点后的精度.
	 * 若不设置此项，则默认为2。
	 * @param scale
	 */
	public void setScale(int scale) {
		if (scale > -1)
			this.scale = scale;
	}
	
	/**
	 * 获取转换单元格格式为数字格式时，数字保留到小数点后的精度.
	 * @return scale
	 */
	public int getScale() {
		return this.scale;
	}
	
	/**
	 * 获取封装了的Excel信息.
	 * @return Map&lt;String, List&lt;T&gt;&gt;
	 */
	public Map<String, List<T>> getMap() {
		return map;
	}

	/**
	 * 设置封装了Excel信息的map.
	 * @param map Map&lt;String, List&lt;T&gt;&gt;
	 */
	public void setMap(Map<String, List<T>> map) {
		this.map = map;
	}
	
	/**
	 * @param list List&lt;T&gt;
	 * @param sheetName sheet名称
	 */
	public void addListAndSheetName(List<T> list, String sheetName) {
		if (map == null)
			map = new LinkedHashMap<>();
		map.put(sheetName, list);
	}

	/**
	 * 获取文件名（完整路径）.
	 * @return file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * 设置输出的文件名（完整路径）.<br>
	 * 方法不会检查路径是否合法，请确保该路径可用。
	 * @param file
	 */
	public void setFile(String file) {
		checkParentFile(file);
	}

	/**
	 * 获取CSV格式分隔符.
	 * @return
	 */
	public String getDelimiter() {
		return this.delimiter == null ? "," : this.delimiter;
	}

	/**
	 * 设置CSV格式分隔符，默认为英文输入状态下的半角逗号.<br>
	 * 此参数仅对以CSV格式写出的文件有效。
	 * @see ExcelBuilder#csvWrite()
	 */
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	/**
	 * 是否冻结表格首行（每个sheet的首行均使用此设置）.<br>
	 * 此项对于以csv格式输出的表格文件无效。
	 * @return
	 */
	public boolean isFreeze1stRow() {
		return freeze1stRow;
	}

	/**
	 * 设置是否冻结表格首行（每个sheet的首行均使用此设置）.<br>
	 * 此项对于以csv格式输出的表格文件无效。
	 * @param freeze1stRow
	 */
	public void setFreeze1stRow(boolean freeze1stRow) {
		this.freeze1stRow = freeze1stRow;
	}

	/**
	 * 输出内容.<br>
	 * 注意：仅当文件扩展名明确为.csv时，才会调用csvWrite()方法。
	 * 其它情况下一律调用xlsxWrite()。<br>
	 * 注意：仅当调用者不知晓最终会输出何种文件格式时，才建议使用此方法自动判断输出格式。
	 * 当调用者明确知晓最终输出文件的格式时，建议直接使用对应的输出方法，以减少CPU调用。
	 * @see ExcelBuilder#csvWrite()
	 * @see ExcelBuilder#xlsxWrite()
	 */
	public void write() {
		if (file.endsWith(".csv"))
			csvWrite();
		else
			xlsxWrite();
	}
	
	/**
	 * 输出为Excel文件.
	 */
	public void xlsxWrite() {
		// 确定文件名
		if (!this.file.endsWith(".xlsx"))
			this.file = changeExtension(this.file, ".xlsx");
		
		try (var out = new FileOutputStream(new File(file), false);
				var excel = new XSSFWorkbook()) {
			// 若map为空，则重构一个map，装入一个空白键值对
			if (map == null || map.isEmpty()) {
				map = new HashMap<>();
				map.put("空", null);
			}

			// 遍历键值对，将表格名称和表格内容传递给下一层，用于输出
			var index = 0;
			for (var entry : map.entrySet()) {
				// 向Excel（缓存区）内填充内容
				writeWorkbook(entry.getValue(), entry.getKey(), excel, index);
				index++;
			}

			excel.write(out);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "输出Excel失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "输出内容到Excel出现未知异常", e);
		}
	}
	
	/**
	 * 输出为csv文件.
	 */
	public void csvWrite() {
		var sheets = map.size();
		final var delimiter = this.delimiter == null ? "," : this.delimiter;
		try {
			// 确定文件名
			if (!this.file.endsWith(".csv"))
				this.file = changeExtension(this.file, ".csv");
			
			// 判断是否需要输出多个文件
			String basePath = null;
			if (sheets != 1)
				basePath = file.substring(0, file.lastIndexOf(".")).concat("(");
			
			var enter = "\r\n";// 换行符
			for (var e : map.entrySet()) {
				var path = Paths.get(basePath == null ? file : basePath + e.getKey() + ").csv");
				
				var value = e.getValue();
				
				/*
				 *  由于绝大多数情况下并不需要输出多个csv文件，
				 *  故为降低代码复杂度，不复用StringBuilder。
				 */
				var builder = new StringBuilder();
				// 写表头
				var header = value.get(0).toExcelHead();
				for (var head : header)
					builder.append(head).append(delimiter);
				builder.deleteCharAt(builder.length() - 1).append(enter);
				
				// 写表格内容
				var size = value.size();
				for (var i = 0; i < size; i++) {
					var infos = value.get(i).toExcelTuple();
					for (var info : infos)
						builder.append(info).append(delimiter);
					builder.deleteCharAt(builder.length() - 1).append(enter);
				}
				Files.write(path, builder.toString().getBytes(),
						StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
			}
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "输出CSV文件出现异常", e);
		}
	}
	
	/**
	 * 向Excel（缓存区）内填充内容.
	 * @param result
	 * @param sheet
	 * @param excel
	 * @param index
	 */
	private final void writeWorkbook(List<T> result, String sheet, Workbook excel, int index) throws Exception {
		// 若sheetName为空，则填充“空”
		if (sheet == null || sheet.isEmpty())
			sheet = "未命名".concat(Integer.toString(index));

		var sht = excel.createSheet(sheet);
		if (freeze1stRow)
			sht.createFreezePane(0, 1, 0, 1);

		// 若集合为空，则输出“空”字符
		if (result == null || result.isEmpty()) {
			sht.createRow(0).createCell(0).setCellValue("空");
			return;
		}
		
		final var normalNum = this.normalNum;
		final var notationNum = this.notationNum;

		// 将集合内元素toExcelTuple()返回的字符串切割成数组
		var header = result.get(0).toExcelHead();
		var row = sht.createRow(0);// 获取此sheet表格的第一行
		var times = header.length;
		// 遍历数组，用于构造表头
		for (var t = 0; t < times; t++)
			// 只取“=”之前的字符
			row.createCell(t).setCellValue(header[t]);

		// 遍历集合，填充表格内容
		var bound = result.size() + 1;
		for (var i = 1; i < bound; i++) {
			row = sht.createRow(i);
			var infos = result.get(i - 1).toExcelTuple();
			var round = infos.length;
			for (var j = 0; j < round; j++) {
				var info = infos[j].strip();

				var cell = row.createCell(j);
				// 判断是否需要转换单元格格式
				if (transform && transform(j)
						&& (normalNum.matcher(info).matches() || notationNum.matcher(info).matches())) {
					cell.setCellStyle(style == null ? createStyle(excel) : style);
					cell.setCellValue(Double.parseDouble(info));
				} else
					cell.setCellValue(info);
			}
		}
	}
	
	/**
	 * 生成数字格式的单元格格式.
	 * @param excel
	 * @return
	 */
	private final CellStyle createStyle(Workbook excel) {
		var newLen = this.scale == 0 ? 1 : this.scale + 2;
		var arr = new char[newLen];
		arr[0] = '0';
		for (var i = 1; i < newLen; i++)
			arr[i] = i == 1 ? '.' : '0';
		// 设置单元格格式为数字格式并保留小数点后的位数
		this.style = excel.createCellStyle();
		this.style.setDataFormat(excel.createDataFormat().getFormat(new String(arr)));
		
		return this.style;
	}
	
	/**
	 * 按下标判断所在列是否属于不需要转换格式的特殊列.
	 * @param index
	 * @return
	 */
	private final boolean transform(int index) {
		if (spColumns != null)
			for (var i : spColumns)
				if (index == i)
					return false;

		return true;
	}

}