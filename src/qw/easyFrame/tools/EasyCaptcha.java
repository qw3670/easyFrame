package qw.easyFrame.tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import javax.imageio.ImageIO;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.algorithm.MTRandomFast;

/**
 * 用于生成验证码图片并发送到前端.
 * 验证码的内容会保存在session中。
 * @author Rex
 * 
 */
public class EasyCaptcha implements Serializable {
	private static final long serialVersionUID = 20231221L;
	
	/**
	 * 验证码字体.
	 */
	private final Font font;
	
	/**
	 * 图片宽度.
	 * 构造器内赋值，不可变
	 */
	private final int width;
	
	/**
	 * 图片高度.
	 * 构造器内赋值，不可变
	 */
	private final int height;
	
	/**
	 * 每个字符之间的间隔.
	 * 默认为17个像素。
	 */
	private int x;
	
	/**
	 * 字符距离图片顶端的高度.
	 * 默认与fontSize相同。
	 */
	private int y;
	
	/**
	 * 文字验证码的字符串长度.
	 * 默认为4。
	 * createFormula方法不使用此变量。
	 */
	private int length;
	
	/**
	 * 干扰线数量.
	 * 默认为5。
	 */
	private int lines;
	
	/**
	 * 算术验证码的数字取值上限（不含、下限为0，含）.
	 * 若不设置此项，则默认为101。
	 * createWords方法不使用此变量。
	 */
	private int range;
	
	/**
	 * height - y + 1的值，
	 * 即：验证码图片中的字符上下抖动的幅度.
	 */
	private int bound;
	
	/**
	 * 字典，用于组成文字验证码中的随机字符串. 
	 * 下列字符经过肉眼挑选，实际显示效果较清晰、不模糊， 
	 * 且各字符字体差异较大，不易被混淆，适合验证码使用。
	 * @author Rex
	 *
	 */
	private static final class Pool {
		private static final String[] DICT = { "0", "1", "4", "7", "8", "5", "2", "3", "6", "9", "Q", "W", "E", "R",
				"T", "Y", "U", "O", "P", "L", "K", "J", "0", "1", "4", "7", "8", "H", "G", "F", "D", "S", "A", "Z", "X",
				"C", "V", "B", "N", "M", "0", "1", "4", "7", "8", "5", "2", "3", "6", "9", "n", "b", "j", "h", "g", "5",
				"2", "3", "6", "9", "f", "d", "a", "e", "r", "t", "y", "i", "0", "1", "4", "7", "8", "5", "2", "3", "6",
				"9" };
	}
	
	/**
	 * 验证码图片会用到的颜色.
	 * 下列颜色经过肉眼挑选，实际显示效果较鲜艳、不模糊，适合验证码使用。
	 * 且下列颜色均为Color类中缓存的静态对象，jvm无需额外new对象，节约了CPU和内存资源。
	 * 生成的验证码字符颜色、干扰线颜色会从中随机选择。
	 */
	private static final Color[] COLORS = { Color.BLUE, Color.MAGENTA, Color.GREEN, Color.CYAN, Color.ORANGE,
			Color.PINK, Color.RED, Color.BLACK, Color.DARK_GRAY, Color.GRAY };
	
	/**
	 * 不提供width变量、height变量和fontSize变量的setter方法，
	 * 三者的值必须在构造对象时确定.<br>
	 * 若不设置x、y、length、lines变量，
	 * 则默认分别取值17、fontSize与height中的较小值、4、5。
	 * @param width 图片宽度
	 * @param height 图片高度
	 * @param fontSize 字体大小，可以使用推荐的字体大小
	 */
	public EasyCaptcha(int width, int height, int fontSize) {
		this.width = width > 0 ? width : 1;
		this.height = height > 0 ? height : 1;
		this.x = 17;
		fontSize = fontSize > 0 ? fontSize : 1;
		defaultY(fontSize);
		this.length = 4;
		this.lines = 5;
		this.range = 101;
		this.font = new Font("Microsoft yahei", Font.BOLD | Font.ITALIC, fontSize);
	}
	
	/**
	 * 不提供width变量、height变量和font变量的setter方法，
	 * 三者的值必须在构造对象时确定.<br>
	 * 若不设置x、y、length、lines变量，
	 * 则默认分别取值17、fontSize与height中的较小值、4、5。
	 * @param width 图片宽度
	 * @param height 图片高度
	 * @param font 字体
	 */
	public EasyCaptcha(int width, int height, Font font) {
		this.width = width > 0 ? width : 1;
		this.height = height > 0 ? height : 1;
		this.x = 17;
		defaultY(font.getSize());
		this.length = 4;
		this.lines = 5;
		this.range = 101;
		this.font = font;
	}
	
	/**
	 * 设置默认的y值.
	 * 默认为fontSize与height中的较小值，
	 * 同时设置bound值，此值为验证码图片中的字符上下抖动的幅度。
	 * @param fontSize
	 * @see EasyCaptcha#y
	 * @see EasyCaptcha#bound
	 */
	private final void defaultY(int fontSize) {
		this.y = fontSize > height ? height : fontSize;
		this.bound = height - y + 1;
	}
	
	public int getX() {
		return x;
	}
	
	/**
	 * 每个字符之间的间隔.
	 * @param x
	 */
	public void setX(int x) {
		this.x = x > 0 ? x : 1;
	}
	
	public int getY() {
		return y;
	}
	
	/**
	 * y为字符左下角（绘图时，以字符左下角为原点开始绘制）与图片上边缘的距离的最小值.<br>
	 * 注意：若此值设置过小，会导致字符超出图片顶端。<br>
	 * 最终生成的字符与图片上边缘的距离会在 [y,height] 范围内随机。
	 * 其中height为图片高度。<br>
	 * 若不设置此项，则默认值等于fontSize与height中的较小值。
	 * @param y
	 * @see EasyCaptcha#getFontSize()
	 */
	public void setY(int y) {
		y = y < 0 ? 0 : y;
		y = y > this.height ? this.height : y;
		this.y = y;
		this.bound = this.height - y + 1;
	}
	
	public int getLength() {
		return length;
	}
	
	/**
	 * 字符串长度.
	 * createFormula方法不使用此变量。
	 * @param length
	 */
	public void setLength(int length) {
		this.length = length > 0 ? length : 1;
	}
	
	public int getLines() {
		return lines;
	}
	
	/**
	 * 干扰线数量.
	 * @param lines
	 */
	public void setLines(int lines) {
		this.lines = lines;
	}

	public int getRange() {
		return range;
	}

	/**
	 * 算术验证码的数字取值上限（不含、下限为0，含 ）.<br>
	 * 若不设置此项，则默认为101。
	 * createWords方法不使用此变量。
	 * 注意：此项最低设为11，若设置了小于11的数，会默认为11。
	 * @param range
	 */
	public void setRange(int range) {
		this.range = range < 11 ? 11 : range;
	}

	/*
	 * 不提供width、height、fontSize和font变量的setter方法，
	 * 上述四者的值必须在构造时确定。
	 */
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public int getFontSize() {
		return font.getSize();
	}
	public Font getFont() {
		return font;
	}

	/**
	 * 生成文字验证码.<br>
	 * 验证码的内容会被保存在session中（区分大小写），key为name参数。
	 * 默认验证码字体为微软雅黑，若未找到微软雅黑，则自动以Dialog字体替代。
	 * 注意：此方法除了生成验证码图片之外，已经包含了将验证码图片发送到前端的代码逻辑，
	 * 直接调用此方法即可在前端接收到验证码图片。<br>
	 * 此方法不需要range参数。<br>
	 * 此方法线程安全。
	 * 	@param req
	 * @param resp
	 * @param name
	 * @return
	 */
	public boolean createWords(HttpServletRequest req, ServletResponse resp, String name) {
		return createWords(req.getSession(), resp, name);
	}
	
	/**
	 * 生成文字验证码.<br>
	 * 验证码的内容会被保存在session中（区分大小写），key为name参数。
	 * 默认验证码字体为微软雅黑，若未找到微软雅黑，则自动以Dialog字体替代。
	 * 注意：此方法除了生成验证码图片之外，已经包含了将验证码图片发送到前端的代码逻辑，
	 * 直接调用此方法即可在前端接收到验证码图片。<br>
	 * 此方法不需要range参数。<br>
	 * 此方法线程安全。
	 * 	@param session
	 * @param resp
	 * @param name
	 * @return
	 */
	public boolean createWords(HttpSession session, ServletResponse resp, String name) {
		try {
			// 创建图片
			var img = initImage();
			var g = initGraphics(img);

			// 适合高并发环境的随机数生成器
			var ran = MTRandomFast.inThreadLocal();
			
			// 书写并缓存文字
			final var colors = COLORS;
			final var cSize = colors.length;
			
			final var dict = Pool.DICT;
			final var dSize = dict.length;
			
			var buffer = new StringBuilder(length);// 缓存
			for (var i = 0; i < length;) {
				// 设置随机字体颜色
				g.setColor(colors[ran.nextInt(cSize)]);
				var temp = dict[ran.nextInt(dSize)];
				i++;
				g.drawString(temp, i * x, y + ran.nextInt(bound));
				buffer.append(temp);
			}

			// 将验证码的缓存加入Session
			session.setAttribute(name, buffer.toString());
			
			// 最终处理：添加干扰线、输出
			return finalProcessing(g, ran,img,resp);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "输出文字验证码失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "生成文字验证码失败", e);
		}
	}
	
	/**
	 * 生成算术验证码.<br>
	 * 算术验证码的计算结果（Integer对象）会被保存在session中，key为name参数。
	 * 默认验证码字体为微软雅黑，若未找到微软雅黑，则自动以Dialog字体替代。
	 * 注意：此方法除了生成验证码图片之外，已经包含了将验证码图片发送到前端的代码逻辑，
	 * 直接调用此方法即可在前端接收到验证码图片。<br>
	 * 此方法不需要length参数。<br>
	 * 此方法线程安全。
	 * 	@param req
	 * @param resp
	 * @param name
	 * @return
	 */
	public boolean createFormula(HttpServletRequest req, ServletResponse resp, String name) {
		return createFormula(req.getSession(), resp, name);
	}
	
	/**
	 * 生成算术验证码.<br>
	 * 算术验证码的计算结果（Integer对象）会被保存在session中，key为name参数。
	 * 默认验证码字体为微软雅黑，若未找到微软雅黑，则自动以Dialog字体替代。
	 * 注意：此方法除了生成验证码图片之外，已经包含了将验证码图片发送到前端的代码逻辑，
	 * 直接调用此方法即可在前端接收到验证码图片。<br>
	 * 此方法不需要length参数。<br>
	 * 此方法线程安全。
	 * 	@param session
	 * @param resp
	 * @param name
	 * @return
	 */
	public boolean createFormula(HttpSession session, ServletResponse resp, String name) {
		try {
			// 创建图片
			var img = initImage();
			var g = initGraphics(img);

			// 适合高并发环境的随机数生成器
			var ran = MTRandomFast.inThreadLocal();
			
			// true为加号，false为减号
			var symbol = ran.nextBoolean();
			// 生成两个不大于100的随机数
			var a = ran.nextInt(range);
			var b = ran.nextInt(range);
			// 计算结果
			var reslut = symbol ? a + b : a - b;
			// 将结果加入Session
			session.setAttribute(name, reslut);
			
			// 生成待输出的文字
			var buffer = new StringBuilder(Integer.toString(a));
			buffer.append(symbol ? "+" : "-");
			buffer.append(Integer.toString(b)).append("=");
			
			// 开始绘制图形
			var split = buffer.toString().split("", 0);
			var len = split.length;
			final var colors = COLORS;
			for (var i = 0; i < len;) {
				// 设置随机字体颜色
				g.setColor(colors[ran.nextInt(10)]);
				String str = split[i];
				i++;
				g.drawString(str, i * x, y + ran.nextInt(bound));
			}
			
			// 最终处理：添加干扰线、输出
			return  finalProcessing(g, ran,img,resp);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "输出算术验证码失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "生成算术验证码失败", e);
		}
	}

	/**
	 * 创建图片.
	 * @return
	 */
	private final BufferedImage initImage() {
		return new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	}

	/**
	 * 创建画笔.
	 * @param img
	 * @return
	 */
	private final Graphics initGraphics(BufferedImage img) {
		var g = img.getGraphics();
		// 设置背景色
		g.setColor(Color.WHITE);
		// 设置画布尺寸
		g.fillRect(0, 0, width, height);
		// 设置字体
		g.setFont(font);
		
		return g;
	}

	/**
	 * 添加干扰线并输出图片.
	 * @param g
	 * @param ran
	 * @param img
	 * @param resp
	 * @throws IOException 
	 */
	private final boolean finalProcessing(Graphics g, MTRandomFast ran, BufferedImage img, ServletResponse resp)
			throws IOException {
		final var colors = COLORS;
		for (var i = 0; i < lines; i++) {
			// 设置随机干扰线颜色
			g.setColor(colors[ran.nextInt(10)]);
			g.drawLine(ran.nextInt(width), ran.nextInt(height), ran.nextInt(width), ran.nextInt(height));
		}
		
		return ImageIO.write(img, "jpg", resp.getOutputStream());
	}
	
}