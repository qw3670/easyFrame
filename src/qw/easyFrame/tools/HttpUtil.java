package qw.easyFrame.tools;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.Map;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.ServletUtil;

/**
 * 封装常用的Http方法的工具类.
 * @author Rex
 *
 */
public class HttpUtil {

    /**
     * 超时时间.
     */
    private final Duration timeout;
    
    private HttpClient httpClient;
    
    /**
     * Post请求的字符集.
     */
    private final Charset charset;
    
    /**
     * response返回的状态码.<br>
     * 若未进行http会话或HttpUtil对象被关闭，则此值为-1。
     */
    private int statusCode;
    
	/**
	 * 超时时间5秒，不关闭主机验证，使用默认字符集作为Post请求字符集.
	 */
	public HttpUtil() {
		this(5, false, null);
	}
	
	/**
	 * 不关闭主机验证，使用默认字符集作为Post请求字符集.<br>
	 * 注意：参数1的单位是秒！不是毫秒！
	 * @param timeoutSeconds
	 */
	public HttpUtil(long timeoutSeconds) {
		this(timeoutSeconds, false, null);
	}
	
	/**
	 * 设置请求超时时间，设置是否关闭主机验证，指定Post请求字符集.<br>
	 * 注意：参数1的单位是秒！不是毫秒！<br>
	 * 注意：参数3指定的是Post请求的字符集，不是Get请求的字符集、不是响应的字符集！
	 * 即：参数3只在执行Post的请求时生效。<br>
	 * 注意：Get请求的字符集请调用者通过自行对字符串进行编码的方式指定。<br>
	 * 注意：参数2是全局生效的。即：在一处关闭主机验证，则全局关闭。且主机验证关闭后无法再开启，
	 * 除非重启整个项目。请谨慎开启。
	 * @param timeoutSeconds
	 * @param disableHostnameVerification 是否关闭主机验证，即：是否允许直接使用IP地址访问网站
	 * @param charset 指定请求的字符集
	 */
	public HttpUtil(long timeoutSeconds, boolean disableHostnameVerification, String charset) {
		this.timeout  = Duration.ofSeconds(timeoutSeconds);
		this.httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_2)
				.connectTimeout(timeout)
				.followRedirects(HttpClient.Redirect.NEVER)
				.sslContext(sslContext(disableHostnameVerification))
				.proxy(ProxySelector.getDefault())
				.build();
		this.charset = charset == null ? Charset.defaultCharset() : Charset.forName(charset);
	}
	
    /**
     * 生成安全套接字，用于https的证书跳过.
     * @param disableHostnameVerification
     * @return
     */
	private final static SSLContext sslContext(boolean disableHostnameVerification) {
		var managers = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x, String s) {}
			@Override
			public void checkServerTrusted(X509Certificate[] x, String s) {}
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		}};

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
			// 取消主机名验证
			if (disableHostnameVerification)
				System.setProperty("jdk.internal.httpclient.disableHostnameVerification", "true");
			sc.init(null, managers, new SecureRandom());
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			throw new DefinedRuntimeException(false, "创建SSLContext异常", e);
		}
		return sc;
	}

	/**
	 * 获取当前会话返回的状态码.
	 * @return
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
     * get请求.
     * @param url 地址
     * @param map 参数
     * @return
     */
	public String get(String url, Map<String, String> map) {
		var kv = createParameters(map);
		return get(url.concat("?").concat(kv));
	}

    /**
     * get请求.
     * @param url 地址
     * @return
     */
	public String get(String url) {
		try {
			var request = createGet(url);
			var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
			if ((this.statusCode = response.statusCode()) == 200)
				return response.body();
		} catch (IOException | InterruptedException e) {
			throw new DefinedRuntimeException(false, "执行Get请求异常", e);
		}
		return "";
	}
	
	/**
	 * 使用get请求用于下载文件.
	 * @param url
	 * @param path
	 * @return
	 */
	public Path get(String url, Path path) {
		try {
			var request = createGet(url);
			var response = httpClient.send(request, HttpResponse.BodyHandlers.ofFile(path));
			if ((this.statusCode = response.statusCode()) == 200)
				return response.body();
		} catch (IOException | InterruptedException e) {
			throw new DefinedRuntimeException(false, "执行Get请求异常", e);
		}
		return null;
	}
	
	/**
	 * 将HttpResponse写入ServletResponse.
	 * @param url
	 * @param servletResponse
	 * @return
	 */
	public long get(String url, Object servletResponse) {
		try {
			var request = createGet(url);
			var response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
			if ((this.statusCode = response.statusCode()) == 200)
				return ServletUtil.writeToResp(response.body(), servletResponse);
		} catch (IOException | InterruptedException e) {
			throw new DefinedRuntimeException(false, "执行Get请求异常", e);
		}
		return -1;
	}
	
	/**
	 * 创建一个用于执行Get请求的HttpRequest对象.
	 * @param url
	 * @return
	 */
	private final HttpRequest createGet(String url) {
		final var timeout = this.timeout;
		return HttpRequest.newBuilder()
				.header("Content-Type", " text/html")
				.version(HttpClient.Version.HTTP_2)
				.uri(URI.create(url)).GET()
				.timeout(timeout)
				.build();
	}

    /**
     * post请求发送Json数据.
     * @param url 地址
     * @param json json字符串
     * @return
     */
	public String post(String url, String json) {
		final var charset = this.charset;
		var request = createPost(url, HttpRequest.BodyPublishers.ofString(json, charset), "application/json");
		return post(request);
	}
	
    /**
     * post请求发送表单数据.
     * @param url 地址
     * @param map 
     * @return
     */
	public String post(String url, Map<String, String> map) {
		final var charset = this.charset;
		var kv = createParameters(map);
		var request = createPost(url, HttpRequest.BodyPublishers.ofString(kv, charset), "application/x-www-form-urlencoded");
		return post(request);
	}
	
    /**
     * 发送post请求.
     * @param request
     * @return
     */
	private final String post(HttpRequest request) {
		try {
			var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
			if ((this.statusCode = response.statusCode()) == 200)
				return response.body();
		} catch (IOException | InterruptedException e) {
			throw new DefinedRuntimeException(false, "执行Post请求异常", e);
		}
		return "";
	}
	
	/**
	 * 创建一个用于执行Post请求的HttpRequest对象.
	 * @param url
	 * @param publisher
	 * @param type
	 * @return
	 */
	private final HttpRequest createPost(String url, BodyPublisher publisher, String type) {
		final var timeout = this.timeout;
		return HttpRequest.newBuilder()
				.header("Content-Type", type)
				.version(HttpClient.Version.HTTP_2)
				.uri(URI.create(url))
				.POST(publisher)
				.timeout(timeout)
				.build();
	}
	
	/**
	 * 拼接url参数字符串.
	 * @param map
	 * @return
	 */
	private static final String createParameters(Map<String, String> map) {
		if (map.size() > 0) {
			var kv = new StringBuilder();
			for (var e : map.entrySet())
				kv.append(e.getKey()).append("=").append(e.getValue()).append("&");
			kv.deleteCharAt(kv.length() - 1);
			
			return kv.toString();
		}
		return "";
	}

}