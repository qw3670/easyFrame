package qw.easyFrame.tools.statics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 封装与IO有关的静态方法.
 * @author Rex
 *
 */
public class IOUtil {
	
	/**
	 * 将byte数组写到本地文件.
	 * @param bytes
	 * @param file
	 * @param append 是否在原文件末尾继续写入
	 */
	public final static void write(byte[] bytes, File file, boolean append) {
		var parentFile = file.getParentFile();
		if (!parentFile.exists())
			parentFile.mkdirs();
		
		try (var fos = new FileOutputStream(file, append)) {
			fos.write(bytes);
			fos.flush();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "写文件失败", e);
		}
	}
	
	/**
	 * 将输入流写到输出流.
	 * @param in
	 * @param out
	 * @param bufferSize byte数组缓存大小
	 * @param close 完成操作后是否关闭两个流
	 */
	public final static void write(InputStream in, OutputStream out, int bufferSize, boolean close) {
		try {
			var buffer = new byte[bufferSize];
			var len = 0;
			while ((len = in.read(buffer, 0, bufferSize)) != -1)
				out.write(buffer, 0, len);

			out.flush();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "将输入流写到输出流失败", e);
		} finally {
			if (close)
				CloseSource.close(in, out);
		}
	}
	
	/**
	 * 将文件读取为byte数组.
	 * @param file
	 * @return
	 */
	public final static byte[] read(File file) {
		if (file.isFile()) {
			var length = file.length();
			if (length > Integer.MAX_VALUE)
				throw new DefinedRuntimeException(false, "文件体积超出2GiB上限");

			try (var in = new FileInputStream(file)) {
				var len = (int) length;
				var data = new byte[len];
				in.read(data, 0, len);
				return data;
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "读取文件失败", e);
			}
		}
		return null;
	}
	
	/**
	 * 	移动文件，若移动失败，会抛出异常.
	 * @see IOUtil#moveFile(File, File, File)
	 * @param file 源文件（可以是文件夹）
	 * @param destFile 目标文件
	 * @return 返回新文件的路径
	 */
	public static Path moveFile(File file, File destFile) {
		return moveFile(file, destFile, destFile.getParentFile());
	}
	
	/**
	 * 移动文件，若移动失败，会抛出异常.
	 * @param file 源文件（可以是文件夹）
	 * @param destFile 目标文件
	 * @param destParentFile 目标文件的母目录
	 * @return
	 */
	public final static Path moveFile(File file, File destFile, File destParentFile) {
		try {
			if (!destParentFile.exists())
				destParentFile.mkdirs();

			return Files.move(file.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "移动文件失败", e);
		}
	}
	
	/**
	 * 复制文件.
	 * @param file 源文件（不可以是文件夹）
	 * @param destFile 目标文件
	 * @return 返回目标文件
	 */
	public static File copyFile(File file, File destFile) {
		return copyFile(file, destFile, destFile.getParentFile());
	}
	
	/**
	 * 复制文件.
	 * @param file 源文件（不可以是文件夹）
	 * @param destFile 目标文件
	 * @param destParentFile 目标文件的母目录
	 * @return 返回目标文件
	 */
	public final static File copyFile(File file, File destFile, File destParentFile) {
		if (!destParentFile.exists())
			destParentFile.mkdirs();

		try (var fis = new FileInputStream(file);
				var input = fis.getChannel();
				var fos = new FileOutputStream(destFile, false);
				var output = fos.getChannel()) {

			output.transferFrom(input, 0, input.size());

			return destFile;
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "复制文件失败", e);
		}
	}
	
	/**
	 * 使用指定字符集，从文件中读取文本.<br>
	 * 注意：返回的字符串末尾会带有一个换行符。
	 * @param path
	 * @param charset 指定字符集，若为null，则使用系统默认字符集
	 * @return
	 * @throws IOException 
	 */
	public static String getText(Path path, String charset) {
		try {
			return Files.readString(path, Charset.forName(charset));
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "读取文件失败", e);
		}
	}
	
	/**
	 * 使用指定字符集，从InputStream中读取文本.<br>
	 * 注意：返回的字符串末尾会带有一个换行符。
	 * @param is
	 * @param charset 指定字符集，若为null，则使用系统默认字符集
	 * @param close 方法执行完成后，是否关闭输入流
	 * @return
	 */
	public final static String getText(InputStream is, String charset, boolean close) {
		var sb = new StringBuilder();

		InputStreamReader in = null;
		BufferedReader reader = null;
		try {
			in = charset == null ? new InputStreamReader(is) : new InputStreamReader(is, charset);
			reader = new BufferedReader(in);
			String line = null;
			while ((line = reader.readLine()) != null)
				sb.append(line).append("\r\n");
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "不支持的字符集", e);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "输入流读取异常", e);
		} finally {
			if (close)
				CloseSource.close(reader, in, is);
		}
		return sb.toString();
	}
	
	/**
	 *  删除文件或递归删除文件夹.<br>
	 *  当不确定File对象是文件还是文件夹时，使用此方法。
	 * @param file
	 */
	public final static boolean delete(File file) {
		if (file != null && file.exists())
			if (file.isDirectory())
				return deleteDirWithoutTest(file);
			else
				return file.delete();
		return false;
	}
	
	/**
	 * 递归删除文件夹.<br>
	 * 当确定File对象是文件夹时，使用此方法。
	 * @param dir
	 */
	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()/* 此处调用exists()亦可，调用isDirectory()更严谨 */)
			return deleteDirWithoutTest(dir);
		return false;
	}
	
	/**
	 * 不验证File是否存在（不判空且不调用File.exists()），直接递归删除文件夹.<br>
	 * 仅当完全确定待删除的File对象存在且是文件夹时才调用此方法。
	 * @param dir
	 */
	public final static boolean deleteDirWithoutTest(File dir) {
		for (var file : dir.listFiles())
			if (file.isDirectory())
				deleteDirWithoutTest(file);
			else
				file.delete();

		return dir.delete();
	}

}