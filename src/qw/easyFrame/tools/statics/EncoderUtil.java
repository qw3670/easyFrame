package qw.easyFrame.tools.statics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 各种编码/解码器.
 * @author Rex
 *
 */
public class EncoderUtil {
	
	private static final class EncoderPool {
		// Base64编码器
		private static final Encoder EN = Base64.getEncoder();
	}
	
	private static final class DecoderPool {
		// Base64解码器
		private static final Decoder DE = Base64.getDecoder();
	}

	private static final class MD5DigestPoolForBytes {
		// MD5摘要生成器
		private static MessageDigest digest;

		static {
			digest = getMd5Digest();
		}

	}
	
	private static final class MD5DigestPoolForFile {
		// MD5摘要生成器
		private static MessageDigest digest;

		static {
			digest = getMd5Digest();
		}

	}
	
	private static final class SHA256DigestPool {
		// SHA-256摘要生成器
		private static MessageDigest digest;
		
		static {
			try {
				digest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				throw new DefinedRuntimeException(false, "初始化SHA-256算法失败", e);
			}
		}
		
	}
	
	/**
	 * 用于生产MD5算法的摘要生成器.
	 * @return
	 */
	private static final MessageDigest getMd5Digest() {
		try {
			return MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new DefinedRuntimeException(false, "初始化MD5算法失败", e);
		}
	}
	
	/**
	 * 计算byte数组的MD5.<br>
	 * 注意：此方法非线程安全。
	 * @param bytes
	 * @return String MD5
	 */
	public static String md5(byte[] bytes) {
		final var digest = MD5DigestPoolForBytes.digest;

		digest.reset();
		// 计算md5函数
		digest.update(bytes);
		var arr = digest.digest();

		return bytesToHex(arr, 32);
	}
	
    /** 
     * 计算单个文件的MD5值.<br>
     * 若参数是文件夹或文件不存在，则会返回""（空字符串）。<br>
     * 注意：此方法非线程安全。
     * @param file 不可以是文件夹
     * @return
     */  
	public static String md5(File file) {
		if (file.isFile())
			try (var fis = new FileInputStream(file)) {
				final var digest = MD5DigestPoolForFile.digest;

				digest.reset();
				// 开始计算文件的MD5值
				var size = 1048576;// 1MiB
				var buffer = new byte[size];
				int len;
				while ((len = fis.read(buffer, 0, size)) != -1)
					digest.update(buffer, 0, len);
				var arr = digest.digest();

				return bytesToHex(arr, 32);
			} catch (IOException e) {
				throw new DefinedRuntimeException(false, "文件读取异常", e);
			} catch (Exception e) {
				throw new DefinedRuntimeException(false, "计算MD5时出现未知异常", e);
			}

		return "";
	}
	
	/**
	 * 封装SHA-256算法.<br>
	 * 此方法非线程安全。
	 * @param bytes
	 * @return
	 */
	public static String sha256(byte[] bytes) {
		final var digest = SHA256DigestPool.digest;

		digest.reset();
		digest.update(bytes);
		var arr = digest.digest();

		return bytesToHex(arr, 64);
	}
	
	/**
	 * 将byte数组编码为Base64的byte数组.
	 * @param bytes
	 * @return
	 */
	public static final byte[] encode(byte[] bytes) {
		final var en  = EncoderPool.EN;
		return en.encode(bytes);
	}
	
	/**
	 * 解码Base64的byte数组.
	 * @param bytes
	 * @return
	 */
	public static final byte[] decode(byte[] bytes) {
		final var de = DecoderPool.DE;
		return de.decode(bytes);
	}

    /**
     * byte数组转换为16进制值.<br>
     * capacity参数是结果字符串的长度，在方法内用于构建StringBuilder对象。
     * 若调用者已知结果字符串的最终长度，指定该值可提升性能、减少CPU占用；
     * 反之，则可设置一个大致的长度。
     * @param hash
     * @param capacity 结果字符串的长度
     * @return
     */
    public static final String bytesToHex(byte[] hash, int capacity) {
        var hexString = new StringBuilder(capacity);
        for (var b : hash) {
            var hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1)
                hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
	
}