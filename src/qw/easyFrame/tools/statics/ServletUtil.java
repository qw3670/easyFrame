package qw.easyFrame.tools.statics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletResponse;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 封装与servlet有关的静态方法.
 * API基于Servlet5.0。
 * @author Rex
 *
 */
public class ServletUtil {
	
	/**
	 * 提供文件下载服务.<br>
	 * 注意：此方法执行完毕后不会删除文件，若有需求请手动删除。
	 * @param f
	 * @param resp
	 * @param context
	 */
	public static void download(File f, HttpServletResponse resp, ServletContext context) {
		try (var input = new FileInputStream(f)) {
			// 告知浏览器要下载文件
			var filename = f.getName();
			var bytes = filename.getBytes();
			resp.setHeader("content-disposition",
					"attachment;filename=".concat(new String(bytes, 0, bytes.length, "iso-8859-1")));
			resp.setContentType(context.getMimeType(filename));// 根据文件名自动获得文件类型

			// 输出文件
			IOUtil.write(input, resp.getOutputStream(), 1048576/* 1MiB */, false);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "提供文件下载服务失败", e);
		}
	}
	
	/**
	 * 允许Get、POST跨域请求.
	 * @param resp
	 */
	public static void accessControlAllow(HttpServletResponse resp) {
		resp.addHeader("Access-Control-Allow-Origin", "*");
		resp.addHeader("Access-Control-Allow-Methods", "GET, POST");
	}
	
	/**
	 * 禁用浏览器缓存.
	 * @param resp
	 */
	public static void noCache(HttpServletResponse resp) {
		resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
		resp.setHeader("Pragma", "no-cache"); // HTTP 1.0
		resp.setHeader("Expires", "0");// Proxies
	}
	
	/**
	 * 使浏览器传入的中文字符和服务端输出的中文字符不乱码.
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public static void noGarbled(ServletRequest req, ServletResponse resp) throws IOException {
		// 使浏览器传入的中文字符串不乱码
		req.setCharacterEncoding("UTF-8");
		// 使服务端输出的中文字符串不乱码
		resp.setContentType("text/html;charset=UTF-8");
	}
	
	/**
	 * 使用指定字符集，得到ServletRequest中的body.
	 * @param req
	 * @param charset 指定的字符集，若为null，则使用系统默认字符集
	 * @return
	 */
	public static String getRequestBody(ServletRequest req, String charset) {
		try {
			return IOUtil.getText(req.getInputStream(), charset, false);
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "从HttpServletRequest中获取输入流异常", e);
		}
	}
	
	/**
	 * 根据key，从ServletRequest中获取strip()后的Parameter.
	 * 若不存在，则返回空字符串。
	 * @param req
	 * @param key
	 * @return
	 */
	public static String getParameter(ServletRequest req, String key) {
		return StringUtil.strip(req.getParameter(key));
	}
	
	/**
	 * 将输入流写入ServletResponse.
	 * @param in
	 * @param servletResponse
	 * @return
	 */
	public static final long writeToResp(InputStream in, Object servletResponse) {
		try {
			return in.transferTo(((ServletResponse) servletResponse).getOutputStream());
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "将输入流写入ServletResponse异常", e);
		}
	}
	
}