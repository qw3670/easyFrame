package qw.easyFrame.tools.statics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import qw.easyFrame.tools.algorithm.MTRandomFast;

/**
 * 封装与排序有关的各类算法.
 * @author Rex
 *
 */
public class SortUtil {
	
	/**
	 * 找出极值.
	 * @param <T>
	 * @param list 使用ArrayList性能较好
	 * @param max 若为true，则返回极大值；反之，则返回极小值
	 * @return
	 */
	public static <T extends Comparable<T>> T extremum(List<T> list, boolean max) {
		var t = list.get(0);
		var size = list.size();
		for (var i = 1; i < size; i++) {
			var temp = list.get(i);
			if ((t.compareTo(temp) < 0) == max)
				t = temp;
		}
		return t;
	}
	
	/**
	 * 找出极值.
	 * @param <T>
	 * @param arr
	 * @param max 若为true，则返回极大值；反之，则返回极小值
	 * @return
	 */
	public static <T extends Comparable<T>> T extremum(T[] arr, boolean max) {
		var t = arr[0];
		var size = arr.length;
		for (var i = 1; i < size; i++) {
			var temp = arr[i];
			if ((t.compareTo(temp) < 0) == max)
				t = temp;
		}
		return t;
	}
	
	/**
	 * 找出极值.
	 * @param arr
	 * @param max 若为true，则返回极大值；反之，则返回极小值
	 * @return
	 */
	public static int extremum(int[] arr, boolean max) {
		var t = arr[0];
		var size = arr.length;
		for (var i = 1; i < size; i++) {
			var temp = arr[i];
			if ((t < temp) == max)
				t = temp;
		}
		return t;
	}
	
	/**
	 * 希尔排序.<br>
	 * 推荐在需要降序、且不要求稳定排序的业务逻辑中调用此方法。<br>
	 * 注意：希尔排序算法是不稳定的排序算法，JDK自带的List.sort()是稳定的排序算法。<br>
	 * 注意：实测升序时，希尔排序性能弱于List.sort(null)。但降序时，其性能强于List.sort(Comparator)。<br>
	 * 注意：List.sort()方法的底层调用了Arrays.sort()。<br>
	 * 注意：Arrays.sort()仅可对数组进行排序。对基本类型数组采用快速排序（不稳定），
	 * 对对象数组采用归并排序（稳定，被List.sort()调用）。<br>
	 * 注意：JDK还提供了Collections.sort(List)方法，但其底层直接调用List.sort()，无任何其它代码。
	 * @see List#sort(java.util.Comparator)
	 * @see Arrays#sort(Object[])
	 * @see Arrays#sort(Object[], int, int)
	 * @param <T>
	 * @param list 使用ArrayList性能较好
	 * @param desc 若为true，则为降序排序；反之，则为升序排序
	 */
	public static <T extends Comparable<T>> void shellSort(List<T> list, boolean desc) {
		var size = list.size();
		for (var gap = size / 2; gap > 0; gap /= 2)// 步长逐渐减小
			for (var i = gap; i < size; i++) {// 在同一步长内，排序方式是插入排序
				var temp0 = list.get(i);// 待排元素
				T temp1;// 待排元素
				int j;
				// j-gap代表有序数组中最大数的下标，j表示有序数组的前一个元素，减gap是减去偏移量就是步长
				for (j = i; j >= gap && (temp0.compareTo(temp1 = list.get(j - gap)) > 0) == desc; j -= gap)
					list.set(j, temp1);// 原有序数组最大的后移一位
				list.set(j, temp0);// 找到了合适的位置插入
			}
	}
	
	/**
	 * 希尔排序.<br>
	 * 推荐在需要降序、且不要求稳定排序的业务逻辑中调用此方法。<br>
	 * 注意：实测升序时，希尔排序性能弱于Arrays.sort()。<br>
	 * 注意：Arrays.sort()对对象数组采用归并排序（稳定，被List.sort()调用）。
	 * @see Arrays#sort(Object[])
	 * @see Arrays#sort(Object[], int, int)
	 * @param <T>
	 * @param arr 
	 * @param desc 若为true，则为降序排序；反之，则为升序排序
	 */
	public static <T extends Comparable<T>> void shellSort(T[] arr, boolean desc) {
		var size = arr.length;
		for (var gap = size / 2; gap > 0; gap /= 2)// 步长逐渐减小
			for (var i = gap; i < size; i++) {// 在同一步长内，排序方式是插入排序
				var temp0 = arr[i];// 待排元素
				T temp1;// 待排元素
				int j;
				// j-gap代表有序数组中最大数的下标，j表示有序数组的前一个元素，减gap是减去偏移量就是步长
				for (j = i; j >= gap && (temp0.compareTo(temp1 = arr[j - gap]) > 0) == desc; j -= gap)
					arr[j] = temp1;// 原有序数组最大的后移一位
				arr[j] = temp0;// 找到了合适的位置插入
			}
	}
	
	/**
	 * 希尔排序.<br>
	 * 推荐在需要降序、且不要求稳定排序的业务逻辑中调用此方法。<br>
	 * 注意：实测升序时，希尔排序性能弱于Arrays.sort()。<br>
	 * 注意：Arrays.sort()对基本类型数组采用快速排序（不稳定），
	 * @see Arrays#sort(int[])
	 * @see Arrays#sort(int[], int, int)
	 * @param arr 
	 * @param desc 若为true，则为降序排序；反之，则为升序排序
	 */
	public static void shellSort(int[] arr, boolean desc) {
		var size = arr.length;
		for (var gap = size / 2; gap > 0; gap /= 2)// 步长逐渐减小
			for (var i = gap; i < size; i++) {// 在同一步长内，排序方式是插入排序
				var temp0 = arr[i];// 待排元素
				int temp1;// 待排元素
				int j;
				// j-gap代表有序数组中最大数的下标，j表示有序数组的前一个元素，减gap是减去偏移量就是步长
				for (j = i; j >= gap && (temp0 > (temp1 = arr[j - gap])) == desc; j -= gap)
					arr[j] = temp1;// 原有序数组最大的后移一位
				arr[j] = temp0;// 找到了合适的位置插入
			}
	}
	
	/**
	 * 希尔排序.<br>
	 * 推荐在需要降序、且不要求稳定排序的业务逻辑中调用此方法。<br>
	 * 注意：实测升序时，希尔排序性能弱于Arrays.sort()。<br>
	 * 注意：Arrays.sort()对基本类型数组采用快速排序（不稳定），
	 * @see Arrays#sort(byte[])
	 * @see Arrays#sort(byte[], int, int)
	 * @param arr 
	 * @param desc 若为true，则为降序排序；反之，则为升序排序
	 */
	public static void shellSort(byte[] arr, boolean desc) {
		var size = arr.length;
		for (var gap = size / 2; gap > 0; gap /= 2)// 步长逐渐减小
			for (var i = gap; i < size; i++) {// 在同一步长内，排序方式是插入排序
				var temp0 = arr[i];// 待排元素
				byte temp1;// 待排元素
				int j;
				// j-gap代表有序数组中最大数的下标，j表示有序数组的前一个元素，减gap是减去偏移量就是步长
				for (j = i; j >= gap && (temp0 > (temp1 = arr[j - gap])) == desc; j -= gap)
					arr[j] = temp1;// 原有序数组最大的后移一位
				arr[j] = temp0;// 找到了合适的位置插入
			}
	}
	
	/**
	 * 反转对象数组.
	 * @param arr
	 * @return
	 */
	public static void reverse(Object[] arr) {
		var len = arr.length;// 数组长度
		var times = len / 2;// 循环次数
		var lastIndex = len - 1;
		for (var i = 0; i < times; i++) {
			var targetIndex = lastIndex - i;
			var temp = arr[i];
			arr[i] = arr[targetIndex];
			arr[targetIndex] = temp;
		}
	}
	
	/**
	 * 反转数组.
	 * @param arr
	 * @return
	 */
	public static void reverse(int[] arr) {
		var len = arr.length;// 数组长度
		var times = len / 2;// 循环次数
		var lastIndex = len - 1;
		for (var i = 0; i < times; i++) {
			var targetIndex = lastIndex - i;
			var temp = arr[i];
			arr[i] = arr[targetIndex];
			arr[targetIndex] = temp;
		}
	}
	
	/**
	 * 反转数组.
	 * @param arr
	 * @return
	 */
	public static void reverse(byte[] arr) {
		var len = arr.length;// 数组长度
		var times = len / 2;// 循环次数
		var lastIndex = len - 1;
		for (var i = 0; i < times; i++) {
			var targetIndex = lastIndex - i;
			var temp = arr[i];
			arr[i] = arr[targetIndex];
			arr[targetIndex] = temp;
		}
	}
	
	/**
	 * 反转数组.
	 * @param arr
	 * @return
	 */
	public static void reverse(char[] arr) {
		var len = arr.length;// 数组长度
		var times = len / 2;// 循环次数
		var lastIndex = len - 1;
		for (var i = 0; i < times; i++) {
			var targetIndex = lastIndex - i;
			var temp = arr[i];
			arr[i] = arr[targetIndex];
			arr[targetIndex] = temp;
		}
	}
	
	/**
	 * 打乱对象数组.
	 * 打乱List请使用JDK自带的Collections#shuffle()方法。
	 * @see Collections#shuffle(List)
	 * @see Collections#shuffle(List, java.util.Random)
	 * @param arr
	 */
	public static void shuffle(Object[] arr) {
		var ran = MTRandomFast.inThreadLocal();
		var len = arr.length;
		for (var i = 0; i < len; i++) {
			var newIndex = ran.nextInt(len);
			var temp = arr[i];
			arr[i] = arr[newIndex];
			arr[newIndex] = temp;
		}
	}
	
	/**
	 * 打乱数组.
	 * 打乱List请使用JDK自带的Collections#shuffle()方法。
	 * @see Collections#shuffle(List)
	 * @see Collections#shuffle(List, java.util.Random)
	 * @param arr
	 */
	public static void shuffle(int[] arr) {
		var ran = MTRandomFast.inThreadLocal();
		var len = arr.length;
		for (var i = 0; i < len; i++) {
			var newIndex = ran.nextInt(len);
			var temp = arr[i];
			arr[i] = arr[newIndex];
			arr[newIndex] = temp;
		}
	}
	
	/**
	 * 打乱数组.
	 * 打乱List请使用JDK自带的Collections#shuffle()方法。
	 * @see Collections#shuffle(List)
	 * @see Collections#shuffle(List, java.util.Random)
	 * @param arr
	 */
	public static void shuffle(byte[] arr) {
		var ran = MTRandomFast.inThreadLocal();
		var len = arr.length;
		for (var i = 0; i < len; i++) {
			var newIndex = ran.nextInt(len);
			var temp = arr[i];
			arr[i] = arr[newIndex];
			arr[newIndex] = temp;
		}
	}
	
	/**
	 * 打乱数组.
	 * 打乱List请使用JDK自带的Collections#shuffle()方法。
	 * @see Collections#shuffle(List)
	 * @see Collections#shuffle(List, java.util.Random)
	 * @param arr
	 */
	public static void shuffle(char[] arr) {
		var ran = MTRandomFast.inThreadLocal();
		var len = arr.length;
		for (var i = 0; i < len; i++) {
			var newIndex = ran.nextInt(len);
			var temp = arr[i];
			arr[i] = arr[newIndex];
			arr[newIndex] = temp;
		}
	}
	
}