package qw.easyFrame.tools.statics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * 封装与数字有关的工具类.
 * @author Rex
 *
 */
public class NumberUtil {
	
	/**
	 * 以四舍五入法精确到小数点后n位.
	 * @param decimal 待处理的数
	 * @param scale 精度
	 * @return
	 */
	public static final BigDecimal setScale(BigDecimal decimal, int scale) {
		return decimal.setScale(scale, RoundingMode.HALF_UP);
	}
	
	/**
	 * 转换数值为BigDecimal.
	 * @param decimal
	 * @return
	 */
	public static final BigDecimal bigDecimal(double decimal) {
		return bigDecimal(Double.toString(decimal));
	}
	
	/**
	 * 转换数值为BigDecimal.
	 * 注意：方法不会调用decimal参数的strip()方法。
	 * @param decimal
	 * @return
	 */
	public final static BigDecimal bigDecimal(String decimal) {
		return switch (decimal) {
		case "0", "0.0", "0.00" -> BigDecimal.ZERO;
		case "1", "1.0", "1.00" -> BigDecimal.ONE;
		case "10", "10.0", "10.00" -> BigDecimal.TEN;
		default -> new BigDecimal(decimal.toCharArray(), 0, decimal.length(), MathContext.UNLIMITED);
		};
	}
	
	/**
	 * 转换数值为BigDecimal.
	 * @param decimal
	 * @return
	 */
	public static final BigDecimal bigDecimal(int decimal) {
		return switch (decimal) {
		case 0 -> BigDecimal.ZERO;
		case 1 -> BigDecimal.ONE;
		case 10 -> BigDecimal.TEN;
		default -> new BigDecimal(decimal);
		};
	}
	
	/**
	 * 按四舍五入法计算商，精确到指定精度.
	 * @param dividend 被除数
	 * @param divisor 除数
	 * @param scale 精度
	 * @return
	 */
	public static final BigDecimal divide(BigDecimal dividend, BigDecimal divisor, int scale) {
		return dividend.divide(divisor, scale, RoundingMode.HALF_UP);
	}
	
	/**
	 * 将字符串解析为整型，若解析抛出异常，则返回默认值.<br>
	 * 注意：此方法不会对传入的字符串进行strip操作，如需strip请在传参前自行调用strip()。
	 * @param str 待解析的字符串
	 * @param defaultInt 解析失败时返回的默认值
	 * @return
	 */
	public final static int parseInt(String str, int defaultInt) {
		try {
			return Integer.parseInt(str, 10);
		} catch (Exception e) {
			return defaultInt;
		}
	}

	/**
	 * 判断一个整数是否为质数.
	 * @param n
	 * @return
	 */
	public static boolean isPrime(int n) {
		if (n < 2)
			return false;
		else
			// 枚举到√n，注意溢出
			for (var i = 2; i <= n / i; i++)
				if (n % i == 0) // 如果i可以整除n,说明n不是素数
					return false;

		return true;
	}

}