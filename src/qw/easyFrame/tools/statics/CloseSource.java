package qw.easyFrame.tools.statics;

import java.util.Collection;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.CloseSourceX;

/**
 * 用于关闭AutoCloseable资源的工具类.
 * 更多功能请使用CloseSourceX
 * @see CloseSourceX
 * @author Rex
 *
 */
public class CloseSource {
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param acs
	 */
	public final static void close(AutoCloseable... acs) {
		int len;
		if (acs != null && (len = acs.length) != 0)
			doClose(acs, 0, len);
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param list
	 */
	public final static void close(Collection<AutoCloseable> list) {
		int size;
		if (list != null && (size = list.size()) != 0)
			doClose(list.toArray(new AutoCloseable[size]), 0, size);
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param arr
	 * @param start
	 * @param end
	 */
	public final static void close(AutoCloseable[] arr, int start, int end) {
		int len;
		if (arr != null && start > -1 && start < (len = arr.length)) {
			if (end > len)
				end = len;
			doClose(arr, start, end);
		}
	}
	
	/**
	 * 执行具体的关闭操作.
	 * 	@param start
	 * @param end
	 * @param arr
	 */
	private final static void doClose(AutoCloseable[] arr, int start, int end) {
		try {
			for (; start < end; start++)
				if (arr[start] != null)
					arr[start].close();
		} catch (Exception e) {
			// 仅记录异常，不抛出
			new DefinedRuntimeException("关闭资源失败", e);
			// 跳过出错的对象，从下一个对象开始继续逐个关闭
			if (++start < end)
				doClose(arr, start, end);
		}
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param ac
	 */
	public final static void close(AutoCloseable ac) {
		if (ac != null)
			try {
				ac.close();
			} catch (Exception e) {
				// 仅记录异常，不抛出
				new DefinedRuntimeException("关闭资源失败", e);
			}
	}

}