package qw.easyFrame.tools.statics;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 封装与字符串操作有关的静态方法.
 * @author Rex
 *
 */
public class StringUtil {
	
	/**
	 * 检查字符串对象是否为null，
	 * 若为null，则返回空字符串""，
	 * 反之返回原参数.strip()的值.
	 * @param str
	 * @return String
	 */
	public final static String strip(String str) {
		return str == null ? "" : str.strip();
	}
	
	/**
	 * 若传入的对象为null，或对象的toString()方法返回值为null，则返回字符串"null"，
	 * 反之返回对象的toString().strip()的值.
	 * @param obj
	 * @return
	 */
	public static final String toString(Object obj) {
		String objStr;
		if (obj != null && (objStr = obj.toString()) != null)
			return objStr.strip();
		return "null";
	}
	
	/**
	 * 判断在指定的字符串数组中是否包含指定字符串的后缀.<br>
	 * 注意：若ends参数为空数组，则返回值始终为false。
	 * @param str
	 * @param ends
	 * @return boolean
	 */
	public final static boolean endsWith(String str, String[] ends) {
		for (var path : ends)
			if (str.endsWith(path))
				return true;

		return false;
	}
	
	/**
	 * 判断在指定的字符串数组中是否包含指定字符串的后缀.<br>
	 * 若是，则返回匹配到的字符串后缀。若否，则返回null。<br>
	 * 注意：若ends参数为空数组，则返回值始终为null。
	 * @param str
	 * @param ends
	 * @return boolean
	 */
	public static String endsWithWhat(String str, String[] ends) {
		for (var path : ends)
			if (str.endsWith(path))
				return path;

		return null;
	}
	
	/**
	 * 敏感字符串脱敏.<br>
	 * 使用星号（*）替换原字符串中的部分字符，
	 * 常用于身份证号、客户号等的脱敏，<br>
	 * 如：13912341234 -> 139****1234。
	 * @param id 待脱敏的字符串
	 * @param start 开始位置下标
	 * @param end 结束位置下标
	 * @return
	 */
	public static String desensitize(String id, int start, int end) {
		var array = id.toCharArray();
		for (var i = start; i < end; i++)
			array[i] = '*';

		return new String(array);
	}
	
    /**
     * 反转字符串.<br>
     * 为了减少栈调用造成的CPU损耗，实际上本方法并未调用SortUtil.reverse(char[])。
     * 而是直接将SortUtil.reverse(char[])的代码复制到方法体内。<br>
     * 压力测试时，当连续调用次数少于三十万次时，
     * 此方法的性能高于StringBuilder的reverse()方法；
     * 若业务中需要连续调用reverse方法超过三十万次，
     * 建议使用new StringBuilder(String).reverse().toString()。
     * @param str
     * @see StringBuilder#reverse()
     * @see SortUtil#reverse(char[])
     * @return
     */
	public static final String reverse(String str) {
		var len = str.length();
		if (len > 1) {
			var times = len / 2;// 循环次数
			var lastIndex = len - 1;
			var cs = str.toCharArray();
			// 数组头尾元素依次交换
			for (var i = 0; i < times; i++) {
				var target = lastIndex - i;
				var temp = cs[i];
				cs[i] = cs[target];
				cs[target] = temp;
			}
			str = new String(cs);
		}
		return str;
	}
	
	/**
	 * 修改文件扩展名.<br>
	 * 若原文件名不包含扩展名，则直接将新拓展名加在原文件名末尾。
	 * @param src 原文件名
	 * @param tail 新扩展名（是否包含小数点均可）
	 * @return
	 */
	public final static String changeExtension(String src, String tail) {
		var pointed = tail.startsWith(".", 0);
		if (!pointed || !src.endsWith(tail)) {
			var index = src.lastIndexOf(".");
			if (index != -1)
				src = src.substring(0, index);
			src = src.concat(pointed ? "" : ".").concat(tail);
		}
		return src;
	}
	
	/**
	 * 从字符串中删除指定的子字符串.<br>
	 * 若匹配到多个子字符串，会全部删除。
	 * @param str
	 * @param sub
	 * @return
	 */
	public static String delete(String str, String sub) {
		var subLen = sub.length();

		var index = 0;
		if (subLen != 0)
			while (true)
				if ((index = str.indexOf(sub, index)) != -1)
					str = delete(str, index, subLen);
				else
					break;

		return str;
	}
	
	/**
	 * 从字符串中删除指定下标范围内的字符.
	 * @param str 源字符串
	 * @param index 要删除的范围的起始位置下标
	 * @param length 要删除的长度
	 * @return
	 */
	public final static String delete(String str, int index, int length) {
		var oldStr = str.toCharArray();
		var count = oldStr.length - length;
		System.arraycopy(oldStr, index + length, oldStr, index, count - index);
		return new String(oldStr, 0, count);
	}
	
	/**
	 * 根据regex分割src为数组，不包含空字符串，且元素均会被strip().
	 * @param src
	 * @param regex
	 * @return
	 */
	public static String[] split(String src, String regex) {
		var split = src.split(regex, 0);

		var len = split.length;
		var newLen = len;
		for (var i = 0; i < len; i++)
			if ((split[i] = split[i].strip()).isEmpty()) {
				split[i] = null;
				newLen--;
			}

		if (newLen == 0)
			return new String[0];
		if (newLen == len)
			return split;

		var arr = new String[newLen];
		var index = -1;
		for (var i = 0; i < newLen; i++) {
			do {
				index++;
			} while (split[index] == null);
			arr[i] = split[index];
		}

		return arr;
	}
	
	/**
	 * 获取Properties配置文件中的汉语配置项.<br>
	 * 此方法会对返回的配置项进行strip操作。
	 * @param p
	 * @param key
	 * @return
	 */
	public static String getHanProperty(Properties p, String key) {
		try {
			return new String(p.getProperty(key).strip().getBytes("iso-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new DefinedRuntimeException(false, "转换Properties配置文件编码失败", e);
		}
	}
	
}