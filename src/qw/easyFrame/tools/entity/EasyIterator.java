package qw.easyFrame.tools.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;

/**
 * 简易迭代器.
 * @author Rex
 * @param <T>
 */
public class EasyIterator<T> implements Iterator<T>, Iterable<T>, Cloneable, Serializable {
	private static final long serialVersionUID = 20220828L;
	
	private T[] target;
	private int start;
	private int lastIndex;
	private int cursor;
	
	public EasyIterator() {
		super();
	}

	public EasyIterator(T[] array) {
		this.target = array;
		this.start = 0;
		this.lastIndex = array.length -1;
		this.cursor = -1;
	}
	
	/**
	 * 此构造器可指定迭代长度.
	 * @param array 待迭代的数组
	 * @param length 迭代长度
	 */
	public EasyIterator(T[] array, int length) {
		this(array, 0, length);
	}
	
	/**
	 * 此构造器可指定迭代的起止位置.
	 * @param array 待迭代的数组
	 * @param start 迭代的起始位置（含）
	 * @param end   迭代的终止位置（不含）
	 */
	public EasyIterator(T[] array, int start, int end) {
		if (start > end)
			throw new ArrayIndexOutOfBoundsException("起始下标大于终止下标");
		if (start < 0)
			throw new ArrayIndexOutOfBoundsException(start);
		if (end > array.length)
			throw new ArrayIndexOutOfBoundsException(end);
		
		this.target = array;
		this.start = start;
		this.lastIndex = end - 1;
		this.cursor = start - 1;
	}

	/**
	 * 此方法返回的数组是原数组的拷贝，对返回的数组的操作不影响本迭代器内部的数组.
	 * @return
	 */
	public T[] getArray() {
		return this.target = Arrays.copyOfRange(target, start, lastIndex + 1);
	}

	public void setArray(T[] array) {
		this.target = array;
		this.start = 0;
		this.lastIndex = array.length -1;
		this.cursor = -1;
	}
	
	/**
	 * 此方法可指定迭代长度.
	 * @param array 待迭代的数组
	 * @param length 迭代长度
	 */
	public void setArray(T[] array, int length) {
		setArray(array, 0, length);
	}
	
	/**
	 * 此方法可指定迭代的起止位置.
	 * @param array 待迭代的数组
	 * @param start 迭代的起始位置（含）
	 * @param end 迭代的终止位置（不含）
	 */
	public void setArray(T[] array, int start, int end) {
		if (start > end)
			throw new ArrayIndexOutOfBoundsException("起始下标大于终止下标");
		if (start < 0)
			throw new ArrayIndexOutOfBoundsException(start);
		if (end > array.length)
			throw new ArrayIndexOutOfBoundsException(end);
		
		this.target = array;
		this.start = start;
		this.lastIndex = end - 1;
		this.cursor = start - 1;
	}
	
	/**
	 * 此克隆方法是深克隆.
	 */
	@Override
	public Object clone() {
		return iterator();
	}

	@Override
	public boolean hasNext() {
		return cursor != lastIndex;
	}

	@Override
	public T next() {
		cursor ++;
		return target[cursor];
	}
	
	@Override
	public Iterator<T> iterator() {
		var copyOf = Arrays.copyOfRange(target, start, lastIndex + 1);
		return new EasyIterator<>(copyOf);
	}
	
	/**
	 * 复位，使迭代器的下标恢复到迭代前的初始位置.
	 */
	public void reset() {
		this.cursor = start - 1;
	}

}