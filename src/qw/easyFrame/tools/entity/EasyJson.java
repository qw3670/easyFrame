package qw.easyFrame.tools.entity;

import java.io.Serializable;
import java.util.Map;
import qw.easyFrame.interfaces.Jsonable;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 用于生成普通json字符串或含有直接量（value两侧无双引号）的json字符串.<br>
 * 注意：非线程安全！<br>
 * 不支持生成嵌套结构的json字符串。<br>
 * 若不需要生成带有直接量的json字符串，可直接使用静态方法arrToJson。
 * @author Rex
 *
 */
public class EasyJson implements Jsonable, Serializable {
	private static final long serialVersionUID = 20230609L;
	
	protected StringBuilder sb = new StringBuilder("{");
	
	/**
	 * 记录键值对数量.
	 */
	private int size;
	
	public EasyJson() {
		super();
	}

	/**
	 * 输出的json中，此键值对的value两侧有双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, String value) {
		sb.append("\"").append(key).append("\":\"");
		sb.append(value).append("\",");
		size++;
	}
	
	/**
	 * 输出的json中，此键值对的value两侧有双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, Object value) {
		sb.append("\"").append(key).append("\":\"");
		sb.append(String.valueOf(value).strip()).append("\",");
		size++;
	}
	
	/**
	 * 输出的json中，此键值对的value两侧不加双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, long value) {
		sb.append("\"").append(key).append("\":");
		sb.append(value).append(",");
		size++;
	}
	
	/**
	 * 输出的json中，此键值对的value两侧不加双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, int value) {
		sb.append("\"").append(key).append("\":");
		sb.append(value).append(",");
		size++;
	}
	
	/**
	 * 输出的json中，此键值对的value两侧不加双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, double value) {
		sb.append("\"").append(key).append("\":");
		sb.append(value).append(",");
		size++;
	}
	
	/**
	 * 输出的json中，此键值对的value两侧不加双引号.
	 * @param key
	 * @param value
	 */
	public void add(String key, boolean value) {
		sb.append("\"").append(key).append("\":");
		sb.append(value).append(",");
		size++;
	}
	
	/**
	 * 此方法会在json字符串中增加一个value为null的键值对.<br>
	 * 输出的json中，此键值对的value两侧不加双引号。
	 * @param key
	 */
	public void add(String key) {
		sb.append("\"").append(key).append("\":");
		sb.append("null").append(",");
		size++;
	}

	/**
	 * 获取该JSON字符串中包含的键值对的数量.
	 * @return
	 */
	@Override
	public int jsonSize() {
		return size;
	}
	
	/**
	 * 清空json字符串（删除字符串内所有键值对）.
	 */
	public void clear() {
		if (this.size != 0) {
			this.size = 0;
			this.sb.delete(1, sb.length());
		}
	}

	/**
	 * 此方法不会缓存字符串，每次点用此方法均会重新生成新的字符串.
	 */
	@Override
	public final String toJson() {
		if (size == 0)
			return "{}";
		else
			return sb.deleteCharAt(sb.length() - 1).append("}").toString();
	}
	
	@Override
	public String toString() {
		return toJson();
	}
	
	/**
	 * 将对象数组转换为json字符串.<br>
	 * value两侧均有双引号。<br>
	 * 若不需要生成带有直接量的json字符串，可使用此方法。
	 * @param arr
	 * @return
	 */
	@SafeVarargs
	public static <T> String toJson(T... arr) {
		int len;
		if (arr == null || (len = arr.length) == 0)
			return "{}";
		if (len % 2 != 0)
			throw new DefinedRuntimeException(false, "数组长度必须为偶数");

		var sb = new StringBuilder("{");
		for (var i = 0; i < len; i++) {
			sb.append("\"").append(arr[i]).append("\":\"");
			i++;
			sb.append(arr[i]).append("\",");
		}
		sb.deleteCharAt(sb.length() - 1).append("}");

		return sb.toString();
	}
	
	/**
	 * 将map转换为json字符串.<br>
	 * value两侧均有双引号。<br>
	 * 若不需要生成带有直接量的json字符串，可直接使用此静态方法。
	 * @param <K>
	 * @param <V>
	 * @param map
	 * @return
	 */
	public static <K, V> String toJson(Map<K, V> map) {
		if (map == null || map.size() == 0)
			return "{}";

		var sb = new StringBuilder("{");
		for (var e : map.entrySet()) {
			sb.append("\"").append(e.getKey()).append("\":\"");
			sb.append(e.getValue()).append("\",");
		}
		sb.deleteCharAt(sb.length() - 1).append("}");

		return sb.toString();
	}
	
	/**
	 * 将数组转换为json.
	 * @param heads 封装Json的key
	 * @param infos 封装Json的value
	 * @return
	 */
	public final static <T, U> String toJson(T[] heads, U[] infos) {
		var sb = new StringBuilder("{");

		var last = infos.length - 1;
		var len = heads.length;
		for (var i = 0; i < len; i++) {
			sb.append("\"").append(heads[i]).append("\":\"");
			var info = i > last ? "" : infos[i].toString();
			sb.append(info).append("\"").append(",");
		}

		return sb.deleteCharAt(sb.length() - 1).append("}").toString();
	}

}