package qw.easyFrame.tools.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * 将多个不同的基本类型变量或字符串变量组合输出为字符串数组.
 * @author Rex
 *
 */
public class EasyStringArray implements Iterable<String>, Serializable {
	private static final long serialVersionUID = 20230609L;

	protected List<String> list;

	public EasyStringArray() {
		this.list = new ArrayList<>();
	}

	public EasyStringArray(int size) {
		this.list = new ArrayList<>(size);
	}
	
	public EasyStringArray(Collection<?> another) {
		this.list = new ArrayList<>(another.size());
		for (var obj : another)
			this.list.add(String.valueOf(obj).strip());
	}
	
	public void add(String element) {
		list.add(element.strip());
	}
	
	public void add(long element) {
		list.add(Long.toString(element));
	}
	
	public void add(int element) {
		list.add(Integer.toString(element));
	}
	
	public void add(double element) {
		list.add(Double.toString(element));
	}
	
	public void add(boolean element) {
		list.add(element ? "true" : "false");
	}
	
	public void add(Object element) {
		list.add(String.valueOf(element).strip());
	}
	
	public void addAll(Collection<?> another) {
		for (var obj : another)
			list.add(String.valueOf(obj).strip());
	}
	
	public <T> void addAll(T[] another) {
		for (var obj : another)
			list.add(String.valueOf(obj).strip());
	}
	
	public void addAll(long[] another) {
		for (var i : another)
			list.add(Long.toString(i));
	}
	
	public void addAll(int[] another) {
		for (var i : another)
			list.add(Integer.toString(i));
	}
	
	public void addAll(double[] another) {
		for (var i : another)
			list.add(Double.toString(i));
	}
	
	public void addAll(boolean[] another) {
		for (var i : another)
			list.add(i ? "true" : "false");
	}
	
	public void addAll(char[] another) {
		for (var i : another)
			list.add(String.valueOf(i));
	}
	
	/**
	 * 获取待输出的数组的长度.
	 * @return
	 */
	public int lenth() {
		return this.list.size();
	}
	
	/**
	 * 清空待输出的数组.
	 */
	public void clear() {
		this.list.clear();
	}
	
	public String[] toArray() {
		return list.toArray(new String[list.size()]);
	}
	
	@Override
	public Iterator<String> iterator() {
		return new EasyIterator<>(toArray());
	}
	
	/**
	 * 将对象数组转化为字符串数组.
	 * @param objs
	 * @return
	 */
	public static String[] objsToStrs(Object... objs) {
		int len;
		if (objs == null || (len = objs.length) == 0)
			return new String[0];

		var strs = new String[len];
		for (var i = 0; i < len; i++)
			strs[i] = String.valueOf(objs[i]).strip();

		return strs;
	}

}