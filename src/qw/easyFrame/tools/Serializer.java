package qw.easyFrame.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Properties;
import qw.easyFrame.enums.SecretKeySize;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.algorithm.AESUtil;
import qw.easyFrame.tools.algorithm.MTRandomFast;
import static qw.easyFrame.tools.statics.IOUtil.write;
import static qw.easyFrame.tools.statics.IOUtil.read;
import static qw.easyFrame.tools.statics.StringUtil.strip;
import static qw.easyFrame.tools.statics.EncoderUtil.encode;
import static qw.easyFrame.tools.statics.EncoderUtil.decode;

/**
 * 序列化器.
 * 序列化器本身不是一个可序列化对象。
 * @author Rex
 *
 */
public class Serializer implements Closeable {
	
	/**
	 * AES算法加密器.
	 */
	private final AESUtil aes;
	
	/**
	 * 使用原始密钥初始化序列化器.
	 * 密钥长度（AES加密强度）为128位。
	 * @param key
	 */
	public Serializer(String key) {
		this.aes = new AESUtil(key.getBytes(), SecretKeySize.AES_ECB_128, null);
	}
	
	/**
	 * 使用原始密钥和指定的密钥长度（AES加密强度）初始化序列化器.
	 * @param key
	 * @param keySize
	 */
	public Serializer(byte[] key, SecretKeySize keySize) {
		if(keySize.getTransformation().startsWith("AES/CBC"))
			throw new DefinedRuntimeException(false, "Serializer仅支持ECB模式的SecretKeySize参数");
		
		this.aes = new AESUtil(key, keySize, null);
	}
	
	/**
	 * 使用自定义的AES加密器初始化序列化器.<br>
	 * 注意：通过传入外部AESUtil对象构造的Serializer对象，
	 * 如果调用Serializer对象的close()方法，会同时调用外部AESUtil对象的close()方法。
	 * @param aes
	 */
	public Serializer(AESUtil aes) {
		this.aes = aes;
	}
	
	/**
	 * 根据配置文件初始化Serializer对象. <br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找Serializer.properties文件。<br>
	 * @param properties 配置文件
	 * @return
	 */
	public static Serializer createByProperties(File properties) {
		try (var is = properties == null
				? Thread.currentThread().getContextClassLoader().getResourceAsStream("Serializer.properties")
				: new FileInputStream(properties)) {
			var p = new Properties();
			p.load(is);

			SecretKeySize size = switch (strip(p.getProperty("keySize"))) {
			case "192" -> SecretKeySize.AES_ECB_192;
			case "256" -> SecretKeySize.AES_ECB_256;
			default -> SecretKeySize.AES_ECB_128;
			};

			var key = strip(p.getProperty("key"));
			if (key.isEmpty())
				throw new DefinedRuntimeException(false, "Serializer的配置文件缺少key配置项");

			return new Serializer(key.getBytes(), size);
		} catch (DefinedRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "根据配置文件生成Serializer对象失败", e);
		}
	}
	
	/**
	 * 使用一个随机的key来初始化Serializer.<br>
	 * 注意：由于每次调用此方法会随机生成一个key并用于构造一个序列化器（以下简称“随机序列化器”），
	 * 因此，每个随机序列化器仅在系统关闭前有效。
	 * 若使用随机序列化器后关闭了系统，重启系统时由于无法得到前一次的随机key，前一次被序列化的对象将无法反序列化。
	 * @param size 指定的密钥长度（AES加密强度）
	 * @return 使用随机值构造的序列化器
	 */
	public static Serializer createWithRandomKey(SecretKeySize size) {
		var ran = new MTRandomFast(
				System.nanoTime() - System.currentTimeMillis());
		// 生成一个长度在16~32范围内随机的byte数组
		var key = new byte[ran.nextInt(17) + 16];
		// 使用随机值填充数组
		ran.nextBytes(key);
		return new Serializer(key, size);
	}
	
	/**
	 * 将对象序列化为byte数组.
	 * @param obj
	 * @return
	 */
	public byte[] writeObject(Serializable obj) {
		// 读取对象为byte数组
		var bytes = objToBytes(obj);
		// 使用AES算法加密byte数组
		return aes.encrypt(bytes);
	}
	
	/**
	 * 将对象序列化到本地文件.
	 * @param obj
	 * @param target
	 */
	public void writeObject(Serializable obj, File target) {
		write(writeObject(obj), target, false);
	}
	
	/**
	 * 将对象序列化为Base64字符串.
	 * @param obj
	 * @return
	 */
	public String objToBase64(Serializable obj) {
		var bytes = encode(writeObject(obj));
		return new String(bytes, 0, bytes.length);
	}
	
	/**
	 * 将byte数组反序列化为对象.
	 * @param bytes
	 * @return
	 */
	public Object readObject(byte[] bytes) {
		// 使用AES算法解密byte数组
		bytes = aes.decrypt(bytes);
		// 读取byte数组为对象
		return bytesToObj(bytes);
	}
	
	/**
	 * 将本地文件反序列化为对象.
	 * @param target
	 * @return
	 */
	public Object readObject(File target) {
		var bytes = read(target);
		return readObject(bytes);
	}
	
	/**
	 * 将Base64字符串反序列化为对象.
	 * @param base64
	 * @return
	 */
	public Object base64ToObj(String base64) {
		var bytes = decode(base64.getBytes());
		return readObject(bytes);
	}

	/**
	 * 转换Java对象为byte数组.
	 * @param obj
	 * @return
	 */
	private final static byte[] objToBytes(Serializable obj) {
		try (var out = new ByteArrayOutputStream();
				var sOut = new ObjectOutputStream(out)) {
			sOut.writeObject(obj);
			sOut.flush();
			return out.toByteArray();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "序列化对象失败", e);
		}
	}
	
	/**
	 * 读取byte数组为Java对象.
	 * @param bytes
	 * @return
	 */
	private final static Object bytesToObj(byte[] bytes) {
		try (var in = new ByteArrayInputStream(bytes);
				var sIn = new ObjectInputStream(in)) {
			return sIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			throw new DefinedRuntimeException(false, "反序列化对象失败", e);
		}
	}

	/**
	 * 使用close()方法可将aes加密器关闭，以避免密钥泄露. <br>
	 * 注意：强烈建议在Serializer对象使用完毕后直接将其变量置为null，
	 * 仅在不可出现null值的业务逻辑中才建议使用close()方法关闭对象。<br>
	 * 注意：如果Serializer对象是通过传入一个外部AESUtil对象（使用Serializer(AESUtil aes)构造器）构造的，
	 * close()方法会调用外部AESUtil对象的close()方法。
	 * @see AESUtil#close()
	 * @see Serializer#Serializer(AESUtil)
	 */
	@Override
	public void close() {
		this.aes.close();
	}

}