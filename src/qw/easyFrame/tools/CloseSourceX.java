package qw.easyFrame.tools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import qw.easyFrame.interfaces.ThrowHandler;
import qw.easyFrame.interfaces.ThrowHandler.ThrowHandlers;
import qw.easyFrame.tools.statics.CloseSource;

/**
 * CloseSource的增强版.<br>
 * 此类将CloseSource的功能封装为对象，并增加了功能。
 * 在关闭资源时可能出现的异常，此类可按自定义方式进行处理。<br>
 * 注意：一次传入多个待关闭的资源对象时，此类会使用for循环依次关闭每个对象。
 * 故对于THROW_ONLY和THROW_AND_LOG这个两个模式，
 * 由于其会将包装后的新的异常对象抛出，所以会打断for循环！
 * @see CloseSource
 * @author Rex
 *
 */
public class CloseSourceX implements Serializable {
	private static final long serialVersionUID = 20230414L;

	/**
	 * 若在关闭资源时出现异常，规定如何处理.
	 */
	private ThrowHandler handler;
	
	/**
	 * 特殊class（可不设置）.
	 * 此class的实例在关闭时若出现异常，使用特殊方式处理。
	 */
	private List<Class<? extends AutoCloseable>> spClasses;
	
	/**
	 * 特殊class的实例在关闭时出现异常的处理方式.
	 * 若设置了spClass，则此变量也必须设置。
	 */
	private ThrowHandler spHandler;
	
	public CloseSourceX() {
		super();
	}

	public CloseSourceX(ThrowHandler handler) {
		this.handler = handler;
	}

	public ThrowHandler getExceptionHandler() {
		return handler;
	}
	public void setExceptionHandler(ThrowHandler handler) {
		this.handler = handler;
	}
	
	public List<Class<? extends AutoCloseable>> getSpClasses() {
		return spClasses;
	}
	public void setSpClasses(List<Class<? extends AutoCloseable>> spClasses) {
		this.spClasses = spClasses;
	}
	
	@SuppressWarnings("unchecked")
	public void setSpClasses(Class<? extends AutoCloseable>... spClasses) {
		var len = spClasses.length;
		this.spClasses = new ArrayList<>(len);
		for (var i = 0; i < len; i++)
			this.spClasses.add(spClasses[i]);
	}
	
	public ThrowHandler getSpHandler() {
		return spHandler;
	}
	public void setSpHandler(ThrowHandler spHandler) {
		this.spHandler = spHandler;
	}
	
	/**
	 * 添加特殊class.
	 * @param spClasses
	 */
	@SuppressWarnings("unchecked")
	public void addSpClass(Class<? extends AutoCloseable>... spClasses) {
		if (this.spClasses == null)
			this.spClasses = new LinkedList<>();
		for (var c : spClasses)
			this.spClasses.add(c);
	}
	
	/**
	 * 添加特殊class.
	 * @param spClass
	 */
	public void addSpClass(List<Class<? extends AutoCloseable>> spClasses) {
		if (this.spClasses == null)
			this.spClasses = new LinkedList<>();
		this.spClasses.addAll(spClasses);
	}

	/**
	 * 关闭AutoCloseable资源.
	 * @param acs
	 */
	public void close(AutoCloseable... acs) {
		int len;
		if (acs != null && (len = acs.length) != 0)
			doClose(acs, 0, len);
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param list
	 */
	public void close(Collection<AutoCloseable> list) {
		int size;
		if (list != null && (size = list.size()) != 0)
			doClose(list.toArray(new AutoCloseable[size]), 0, size);
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param arr
	 * @param start
	 * @param end
	 */
	public void close(AutoCloseable[] arr, int start, int end) {
		int len;
		if (arr != null && start > -1 && start < (len = arr.length)) {
			if (end > len)
				end = len;
			doClose(arr, start, end);
		}
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param arr
	 * @param start
	 * @param end
	 */
	private final void doClose(AutoCloseable[] arr, int start, int end) {
		try {
			for (; start < end; start++)
				if (arr[start] != null)
					arr[start].close();
		} catch (Exception e) {
			// 处理
			handle(arr[start], e);
			// 跳过出错的对象，从下一个对象开始继续逐个关闭
			if (++start < end)
				doClose(arr, start, end);
		}
	}
	
	/**
	 * 关闭AutoCloseable资源.
	 * @param ac
	 */
	public void close(AutoCloseable ac) {
		if (ac != null)
			try {
				ac.close();
			} catch (Exception e) {
				handle(ac, e);
			}
	}

	/**
	 * 根据既定的规则，处理在关闭AutoCloseable对象时产生的异常.
	 * @param ac
	 * @param e
	 */
	private final void handle(AutoCloseable ac, Exception e) {
		var currentHandler = handler;

		if (spClasses != null && spHandler != null && isSpClass(ac))
			currentHandler = spHandler;
		else if (currentHandler == null)
			currentHandler = ThrowHandlers.newIgnore();// 忽略异常

		currentHandler.handle(e, ac);
	}
	
	/**
	 * 判断对象是否是特殊class的实例.
	 * @param ac
	 * @return
	 */
	private final boolean isSpClass(AutoCloseable ac) {
		final var spClasses = this.spClasses;
		for (var clazz : spClasses)
			if (ac.getClass() == clazz)
				return true;

		return false;
	}
	
}