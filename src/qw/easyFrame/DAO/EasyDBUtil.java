package qw.easyFrame.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import qw.easyFrame.DAO.proxy.ResultSetProxy;
import qw.easyFrame.DAO.proxy.StatementProxy;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.StringUtil.strip;

/**
 * 数据库连接工具.<br>
 * 此class不使用连接池，每次获取连接均会新建连接。
 * 且由于无连接池，调用返回的连接自带的close()方法会直接关闭连接。
 * @author Rex
 */
public class EasyDBUtil implements Serializable {
	private static final long serialVersionUID = 20230609L;
	
	private final String driverClassName;
	private final String url;
	private final String user;
	
	/**
	 * 序列化时忽略密码.
	 */
	transient private final String pwd;

	/**
	 * 手动传参初始化.
	 * @param driverClassName
	 * @param url
	 * @param user
	 * @param pwd
	 */
	public EasyDBUtil(String driverClassName, String url, String user, String pwd) {
		// 加载驱动
		try {
			this.driverClassName = driverClassName;
			Class.forName(driverClassName);
			this.url = url;
			this.user = user;
			this.pwd = pwd;
		} catch (ClassNotFoundException e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil加载数据库驱动失败", e);
		}
	}
	
	/**
	 * 使用DefaultDBUtil对象和密码构造一个新的DefaultDBUtil对象.
	 * 注意，反序列化后的DefaultDBUtil对象不能直接使用，必须使用此构造器重新构造。
	 * @param util
	 * @param pwd
	 */
	public EasyDBUtil(EasyDBUtil util, String pwd) {
		// 加载驱动
		try {
			this.driverClassName = util.driverClassName;
			Class.forName(this.driverClassName);
			this.url = util.url;
			this.user = util.user;
			this.pwd = pwd;
		} catch (ClassNotFoundException e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil加载数据库驱动失败", e);
		}
	}
	
	/**
	 * 使用配置文件自动初始化.<br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找DB.properties文件。<br>
	 * @param properties 配置文件
	 * @return
	 */
	public static EasyDBUtil createByProperties(File properties) {
		try (var is = properties == null
				? Thread.currentThread().getContextClassLoader().getResourceAsStream("DB.properties")
				: new FileInputStream(properties)) {
			// 加载配置文件
			var p = new Properties();
			p.load(is);
			return new EasyDBUtil(strip(p.getProperty("driverClassName")), strip(p.getProperty("url")),
					strip(p.getProperty("username")), strip(p.getProperty("password")));
		} catch (NullPointerException | IOException e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil加载配置文件异常", e);
		} catch (DefinedRuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil初始化数据库连接失败", e);
		}
	}

	/**
	 * 获取数据库连接.
	 * @return
	 */
	public Connection getConnection() {
		try {
			// 建立连接
			return DriverManager.getConnection(url, user, pwd);
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil建立数据库连接失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "DefaultDBUtil获取数据库连接失败", e);
		}
	}
	
	/**
	 * 直接获得Statement对象.
	 * @see Connection#createStatement()
	 * @return
	 */
	public Statement createStatement() {
		try {
			var conn = DriverManager.getConnection(url, user, pwd);
			var sta = conn.createStatement();
			// 动态代理
			var clazz = sta.getClass();
			return (Statement) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
					new StatementProxy(conn, sta));
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "Statement动态代理失败", e);
		}
	}
	
	/**
	 * 直接获得查询结果集对象.
	 * @see Statement#executeQuery(String)
	 * @param sql
	 * @return
	 */
	public ResultSet executeQuery(String sql) {
		try {
			var conn = DriverManager.getConnection(url, user, pwd);
			var sta = conn.createStatement();
			var query = sta.executeQuery(sql);
			// 动态代理
			return (ResultSet) Proxy.newProxyInstance(query.getClass().getClassLoader(),
					new Class<?>[] { ResultSet.class }, new ResultSetProxy(conn, sta, query));
		} catch (IllegalArgumentException e) {
			throw new DefinedRuntimeException(false, "ResultSet动态代理失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接执行executeUpdate方法（常用于update和delete语句）.
	 * @see Statement#executeUpdate(String)
	 * @param sql
	 * @return
	 */
	public int executeUpdate(String sql) {
		try (var conn = DriverManager.getConnection(url, user, pwd);
				var sta = conn.createStatement()) {
			return sta.executeUpdate(sql);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接执行execute方法（常用于insert语句）.
	 * @see Statement#execute(String)
	 * @param sql
	 * @return
	 */
	public boolean execute(String sql) {
		try (var conn = DriverManager.getConnection(url, user, pwd);
				var sta = conn.createStatement()) {
			return sta.execute(sql);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "执行".concat(sql).concat("失败"), e);
		}
	}
	
}