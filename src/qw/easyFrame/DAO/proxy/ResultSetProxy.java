package qw.easyFrame.DAO.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.CloseSource;

/**
 * ResultSet的动态代理类.
 * @author Rex
 *
 */
public class ResultSetProxy implements InvocationHandler {

	private Connection conn;
	private Statement st;
	private ResultSet set;
	
	public ResultSetProxy(Connection conn, Statement st, ResultSet set) {
		this.conn = conn;
		this.st = st;
		this.set = set;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) {
		try {
			if ("close".equals(method.getName())) {
				CloseSource.close(set, st, conn);
				return null;
			}
			return method.invoke(set, args);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "ResultSetProxy静态代理失败", e);
		}
	}

}