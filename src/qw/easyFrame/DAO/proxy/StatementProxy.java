package qw.easyFrame.DAO.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Statement;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.CloseSource;

/**
 * PreparedStatement和Statement的动态代理类.
 * @author Rex
 *
 */
public class StatementProxy implements InvocationHandler {
	
	private Connection conn;
	private Statement st;
	
	public StatementProxy(Connection conn, Statement st) {
		this.conn = conn;
		this.st = st;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) {
		try {
			if ("close".equals(method.getName())) {
				CloseSource.close(st, conn);
				return null;
			}
			return method.invoke(st, args);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "StatementProxy静态代理失败", e);
		}
	}
	
}