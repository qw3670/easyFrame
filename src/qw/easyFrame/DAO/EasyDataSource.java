package qw.easyFrame.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;
import javax.sql.DataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import qw.easyFrame.DAO.proxy.ResultSetProxy;
import qw.easyFrame.DAO.proxy.StatementProxy;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 从数据库连接池中获取连接并操作连接的实体类.<br>
 * 注意：createDruid(File properties)方法依赖druid-1.2.20.jar。
 * @author Rex
 *
 */
public class EasyDataSource implements DataSource {

	/**
	 * 连接池.
	 */
	private final DataSource pool;

	public EasyDataSource(DataSource pool) {
		this.pool = pool;
	}

	/**
	 * 加载druid连接池配置文件并初始化druid数据源.<br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找Druid.properties文件。
	 * @param properties 配置文件
	 * @return
	 */
	public static final EasyDataSource createDruid(File properties) {
		try (var is = properties == null
				? Thread.currentThread().getContextClassLoader().getResourceAsStream("Druid.properties")
				: new FileInputStream(properties)) {
			// 加载配置文件
			var p = new Properties();
			p.load(is);
			// 加载驱动程序
			return new EasyDataSource(DruidDataSourceFactory.createDataSource(p));
		} catch (NullPointerException | IOException e) {
			throw new DefinedRuntimeException(false, "DataSourceUtil加载配置文件异常", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "初始化Druid连接池失败", e);
		}
	}
	
	/**
	 * 获得数据库连接.
	 * @return Connection
	 */
	@Override
	public Connection getConnection() {
		try {
			return pool.getConnection();
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "从连接池获取连接失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "获取数据库连接失败", e);
		}
	}
	
	/**
	 * 直接获得预编译SQL对象.
	 * @see Connection#prepareStatement(String)
	 * @param sql
	 * @return
	 */
	public PreparedStatement prepareStatement(String sql) {
		try {
			var conn = pool.getConnection();
			var ps = conn.prepareStatement(sql);
			// 动态代理
			var clazz = ps.getClass();
			return (PreparedStatement) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
					new StatementProxy(conn, ps));
		} catch (IllegalArgumentException e) {
			throw new DefinedRuntimeException(false, "PreparedStatement动态代理失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接获得查询结果集对象.
	 * 仅支持参数全部为String类型的SQL。
	 * @see PreparedStatement#executeQuery()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public ResultSet executeQuery(String sql, String... args) {
		try {
			var conn = pool.getConnection();
			var ps = conn.prepareStatement(sql);

			setParameters(args, ps);

			var query = ps.executeQuery();
			// 动态代理
			var clazz = query.getClass();
			return (ResultSet) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
					new ResultSetProxy(conn, ps, query));
		} catch (IllegalArgumentException e) {
			throw new DefinedRuntimeException(false, "ResultSet动态代理失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接获得查询结果集对象.
	 * 仅支持参数全部为int类型的SQL。
	 * @see PreparedStatement#executeQuery()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public ResultSet executeQuery(String sql, int... args) {
		try {
			var conn = pool.getConnection();
			var ps = conn.prepareStatement(sql);

			setParameters(args, ps);

			var query = ps.executeQuery();
			// 动态代理
			var clazz = query.getClass();
			return (ResultSet) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
					new ResultSetProxy(conn, ps, query));
		} catch (IllegalArgumentException e) {
			throw new DefinedRuntimeException(false, "ResultSet动态代理失败", e);
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接执行executeUpdate方法（常用于update和delete语句）.
	 * 仅支持参数全部为String类型的SQL。
	 * @see PreparedStatement#executeUpdate()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public int executeUpdate(String sql, String... args) {
		try (var conn = pool.getConnection();
				var ps = conn.prepareStatement(sql)) {

			setParameters(args, ps);

			return ps.executeUpdate();
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接执行executeUpdate方法（常用于update和delete语句）.
	 * 仅支持参数全部为int类型的SQL。
	 * @see PreparedStatement#executeUpdate()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public int executeUpdate(String sql, int... args) {
		try (var conn = pool.getConnection();
				var ps = conn.prepareStatement(sql)) {

			setParameters(args, ps);

			return ps.executeUpdate();
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 *直接执行execute方法（常用于insert语句）.
	 * 仅支持参数全部为String类型的SQL。
	 * @see PreparedStatement#execute()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public boolean execute(String sql, String... args) {
		try (var conn = pool.getConnection();
				var ps = conn.prepareStatement(sql)) {

			setParameters(args, ps);

			return ps.execute();
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}
	
	/**
	 * 直接执行execute方法（常用于insert语句）.
	 * 仅支持参数全部为int类型的SQL。
	 * @see PreparedStatement#execute()
	 * @param sql 
	 * @param args 参数集
	 * @return
	 */
	public boolean execute(String sql, int... args) {
		try (var conn = pool.getConnection();
				var ps = conn.prepareStatement(sql)) {

			setParameters(args, ps);

			return ps.execute();
		} catch (Exception e) {
			throw new DefinedRuntimeException(false, "预编译或执行".concat(sql).concat("失败"), e);
		}
	}

	/**
	 * 设置PreparedStatement参数.
	 * @param is
	 * @param ps
	 */
	private final static void setParameters(int[] is, PreparedStatement ps) {
		try {
			var len = is.length;
			for (var i = 0; i < len;) {
				var arg = is[i];
				ps.setInt(++i, arg);
			}
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "设置PreparedStatement参数异常", e);
		}
	}
	
	/**
	 * 设置PreparedStatement参数.
	 * @param ss
	 * @param ps
	 */
	private final static void setParameters(String[] ss, PreparedStatement ps) {
		try {
			var len = ss.length;
			for (var i = 0; i < len;) {
				var arg = ss[i];
				ps.setString(++i, arg);
			}
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "设置PreparedStatement参数异常", e);
		}
	}

	@Override
	public Logger getParentLogger() {
		try {
			return pool.getParentLogger();
		} catch (SQLFeatureNotSupportedException e) {
			throw new DefinedRuntimeException(false, "getParentLogger()异常", e);
		}
	}

	@Override
	public <T> T unwrap(Class<T> iface) {
		try {
			return pool.unwrap(iface);
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "unwrap(Class<T> iface)异常", e);
		}
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) {
		try {
			return pool.isWrapperFor(iface);
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "isWrapperFor(Class<?> iface)异常", e);
		}
	}

	@Override
	public Connection getConnection(String username, String password) {
		try {
			return pool.getConnection();
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "getConnection(String username, String password)异常", e);
		}
	}

	@Override
	public PrintWriter getLogWriter() {
		try {
			return pool.getLogWriter();
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "getLogWriter()异常", e);
		}
	}

	@Override
	public void setLogWriter(PrintWriter out) {
		try {
			pool.setLogWriter(out);
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "setLogWriter(PrintWriter out)异常", e);
		}
	}

	@Override
	public void setLoginTimeout(int seconds) {
		try {
			pool.setLoginTimeout(seconds);
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "setLoginTimeout(int seconds)异常", e);
		}
	}

	@Override
	public int getLoginTimeout() {
		try {
			return pool.getLoginTimeout();
		} catch (SQLException e) {
			throw new DefinedRuntimeException(false, "getLoginTimeout()异常", e);
		}
	}

}