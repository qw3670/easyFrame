package qw.easyFrame.concurrent;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import qw.easyFrame.interfaces.Loggable;
import qw.easyFrame.interfaces.Processable;
import qw.easyFrame.throwables.DefinedException;
import qw.easyFrame.throwables.DefinedRuntimeException;
import qw.easyFrame.tools.statics.CloseSource;
import static java.util.Locale.ENGLISH;
import static qw.easyFrame.tools.statics.NumberUtil.parseInt;

/**
 * 封装写日志操作的实体类. <br><br>
 * 注意事项： <br>
 * 1、类中所有以File对象为参数的方法均不会检查File对象是否合法， 调用者请自行确保传入的参数可用；<br>
 * 2、构造对象共需10个参数，参数的值在调用构造器时已全部确定（无参构造器会使用默认参数构造对象，而不是简单的super()语句，
 * 非全参构造器也会使用默认参数补齐缺少的参数），且部分参数在构造器内确定后禁止再更改（没有设置相应的set方法）。
 * 因此，若希望使用自定义参数（而不是默认参数）构造对象，则强烈建议调用全参构造器或使用XML配置文件，
 * 在构造时就传入全部自定义参数，而不是使用无参构造器构造完成之后再使用set方法修改类变量的值。
 * @author Rex
 *
 */
public final class Logger implements Closeable {
	
	private final EasyMQ<Loggable> emq;
	
	/**
	 * 文件输出流.
	 */
	private OutputStream outputStream;

	/**
	 * 用于格式化当前系统时间.<br>
	 * 在构造器和timesuffix变量的setter方法内根据timesuffix的值决定是否初始化此变量。
	 */
	private SimpleDateFormat timesuffix/* = new SimpleDateFormat(" 'at' yyyy-MM-dd HH:mm:ss") */;
	
	/**
	 * 原始文件名（未添加日期后缀和扩展名的文件名）.
	 */
	private String path;

	/**
	 * 最终输出的日志文件. 
	 */
	private File file;
	
	/**
	 * 是否在原文件尾部继续写入（为false时，直接替换原文件）.
	 * @see Logger#isAppend()
	 */
	private boolean append;
	
	/**
	 * 是否按日期输出日志文件.
	 * @see Logger#isDated()
	 */
	private SimpleDateFormat dated;
	
	/**
	 * 换行符.
	 */
	private final String lineFeed;
	
	/**
	 * 是否在每条日志末尾添加来源后缀（若有）.
	 */
	private boolean domainSuffix;
	
	/**
	 * 上一次写日志的当天00:00时刻的毫秒数.
	 */
	private long lastTime;
	
	/**
	 * 用于连接待输出的日志字符串.
	 */
	private final StringBuilder sb = new StringBuilder();
	
	/**
	 * 根据配置文件生成Logger对象. 
	 * <br><br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找Logger.xml文件。
	 * <br><br>
	 * branch1：若解析xml出现异常（DefinedException），
	 * 则会返回一个默认路径（见无参构造器文档）下以type参数为文件名、其他参数为默认值（见全参构造器文档）的Logger对象；
	 * <br><br>
	 * branch2：若使用XML指定的path构造Logger时出现异常（DefinedRuntimeException），
	 * 则会返回一个以XML指定的sparePath为文件名、其他参数仍为XML指定值的Logger对象； 
	 * <br><br>
	 * branch3：若执行branch2时出现异常，
	 * 则会返回一个默认路径（见无参构造器文档）下以type参数为文件名、其他参数为默认值（见全参构造器文档）的Logger对象；
	 * @param xmlFile xml文件
	 * @param type 对应配置文件中&lt;exception&gt;标签的type属性
	 * @return
	 */
	public static final Logger createByXML(File xmlFile, String type) {
		XMLParser xml = null;
		try {
			xml = XMLParser.read(xmlFile, type);
			return new Logger(xml.path, xml.timesuffix, xml.append, xml.maxSize, xml.threadPriority, xml.daemon,
					xml.threadName, xml.dated, xml.extraLineFeed, xml.domainSuffix);
		} catch (DefinedRuntimeException e) {// 使用path构造Logge失败
			try {
				// 尝试使用sparePath构造Logger
				return new Logger(xml.sparePath, xml.timesuffix, xml.append, xml.maxSize, xml.threadPriority,
						xml.daemon, xml.threadName, xml.dated, xml.extraLineFeed, xml.domainSuffix);
			} catch (Exception ex) {// 尝试使用sparePath构造Logger失败
				return new Logger(defaultPath(type), true, true, 128, 5, true, "Logger", false, false, true);
			}
		} catch (Exception e) {// 捕捉包括DefinedException（解析xml异常）在内的所有异常
			return new Logger(defaultPath(type), true, true, 128, 5, true, "Logger", false, false, true);
		}
	}
	
	/**
	 * 根据当前操作系统，生成默认的日志路径.
	 * @param fileName
	 * @return
	 */
	private final static String defaultPath(String fileName) {
		// Windows
		if (System.getProperty("os.name").toLowerCase(ENGLISH).indexOf("windows", 0) > -1) {
			var file = new File("C:\\temp");
			if (!file.exists())
				file.mkdir();
			return "C:\\temp\\".concat(fileName);
		}
		// Linux/Unix
		return "/usr/".concat(fileName);
	}

	/**
	 * 默认的日志输出路径：<br>
	 * Windows : C:\temp\error.log，<br>
	 * Linux/Unix : /usr/error.log.<br>
	 * 注意：对于非Windows系统，Logger类默认其是Linux/Unix系统。
	 * 即，Logger类无法生成除Windows和Linux/Unix之外的其他系统（以下统称“其他系统”）的默认路径。
	 * 故在其他系统下使用时，请使用代码手动传入路径参数（或使用配置文件指定路径），
	 * 避免使用无参构造器生成默认路径。但在使用配置文件指定路径时，若出现IO异常、配置文件文本格式有误、
	 * 配置项不全等情况，仍会调用无参构造器生成默认路径。
	 */
	public Logger() {
		this(defaultPath("error"), true, true, 128, 5, true, "Logger", false, false, true);
	}
	
	/**
	 * 构造器不会检查path对象是否合法， 调用者请自行确保传入的参数可用.<br>
	 * 其余参数均使用默认值。
	 * @param path
	 */
	public Logger(String path) {
		this(path, true, true, 128, 5, true, "Logger", false, false, true);
	}
	
	/**
	 * 构造器不会检查path对象是否合法， 调用者请自行确保传入的参数可用.<br>
	 * 其余参数均使用默认值。
	 * @param path
	 * @param timesuffix 是否在每条日志末尾增加时间后缀
	 * @param dated 是否按日期输出日志文件
	 * @param extraLineFeed 是否在每条日志末尾追加额外的换行
	 * @param domainSuffix 是否在每条日志末尾添加来源后缀（若有）
	 */
	public Logger(String path, boolean timesuffix, boolean dated, boolean extraLineFeed, boolean domainSuffix) {
		this(path, timesuffix, true, 128, 5, true, "Logger", dated, extraLineFeed, domainSuffix);
	}
	
	/**
	 * 构造器不会检查path对象是否合法， 调用者请自行确保传入的参数可用.
	 * @param path
	 * @param timesuffix 是否在每条日志末尾增加时间后缀
	 * @param append 设置写日志时，是否在原文件尾部持续写入（为false时，直接替换原文件）
	 * @param maxSize 队列长度
	 * @param threadPriority 线程优先级
	 * @param daemon 是否设为守护线程
	 * @param threadName 线程名
	 * @param dated 是否按日期输出日志文件
	 * @param extraLineFeed 是否在每条日志末尾追加额外的换行
	 * @param domainSuffix 是否在每条日志末尾添加来源后缀（若有）
	 */
	public Logger(String path, boolean timesuffix, boolean append, int maxSize, int threadPriority, boolean daemon,
			String threadName, boolean dated, boolean extraLineFeed, boolean domainSuffix) {
		try {
			this.append = append;
			this.lineFeed = extraLineFeed ? "\r\n\r\n" : "\r\n";
			this.domainSuffix = domainSuffix;
			setFile(path, dated);
			if (timesuffix)
				this.timesuffix = new SimpleDateFormat(" 'at' yyyy-MM-dd HH:mm:ss");
			this.emq = new EasyMQ<>(maxSize, daemon, threadPriority, threadName);
			this.emq.setRunnable(this::process);
			// 启动消费者线程
			emq.start();
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "创建Logger文件输出流异常", e);
		}
	}
	
	/**
	 * 使用自定义的方法异步处理Logger对象.
	 * @param maxSize 队列长度
	 * @param threadPriority 线程优先级
	 * @param daemon 是否设为守护线程
	 * @param threadName 线程名
	 * @param process 自定义的处理Loggable对象的方法
	 */
	public Logger(int maxSize, int threadPriority, boolean daemon, String threadName, Processable process) {
		this.lineFeed = null;
		this.emq = new EasyMQ<>(maxSize, daemon, threadPriority, threadName);
		this.emq.setRunnable(process::process);
		// 启动消费者线程
		emq.start();
	}

	/**
	 * 构造一个同步的Logger.<br>
	 * 构造器不会检查path对象是否合法，调用者请自行确保传入的参数可用。
	 * @param path
	 * @param timesuffix 是否在每条日志末尾增加时间后缀
	 * @param append 设置写日志时，是否在原文件尾部持续写入（为false时，直接替换原文件）
	 * @param dated 是否按日期输出日志文件
	 * @param domainSuffix 是否在每条日志末尾添加来源后缀（若有）
	 * @param flag 无实际作用，仅用于与其它构造器区分
	 */
	protected Logger(String path, boolean timesuffix, boolean append, boolean dated, boolean extraLineFeed,
			boolean domainSuffix, Object flag) {
		try {
			this.append = append;
			this.lineFeed = extraLineFeed ? "\r\n\r\n" : "\r\n";
			this.domainSuffix = domainSuffix;
			setFile(path, dated);
			if (timesuffix)
				this.timesuffix = new SimpleDateFormat(" 'at' yyyy-MM-dd HH:mm:ss");
			this.emq = null;
		} catch (IOException e) {
			throw new DefinedRuntimeException(false, "创建Logger文件输出流异常", e);
		}
	}

	/**
	 * 构造一个同步的Logger.<br>
	 * 构造器不会检查path对象是否合法，调用者请自行确保传入的参数可用。
	 * @param path
	 * @param timesuffix 是否在每条日志末尾增加时间后缀
	 * @param append 设置写日志时，是否在原文件尾部持续写入（为false时，直接替换原文件）
	 * @param dated 是否按日期输出日志文件
	 * @param domainSuffix 是否在每条日志末尾添加来源后缀（若有）
	 * @return
	 */
	public static Logger syncLogger(String path, boolean timesuffix, boolean append, boolean dated,
			boolean extraLineFeed, boolean domainSuffix) {
		return new Logger(path, timesuffix, append, dated, extraLineFeed, domainSuffix, null);
	}
	
	/**
	 * 根据配置，设置文件名（文件输出流）.
	 * @param path
	 * @param dated
	 * @throws IOException
	 */
	private final void setFile(String path, boolean dated) throws IOException {
		var extension = ".log";
		if (dated) {
			this.dated = new SimpleDateFormat(" yyyy-MM-dd");
			extension = recordDate(new Date());
		}
		this.path = path;
		this.file = new File(path.concat(extension));
		this.outputStream = new FileOutputStream(file, this.append);
	}

	/**
	 * 获取并记录当天00:00时刻的毫秒数，并返回带日期后缀的扩展名.
	 * @param d
	 * @return
	 */
	private final String recordDate(Date d) {
		final var sdf = this.dated;
		try {
			var date = sdf.format(d);
			// 获取当天00:00时刻的毫秒数
			this.lastTime = sdf.parse(date).getTime();
			return date.concat(".log");
		} catch (ParseException e) {
			throw new DefinedRuntimeException(false, "Logger解析系统日期异常", e);
		}
	}

	/**
	 * 得到当前日志文件.
	 * @see Logger#file
	 * @return 日志文件
	 */
	public File getFile() {
		return file;
	}

	/**
	 * 写日志时，是否在原文件尾部继续写入（为false时，直接替换原文件）.
	 * 此值在构造Logger对象时确定，之后不得修改。
	 * @return
	 */
	public boolean isAppend() {
		return append;
	}

	/**
	 * 得到是否添加时间后缀的布尔值.
	 * @see Logger#suffix
	 * @return
	 */
	public boolean isTimesuffix() {
		return this.timesuffix != null;
	}
	
	/**
	 * 获取队列最大容量.<br>
	 * 此值在构造Logger对象时确定，之后不得修改。
	 * @return
	 */
	public int getMaxSize() {
		final var emq = this.emq;
		return emq.getMaxSize();
	}
	
	/**
	 * 得到后台线程优先级.<br>
	 * 此值在构造Logger对象时确定，之后不得修改。
	 * @return
	 */
	public int getThreadPriority() {
		final var emq = this.emq;
		return emq.getThreadPriority();
	}
	
	/**
	 * 写日志的线程是否是守护线程.<br>
	 * 此值在构造Logger对象时确定，之后不得修改。
	 * @return
	 */
	public boolean isDaemon() {
		final var emq = this.emq;
		return emq.isDaemon();
	}
	
	/**
	 * 写日志的线程的线程名.<br>
	 * 此值在构造Logger对象时确定，之后不得修改。
	 * @return
	 */
	public String getThreadName() {
		final var emq = this.emq;
		return emq.getThreadName();
	}
	
	/**
	 * 是否按日期输出日志文件.
	 * @see Logger#dated
	 * @return
	 */
	public boolean isDated() {
		return dated != null;
	}
	
	/**
	 * 是否在每条日志末尾追加额外的换行.
	 * @see Logger#lineFeed
	 * @return
	 */
	public boolean isExtraLineFeed() {
		return lineFeed.length() == 4;
	}
	
	/**
	 * 是否在每条日志末尾添加来源后缀（若有）.
	 * @return
	 */
	public boolean isDomainSuffix() {
		return domainSuffix;
	}

	/**
	 * 获取队列当前容量.
	 * @return
	 */
	public int size() {
		final var emq = this.emq;
		return emq.size();
	}
	
	/**
	 * 输出日志到磁盘.<br>
	 * 消息队列的消费者调用此方法。
	 * @param loggable
	 */
	private final void process(Loggable loggable) {
		final var sdf = this.timesuffix;
		final var sb = this.sb;
		final var lineFeed = this.lineFeed;
		Date date = null;
		String fromDomain;
		try {
			// 获取等待写出的日志文本
			var msg = loggable.getMessage();
			sb.setLength(0);
			sb.append(msg == null ? loggable.lastStackTrace() : msg);
			sb.append("\r\n").append(getStackTrace(loggable.getNonNullCause()));
			// 判断是否增加时间后缀
			if (sdf != null)
				sb.append(sdf.format(date = new Date(loggable.getOccurredTime())));
			// 判断是否增加来源后缀
			if (domainSuffix && (fromDomain = loggable.fromDomain()) != null)
				sb.append(" from ").append(fromDomain);
			msg = sb.append(lineFeed).toString();

			// 判断是否按日期输出不同的日志文件
			if (this.dated != null) {
				var time = loggable.getOccurredTime();
				// 判断是否已到次日
				if (time - this.lastTime >= 84600000/* 一天的毫秒数 */) {
					var extension = recordDate(date == null ? new Date(time) : date);
					this.file = new File(path.concat(extension));
					// 新建带有当前日期后缀的日志文件（先关闭上一个文件输出流）
					CloseSource.close(this.outputStream);
					this.outputStream = new FileOutputStream(file, append);
				}
			}

			// 若已有此文件，则在文件末尾追加写入；若无此文件，则新建
			outputStream.write(msg.getBytes());
			outputStream.flush();
		} catch (IOException e) {
			// 只抛出异常，不写日志
			throw new DefinedRuntimeException(false, "异常日志输出失败，文件输出流异常", e);
		} catch (Exception e) {
			// 只抛出异常，不写日志
			throw new DefinedRuntimeException(false, "写日志时出现未知异常", e);
		}
	}

	/**
	 * 输出日志到磁盘文件（异步）.<br>
	 * 使用add方法将元素添加到队列，若队列已满，会抛出异常。
	 * 为何不使用put方法（队列已满会阻塞线程直至队列有空间可用）或offer方法（队列已满会返回false），
	 * 原因在于：writeAsyn方法主要是作为主系统的子系统，用来记录异常日志。
	 * 若主系统生成了巨量的异常对象，以至于队列都被填满，则此时主系统一定是发生了重大故障。
	 * 此时应当立刻抛出异常以便用户和管理员察觉，显然put方法和offer方法不能做到。
	 * @param e
	 * @return
	 */
	public boolean writeAsync(Loggable e) {
		final var emq = this.emq;
		/*
		 *  发现add方法的源码竟然就是直接调用offer方法，若返回false则手动new一个异常。
		 *  于是，稍加改造其源码，以减少运行时无谓的栈调用。
		 */
		if (emq.cmq.queue.offer(e))
			return true;
		else
			throw new DefinedRuntimeException(false, "Logger队列已满", e.getNonNullCause());
	}
	
	/**
	 * 直接向磁盘写日志，不经过消息队列.<br>
	 * 	注意：此方法不判空，调用者应保证传入的参数不为null。<br>
	 * 注意：非线程安全，仅syncLogger方法构造的对象可调用此方法。
	 * @param e
	 * @see Logger#syncLogger(String, boolean, boolean, boolean)
	 */
	public void writeSync(Loggable e) {
		if (this.emq == null)
			process(e);
		else
			throw new DefinedRuntimeException(false, "仅syncLogge方法构造的对象可调用此方法");
	}
	
	/**
	 * 将异常信息转换为字符串.
	 * @param cause
	 * @return
	 */
	public final static String getStackTrace(Throwable cause) {
		try (var sw = new StringWriter();
				var pw = new PrintWriter(sw)) {
			cause.printStackTrace(pw);
			return sw.toString();
		} catch (Exception e) {
			return cause.getClass().getName();
		}
	}
	
	/**
	 * 清空队列.
	 */
	public void clear() {
		final var emq = this.emq;
		emq.cmq.queue.clear();
	}
	
	/**
	 * 关闭对象.
	 */
	@Override
	public void close() {
		final var cmq = this.emq.cmq;
		CloseSource.close(cmq, this.outputStream);
		this.outputStream = null;
		this.timesuffix = null;
		this.path = null;
		this.file = null;
		this.dated = null;
		this.lastTime = -1;
	}
	
	/**
	 * 日志记录器的XML配置文件解析器.
	 * @author Rex
	 *
	 */
	private static final class XMLParser extends DefaultHandler {
		
		/**
		 * 标记是否已进入对应的exception标签.
		 */
		private boolean started;

		/**
		 * 0：当前处在path标签；<br>
		 * 1：当前处在timesuffix标签；<br>
		 * 2：当前处在append标签；<br>
		 * 3：当前处在maxSize标签；<br>
		 * 4：当前处在threadPriority标签；<br>
		 * 5：当前处在sparePath标签；<br>
		 * 6：当前处在daemon标签；<br>
		 * 7：当前处在threadName标签；<br>
		 * 8：当前处在dated标签；<br>
		 * 9：当前处在extraLineFeed标签；<br>
		 * 10：当前处在domainSuffix标签；<br>
		 * 其他数字：当前未处于指定的标签.
		 */
		private int status;

		/**
		 * 是否已完成整个解析过程.
		 */
		private boolean finished;

		/**
		 * 对应exception标签的type属性.
		 */
		private String type;

		// 下述变量为xml配置文件的原始值
		private char[] _path;
		private char[] _sparePath;
		private char[] _timesuffix;
		private char[] _append;
		private char[] _maxSize;
		private char[] _threadPriority;
		private char[] _daemon;
		private char[] _threadName;
		private char[] _dated;
		private char[] _extraLineFeed;
		private char[] _domainSuffix;
		
		// 下述变量为转换后的变量值
		private String path;
		private String sparePath;
		private boolean timesuffix;
		private boolean append;
		private int maxSize;
		private int threadPriority;
		private boolean daemon;
		private String threadName;
		private boolean dated;
		private boolean extraLineFeed;
		private boolean domainSuffix;

		private XMLParser(final String type) {
			this.started = false;
			this.status = -1;
			this.finished = false;
			this.type = type;
			char[] blank = new char[0];
			this._path = blank;
			this._sparePath = blank;
			this._timesuffix = blank;
			this._append = blank;
			this._maxSize = blank;
			this._threadPriority = blank;
			this._daemon = blank;
			this._threadName = blank;
			this._dated = blank;
			this._extraLineFeed = blank;
			this._domainSuffix = blank;
		}
		
		/**
		 * 将配置文件读取到的原始值转换为所需的变量值.
		 */
		private final void convert() {
			this.path = new String(_path);
			this.sparePath = new String(_sparePath);
			this.maxSize = parseInt(new String(_maxSize), 128);
			this.threadPriority = parseInt(new String(_threadPriority), 5);
			var temp_threadName = new String(_threadName);
			this.threadName = temp_threadName.isBlank() ? "Logger" : temp_threadName;
			
			// 以下几项，仅当配置项明确为false时才取false，否则均取true
			this.append = !"false".equals(new String(_append));
			this.timesuffix = !"false".equals(new String(_timesuffix));
			this.daemon = !"false".equals(new String(_daemon));
			this.domainSuffix = !"false".equals(new String(_domainSuffix));
			
			// 以下几项，仅当配置项明确为true时才取true，否则均取false
			this.dated = "true".equals(new String(_dated));
			this.extraLineFeed = "true".equals(new String(_extraLineFeed));
		}

		/**
		 * 根据配置文件解析出配置项实体类.<br>
		 * File参数若为null，会自动在当前线程上下文class路径下寻找Logger.xml文件。
		 * @param xml xml文件
		 * @param type 对应exception标签的type属性
		 * @return
		 * @throws DefinedException 
		 */
		private static final XMLParser read(File xml, String type) throws DefinedException {
			XMLParser parser = null;
			try (var is = xml == null
					? Thread.currentThread().getContextClassLoader().getResourceAsStream("Logger.xml")
					: new FileInputStream(xml)) {
				// 创建一个SAX解析器
				var sax = SAXParserFactory.newInstance().newSAXParser();
				parser = new XMLParser(type);
				sax.parse(is, parser);
				parser.convert();
			} catch (SAXException e) {
				throw new DefinedException(false, "XMLParser的SAX解析器异常", e);
			} catch (ParserConfigurationException e) {
				throw new DefinedException(false, "XMLParser创建SAX解析器失败", e);
			} catch (Exception e) {
				throw new DefinedException(false, "XMLParser解析XML出现异常", e);
			}
			return parser;
		}
		
		@Override
		public void startElement(String uri, String localName, String qName, Attributes a) throws SAXException {
			if (!finished)
				switch (qName) {
				case "exception" -> started = type.equals(a.getValue("type"));
				case "path" -> status = started ? 0 : -1;
				case "timesuffix" -> status = started ? 1 : -1;
				case "append" -> status = started ? 2 : -1;
				case "maxSize" -> status = started ? 3 : -1;
				case "threadPriority" -> status = started ? 4 : -1;
				case "sparePath" -> status = started ? 5 : -1;
				case "daemon" -> status = started ? 6 : -1;
				case "threadName" -> status = started ? 7 : -1;
				case "dated" -> status = started ? 8 : -1;
				case "extraLineFeed" -> status = started ? 9 : -1;
				case "domainSuffix" -> status = started ? 10 : -1;
				default -> status = -1;
				}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (started && "exception".equals(qName)) {
				started = false;
				finished = true;
			}
		}

		@Override
		public void characters(char[] cs, int start, int len) throws SAXException {
			char[] arg;
			if (started && (arg = strip(cs, start, len)).length != 0)
				switch (status) {
				case 0 -> _path = merge(_path, arg);
				case 1 -> _timesuffix = merge(_timesuffix, arg);
				case 2 -> _append = merge(_append, arg);
				case 3 -> _maxSize = merge(_maxSize, arg);
				case 4 -> _threadPriority = merge(_threadPriority, arg);
				case 5 -> _sparePath = merge(_sparePath, arg);
				case 6 -> _daemon = merge(_daemon, arg);
				case 7 -> _threadName = merge(_threadName, arg);
				case 8 -> _dated = merge(_dated, arg);
				case 9 -> _extraLineFeed = merge(_extraLineFeed, arg);
				case 10 -> _domainSuffix = merge(_domainSuffix, arg);
				}
		}
		
		/**
		 * 功能类似String.strip().
		 * @see String#strip()
		 * @param value
		 * @param st
		 * @param len
		 * @return
		 */
		private static final char[] strip(char[] value, int st, int len) {
			var end = st + len - 1;

			char current;
			while (st < end && ((current = value[st]) < 33 || current == 160 || current == 12288))
				st++;

			while (st < end && ((current = value[end]) < 33 || current == 160 || current == 12288))
				end--;

			var newArray = value;
			int newLen;
			if ((newLen = end - st + 1) < len)
				System.arraycopy(value, st, newArray = new char[newLen], 0, newLen);

				return newArray;
		}
		
		/**
		 * 合并char数组.
		 * @param c1
		 * @param c2
		 * @return
		 */
		private static final char[] merge(char[] c1, char[] c2) {
			var len1 = c1.length;
			var len2 = c2.length;
			var newArr = new char[len1 + len2];
			System.arraycopy(c1, 0, newArr, 0, len1);
			System.arraycopy(c2, 0, newArr, len1, len2);
			return newArr;
		}

	}
	
}