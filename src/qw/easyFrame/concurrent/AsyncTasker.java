package qw.easyFrame.concurrent;

import qw.easyFrame.entity.EasyTask;
import qw.easyFrame.interfaces.AsyncTask;
import qw.easyFrame.interfaces.ThrowHandler;

/**
 * 异步任务执行器.<br>
 * 内部不使用线程池，减小了代码复杂度和内存占用。
 * 每次执行任务都会新建一个线程，适用于不频繁执行的并发任务。<br>
 * 可看作一个线程池（实际不是），池被占满后，后续任务不执行，并返回false。
 * @author Rex
 *
 */
public class AsyncTasker {

	/**
	 * 最大任务数量.
	 */
	private final int maximum;
	
	/**
	 * 当前任务数量.
	 */
	private volatile int current = 0;
	
	/**
	 * 预设的异步任务（包装前）.
	 */
	private AsyncTask task;
	
	/**
	 * 预设的异步任务（包装后）.
	 */
	private Runnable wrapped;
	
	/**
	 * 预设的线程优先级.
	 */
	private int threadPriority;
	
	/**
	 * 预设是否设置执行任务的线程为守护线程.
	 */
	private boolean daemon;
	
	/**
	 * 线程名.
	 */
	private String threadName;
	
	/**
	 * 仅规定最大任务数量，并预设任务.<br>
	 * 使用此构造器构造的对象没有预设任务，需使用setTask(AsyncTask)先预设任务，
	 * 或使用start(AsyncTask)直接提交并执行任务。<br>
	 * 注意：使用此构造器的构造的对象不可使用start()方法。
	 * @see AsyncTasker#start(AsyncTask)
	 * @see AsyncTasker#setTask(AsyncTask)
	 * @param maximum
	 */
	public AsyncTasker(int maximum) {
		this(maximum, (AsyncTask) null);
	}
	
	/**
	 * 规定最大任务数量，并预设任务.<br>
	 * 使用此构造器构造的对象已预设任务，
	 * 可使用start()方法直接执行任务，可多次执行。<br>
	 * 注意：使用此构造器的构造的对象亦可使用start(AsyncTask)方法提交并执行任务。
	 * @see AsyncTasker#start()
	 * @see AsyncTasker#start(AsyncTask)
	 * @param maximum
	 * @param task
	 */
	public AsyncTasker(int maximum, Thread t) {
		this(maximum, new EasyTask(t));
	}
	
	/**
	 * 规定最大任务数量、预设任务.<br>
	 * 使用此构造器构造的对象已预设任务，
	 * 可使用start()方法直接执行任务，可多次执行。<br>
	 * 注意：使用此构造器的构造的对象亦可使用start(AsyncTask)方法提交并执行任务。
	 * @see AsyncTasker#start()
	 * @see AsyncTasker#start(AsyncTask)
	 * @param maximum
	 * @param task
	 */
	public AsyncTasker(int maximum, AsyncTask task) {
		if (maximum < 1)
			maximum = 1;
		this.maximum = maximum;
		setTask(task);
	}
	
	/**
	 * 获取最大任务数量.
	 * @return
	 */
	public int getMaximum() {
		return maximum;
	}

	/**
	 * 获取当前任务数量（非线程安全）.
	 * @return
	 */
	public int getCurrent() {
		return current;
	}

	/**
	 * 获取预设的异步任务（可能获取到null）.
	 * @return
	 */
	public AsyncTask getTask() {
		return task;
	}

	/**
	 * 预设任务.
	 * @param source
	 */
	public void setTask(AsyncTask task) {
		if (task != null) {
			this.task = task;
			this.wrapped = wrap(task, task.getExceptionHandler());
			this.threadPriority = task.getThreadPriority();
			this.daemon = task.isDaemon();
			this.threadName = task.getThreadName();
		}
	}
	
	/**
	 * 执行异步任务.<br>
	 * 当且仅当已经预设了任务，才可使用此方法执行任务，可多次调用。
	 * @see AsyncTasker#AsyncTasker(int, AsyncTask)
	 * @return 若任务未被执行（已达到最大任务数），则返回false
	 */
	public boolean start() {
		return act(wrapped, threadPriority, daemon, threadName, null);
	}
	
	/**
	 * 执行异步任务.<br>
	 * 使用此方法可直接指定任务并开始执行。<br>
	 * 注意：此方法不会缓存AsyncTask source参数，方法运行结束即丢弃。
 	 * @param a
	 * @return 若任务未被执行（已达到最大任务数），则返回false
	 */
	public boolean start(AsyncTask a) {
		return act(a, a.getThreadPriority(), a.isDaemon(), a.getThreadName(), a.getExceptionHandler());
	}
	
	/**
	 * 执行异步任务.<br>
	 * 使用此方法可直接指定任务并开始执行。<br>
	 * 注意：此方法不会缓存Thread thread参数，方法运行结束即丢弃。
 	 * @param t
	 * @return 若任务未被执行（已达到最大任务数），则返回false
	 */
	public boolean start(Thread t) {
		return act(t, t.getPriority(), t.isDaemon(), t.getName(), null);
	}

	/**
	 * 异步执行任务.
	 * @param runnable
	 * @param priority
	 * @param daemon
	 * @param name
	 * @param handler
	 * @return 若任务未被执行（已达到最大任务数），则返回false
	 */
	private synchronized final boolean act(Runnable runnable, int priority, boolean daemon, String name,
			ThrowHandler handler) {
		if (current != maximum) {
			current++;
			var t = new Thread(wrap(runnable, handler), name);
			t.setPriority(priority);
			t.setDaemon(daemon);
			t.start();
			return true;
		}
		return false;
	}
	
	/**
	 * 将普通Runnable包装为适用于AsyncTasker的Runnable.
	 * @param task
	 * @param handler
	 * @return
	 */
	private final Runnable wrap(Runnable task, ThrowHandler handler) {
		return () -> {
			try {
				task.run();
			} catch (Exception e) {
				if (handler != null)
					handler.handle(e, task);
				else
					throw e;
			} finally {
				synchronized (this) {
					current--;
				}
			}
		};
	}
	
}