package qw.easyFrame.concurrent;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import qw.easyFrame.entity.MQCustomer;
import qw.easyFrame.interfaces.Loggable;
import qw.easyFrame.interfaces.MQ;
import qw.easyFrame.interfaces.MQRunnable;
import qw.easyFrame.throwables.DefinedException;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.NumberUtil.parseInt;
import static qw.easyFrame.tools.statics.StringUtil.strip;

/**
 * 消息队列.<br>
 * 注意事项： <br>
 * 构造对象需要1个参数，参数的值在调用构造器时已确定（无参构造器会使用默认参数构造对象，而不是简单的super()语句），
 * 且参数在构造器内确定后禁止再更改（没有设置相应的set方法）。
 * 因此，若希望使用自定义参数（而不是默认参数）构造对象，则强烈建议调用全参构造器或使用properties配置文件，
 * 在构造时就传入参数，而不是使用无参构造器构造完成之后再使用set方法修改类变量的值。
 * @author Rex
 * @param <T>
 */
public class CustomersMQ<T> implements MQ<T> {
	
	/**
	 * 一个阻塞队列，线程安全.
	 */
	final LinkedBlockingQueue<T> queue;
	
	/**
	 * 标记是否继续执行后台线程.
	 */
	private volatile boolean run;
	
	/**
	 * 队列最大容量.
	 */
	private final int maxSize;
	
	/**
	 * 消息队列消费者.
	 */
	private List<MQCustomer<T>> customers;
	
	/**
	 * 消费者线程组.
	 */
	private Thread[] threads;
	
	/**
	 * 根据配置文件生成CustomersMQ对象. <br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找CustomersMQ.properties文件。<br>
	 * 若出现异常，则会返回一个默认（见全参构造器文档）的CustomersMQ对象。
	 * @param <T>
	 * @param properties 配置文件
	 * @return
	 */
	public static <T> CustomersMQ<T> createByProperties(File properties) {
		try (var is = properties == null
				? Thread.currentThread().getContextClassLoader().getResourceAsStream("CustomersMQ.properties")
				: new FileInputStream(properties)) {
			var p = new Properties();
			p.load(is);
			return new CustomersMQ<T>(parseInt(strip(p.getProperty("maxSize")), 1024));
		} catch (Exception e) {
			return new CustomersMQ<T>(1024);
		}
	}
	
	/**
	 * 无参构造器.
	 * 队列长度默认为1024。
	 */
	public CustomersMQ() {
		this.queue = new LinkedBlockingQueue<>(this.maxSize = 1024);
	}
	
	/**
	 * 全参构造器.
	 * @param maxSize 队列长度
	 */
	public CustomersMQ(int maxSize) {
		this.queue = new LinkedBlockingQueue<>(this.maxSize = maxSize);
	}
	
	/**
	 * 获取队列最大容量.
	 * 此值在构造对象时确定，之后不得修改。
	 * @return
	 */
	@Override
	public int getMaxSize() {
		return maxSize;
	}
	
	public List<MQCustomer<T>> getCustomers() {
		return customers;
	}

	/**
	 * 注册消息队列消费者，须在调用start方法前调用本方法.<br>
	 * 若消费者线程已在运行（已调用过start方法），则调用本方法不生效。
	 * 若需重复调用本方法，必须先调用close方法关闭消费者，重复调用本方法后再次调用start方法。
	 * @param customers
	 */
	public void setCustomers(List<MQCustomer<T>> customers) {
		this.customers = customers;
	}
	
	/**
	 * 向消息队列中添加消费者，须在调用start方法前调用本方法.<br>
	 * 若消费者线程已在运行（已调用过start方法），则调用本方法不生效。
	 * 若需重复调用本方法，必须先调用close方法关闭消费者，重复调用本方法后再次调用start方法。
	 * @param customer
	 */
	public void addCustomer(MQCustomer<T> customer) {
		addCustomers(customer, 1);
	}
	
	/**
	 * 将同一个消费者多次重复添加到消息队列，须在调用start方法前调用本方法.<br>
	 * 若消费者线程已在运行（已调用过start方法），则调用本方法不生效。
	 * 若需重复调用本方法，必须先调用close方法关闭消费者，重复调用本方法后再次调用start方法。
	 * @param customer
	 * @param times 重复添加的次数
	 */
	public void addCustomers(MQCustomer<T> customer, int times) {
		if (customers == null)
			customers = new LinkedList<>();
		for (var i = 0; i < times; i++)
			customers.add(customer);
	}

	/**
	 * 获取队列当前容量.
	 * @return
	 */
	@Override
	public int size() {
		final var queue = this.queue;
		return queue.size();
	}

	/**
	 * 添加元素到队列.<br>
	 * 使用add方法将元素添加到队列，若队列已满，会抛出异常。
	 * @param t
	 */
	@Override
	public void add(T t) {
		/*
		 *  发现add方法的源码竟然就是直接调用offer方法，若返回false则手动new一个异常。
		 *  于是，稍加改造其源码，以减少运行时无谓的栈调用
		 */
		// 将待处理的对象保存在队列
		final var queue = this.queue;
		if (!queue.offer(t))
			throw new DefinedRuntimeException(false, "CustomersMQ已满");
	}
	
	/**
	 * 添加元素到队列.<br>
	 * 使用offer方法将元素添加到队列，若队列已满，会返回false。
	 * @param t
	 */
	@Override
	public boolean offer(T t) {
		final var queue = this.queue;
		return queue.offer(t);
	}
	
	/**
	 * 添加元素到队列.<br>
	 * 使用put方法将元素添加到队列，若队列已满，线程会阻塞。
	 * @param t
	 */
	@Override
	public void put(T t) {
		final var queue = this.queue;
		try {
			queue.put(t);
		} catch (InterruptedException e) {
			throw new DefinedRuntimeException(false, "CustomersMQ添加元素异常", e);
		}
	}

	/**
	 * 启动消费者线程.
	 */
	@Override
	public void start() {
		if (!run) {
			run = true;
			var index = 0;
			threads = new Thread[customers.size()];
			for (var c : customers) {
				createThread(c.isDaemon(), c.getThreadPriority(), c.getRunnable(), c.getThreadName(), index);
				index++;
			}
		}
	}

	/**
	 * 生成消费者线程.
	 * @param d 标记该线程是否是守护线程
	 * @param p 线程优先级
	 * @param r 消费者
	 * @param n 线程名
	 * @param i 消费者线程组下标
	 */
	private final void createThread(boolean d, int p, MQRunnable<T> r, String n, int i) {
		var t = new Thread(() -> run(d, p, r, n, i), n);
		threads[i] = t;
		t.setDaemon(d);
		t.setPriority(p);
		t.start();
	}
	
	/**
	 * 消费者循环消费阻塞队列的头部元素.
	 * @param d 标记该线程是否是守护线程
	 * @param p 线程优先级
	 * @param r 消费者
	 * @param n 线程名
	 * @param i 消费者线程组下标
	 */
	private final void run(boolean d, int p, MQRunnable<T> r, String n, int i) {
		final var queue = this.queue;
		try {
			while (run)
				r.run(queue.take());
		} catch (Exception e) {
			/*
			 * 仅正常运行时（run等于true时）抛出的异常会被记录并重建线程，
			 * 而用户主动调用close方法（先使run等于false，再调用线程的interrupt方法）
			 * 抛出的InterruptedException异常属于正常业务逻辑，无需记录和重建线程。
			 */
			if (run) {
				// 当抛出的异常不是Loggable，或是一个未写日志的Loggable
				if (!(e instanceof Loggable log) || log.isQueued() == false)
					new DefinedException("消费者线程异常中断", e);
				createThread(d, p, r, n, i);
			}
		}
	}
	
	/**
	 * 清空队列.
	 */
	@Override
	public void clear() {
		final var queue = this.queue;
		queue.clear();
	}
	
	/**
	 * 关闭消息队列.<br>
	 * 含关闭消费者线程。
	 */
	@Override
	public void close() {
		final var queue = this.queue;
		queue.clear();
		
		this.run = false;
		// 第一个判断是为了保证for循环一定在run变量赋值之后执行
		if (this.run == false && this.threads != null)
			for (var t : this.threads)
				t.interrupt();
		
		this.threads = null;
		this.customers = null;
	}
	
}