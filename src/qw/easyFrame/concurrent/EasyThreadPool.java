package qw.easyFrame.concurrent;

import java.util.concurrent.Executor;
import qw.easyFrame.entity.MQCustomer;
import qw.easyFrame.interfaces.ThrowHandler;
import qw.easyFrame.throwables.DefinedRuntimeException;

/**
 * 使用CustomersMQ实现的简单的线程池.<br>
 * 池内线程全部是守护线程。
 * @author Rex
 *
 */
public class EasyThreadPool implements Executor {
	
	/**
	 * 全局异常处理方式（此项可为null）.
	 */
	private ThrowHandler handler;

	/**
	 * 用于封装待执行的任务的消息队列.
	 */
	private final CustomersMQ<Runnable> cmq;
	
	/**
	 * 线程池大小.
	 */
	private final int size;
	
	/**
	 * 此构造器构造的池内线程是优先级为5的守护线程.
	 * @param size 线程池大小
	 */
	public EasyThreadPool(int size) {
		this(size ,5, true);
	}
	
	/**
	 * @param size 线程池大小
	 * @param threadPriority 线程优先级
	 * @param daemon 是否设为守护线程
	 */
	public EasyThreadPool(int size, int threadPriority, boolean daemon) {
		this.size = size;
		this.cmq = new CustomersMQ<>(Integer.MAX_VALUE);
		var cus = new MQCustomer<Runnable>(R -> R.run(), threadPriority, daemon, "EasyThreadPool");
		this.cmq.addCustomers(cus, size);
		// 在构造器内直接启动池内线程（即CustomersMQ的消费者线程）
		this.cmq.start();
	}
	
	/**
	 * 获取全局异常处理方式（可能返回null）.
	 * @return
	 */
	public ThrowHandler getExceptionHandler() {
		return handler;
	}

	/**
	 * 设置全局异常处理方式（可不设置此项）.<br>
	 * 注意1：若不设置，池内线程遭遇异常会中断，并写日志（见注意2），随后重建线程；
	 * 注意2：若该异常已自行写过日志，则不再写日志。若无，则会将其捕捉，并写日志；
	 * 注意3：此项须在调用execute方法前调用，方可对Runnable对象生效；
	 * 注意4：在线程池使用过程中可重复调用此方法。
	 * @param handler
	 */
	public void setExceptionHandler(ThrowHandler handler) {
		this.handler = handler;
	}

	/**
	 * 获取线程池大小.
	 * @return
	 */
	public int size() {
		return size;
	}
	
	/**
	 * 获取目前队列中待执行的任务总数.
	 * @return
	 */
	public int getRunnableSize() {
		final var queue = this.cmq.queue;
		return queue.size();
	}
	
	/**
	 * 向线程池内添加任务.<br>
	 * 若任务队列已满，添加任务的线程会被阻塞。但由于任务队列的大小是Integer.MAX_VALUE，
	 * 所以在生产环境中实际上并不会遇到被阻塞的情况。
	 * @param r
	 */
	@Override
	public void execute(Runnable r) {
		execute(r, 1);
	}
	
	/**
	 * 将待执行的任务重复times次添加到线程池内.<br>
	 * 若任务队列已满，添加任务的线程会被阻塞。但由于任务队列的大小是Integer.MAX_VALUE，
	 * 所以在生产环境中实际上并不会遇到被阻塞的情况。
	 * @param r
	 * @param times
	 */
	public void execute(Runnable r, int times) {
		if (handler != null)
			r = wrap(r);

		final var cmq = this.cmq;
		try {
			for (var i = 0; i < times; i++)
				cmq.queue.put(r);
		} catch (InterruptedException e) {
			throw new DefinedRuntimeException(false, "EasyThreadPool添加任务异常", e);
		}
	}

	/**
	 * 关闭线程池（即关闭池内所有线程并清空待执行的任务队列）.
	 */
	public void shutdown() {
		this.cmq.close();
	}
	
	/**
	 * 清空待执行的任务队列.
	 */
	public void clear() {
		this.cmq.clear();
	}
	
	/**
	 * 根据设定的全局异常处理方式，重新包装Runnable对象.
	 * @param r
	 * @return
	 */
	private final Runnable wrap(Runnable r) {
		return () -> {
			try {
				r.run();
			} catch (Exception e) {
				handler.handle(e, r);
			}
		};
	}
	
}