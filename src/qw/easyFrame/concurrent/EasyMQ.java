package qw.easyFrame.concurrent;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import qw.easyFrame.entity.MQCustomer;
import qw.easyFrame.interfaces.MQ;
import qw.easyFrame.interfaces.MQRunnable;
import qw.easyFrame.throwables.DefinedRuntimeException;
import static qw.easyFrame.tools.statics.NumberUtil.parseInt;
import static qw.easyFrame.tools.statics.StringUtil.strip;

/**
 * 消息队列.<br>
 * 注意事项： <br>
 * 构造对象共需3个参数，参数的值在调用构造器时已全部确定（无参构造器会使用默认参数构造对象，而不是简单的super()语句，
 * 非全参构造器也会使用默认参数补齐缺少的参数），且参数在构造器内确定后禁止再更改（没有设置相应的set方法）。
 * 因此，若希望使用自定义参数（而不是默认参数）构造对象，则强烈建议调用全参构造器或使用properties配置文件，
 * 在构造时就传入全部自定义参数，而不是使用无参构造器构造完成之后再使用set方法修改类变量的值。
 * @author Rex
 * @param <T>
 */
public class EasyMQ<T> implements MQ<T> {
	
	/**
	 * 一个阻塞队列，线程安全.
	 */
	final CustomersMQ<T> cmq;
	
	private final MQCustomer<T> customer;
	
	/**
	 * 根据配置文件生成EasyMQ对象.  <br>
	 * File参数若为null，会自动在当前线程上下文class路径下寻找EasyMQ.properties文件。<br>
	 * 若出现异常，则会返回一个默认（见全参构造器文档）的EasyMQ对象。
	 * @param <T>
	 * @param properties 配置文件
	 * @return
	 */
	public static <T> EasyMQ<T> createByProperties(File properties) {
		try (var is = properties == null
				? Thread.currentThread().getContextClassLoader().getResourceAsStream("EasyMQ.properties")
				: new FileInputStream(properties)) {
			var p = new Properties();
			p.load(is);

			var maxSize = parseInt(strip(p.getProperty("maxSize")), 1024);
			var daemon = "true".equals(strip(p.getProperty("daemon")));
			var threadPriority = parseInt(strip(p.getProperty("threadPriority")), 5);
			var threadName = strip(p.getProperty("threadName"));
			if (threadName.isEmpty())
				threadName = "EasyMQ";

			return new EasyMQ<T>(maxSize, daemon, threadPriority, threadName);
		} catch (Exception e) {
			return new EasyMQ<T>(1024, true, 5, "EasyMQ");
		}
	}
	
	/**
	 * 无参构造器.
	 */
	public EasyMQ() {
		this(1024, true, 5, "EasyMQ");
	}
	
	/**
	 * @param maxSize 队列长度
	 */
	public EasyMQ(int maxSize) {
		this(maxSize, true, 5, "EasyMQ");
	}
	
	/**
	 * 全参构造器.
	 * @param maxSize 队列长度，默认为1024
	 * @param daemon 设置后台线程是否是守护线程，默认为true
	 * @param threadPriority 线程优先级，默认为5
	 * @param threadName 线程名，默认为"EasyMQ"
	 */
	public EasyMQ(int maxSize, boolean daemon, int threadPriority, String threadName) {
		this.cmq = new CustomersMQ<>(maxSize);
		this.customer = new MQCustomer<>(threadPriority, daemon, threadName);
	}
	
	/**
	 * 获取队列最大容量.<br>
	 * 此值在构造对象时确定，之后不得修改。
	 * @return
	 */
	@Override
	public int getMaxSize() {
		final var cmq = this.cmq;
		return cmq.getMaxSize();
	}
	
	/**
	 * 得到后台线程优先级.<br>
	 * 此值在构造器内确定后不可更改。
	 * @return
	 */
	public int getThreadPriority() {
		final var customer = this.customer;
		return customer.getThreadPriority();
	}
	
	/**
	 * 判断后台线程是否是守护线程.<br>
	 * 此值在构造器内确定后不可更改。
	 * @return
	 */
	public boolean isDaemon() {
		final var customer = this.customer;
		return customer.isDaemon();
	}
	
	/**
	 * 获取线程名.<br>
	 * 此值在构造器内确定后不可更改。
	 * @return
	 */
	public String getThreadName() {
		final var customer = this.customer;
		return customer.getThreadName();
	}
	
	public MQRunnable<T> getRunnable() {
		final var customer = this.customer;
		return customer.getRunnable();
	}

	/**
	 * 注册消息队列消费者，须在调用start方法前调用本方法.
	 * 若消费者线程已在运行（已调用过start方法），则调用本方法不生效。
	 * 若需重复调用本方法，必须先调用close方法关闭消费者，重复调用本方法后再次调用start方法。
	 * @param runnable
	 */
	public void setRunnable(MQRunnable<T> runnable) {
		final var cmq = this.cmq;
		final var customer = this.customer;
		customer.setRunnable(runnable);
		cmq.addCustomer(this.customer);
	}

	/**
	 * 获取队列当前容量.
	 * @return
	 */
	@Override
	public int size() {
		final var queue = this.cmq.queue;
		return queue.size();
	}

	/**
	 * 添加元素到队列.<br>
	 * 使用add方法将元素添加到队列，若队列已满，会抛出异常。
	 * @param t
	 */
	@Override
	public void add(T t) {
		final var cmq = this.cmq;
		/*
		 *  发现add方法的源码竟然就是直接调用offer方法，若返回false则手动new一个异常。
		 *  于是，稍加改造其源码，以减少运行时无谓的栈调用
		 */
		// 将待处理的对象保存在队列
		if (!cmq.queue.offer(t))
			throw new DefinedRuntimeException(false, "EasyMQ已满");
	}
	
	/**
	 * 添加元素到队列.<br>
	 * 使用offer方法将元素添加到队列，若队列已满，会返回false。
	 * @param t
	 */
	@Override
	public boolean offer(T t) {
		final var cmq = this.cmq;
		return cmq.queue.offer(t);
	}
	
	/**
	 * 添加元素到队列.<br>
	 * 使用put方法将元素添加到队列，若队列已满，线程会阻塞。
	 * @param t
	 */
	@Override
	public void put(T t) {
		final var cmq = this.cmq;
		try {
			cmq.queue.put(t);
		} catch (InterruptedException e) {
			throw new DefinedRuntimeException(false, "EasyMQ添加元素异常", e);
		}
	}

	/**
	 * 启动消费者线程.
	 */
	@Override
	public void start() {
		final var cmq = this.cmq;
		cmq.start();
	}
	
	/**
	 * 清空队列.
	 */
	@Override
	public void clear() {
		final var cmq = this.cmq;
		cmq.queue.clear();
	}
	
	/**
	 * 关闭消息队列.
	 * 含关闭消费者线程。
	 */
	@Override
	public void close() {
		final var cmq = this.cmq;
		cmq.close();
	}
	
}